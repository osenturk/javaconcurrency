/**
 * 
 */
package hdm.sanitycheck;

import com.alcatel.hdm.service.nbi.client.NBIServicePort;
import hdm.sanitycheck.main.SanityCheckUtil;
import hdm.sanitycheck.nbicore.NbiServiceFactory;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;

/**
 * @author ozan
 *
 */
public class TestNbiServiceFactory {

	private final static Logger  logger = Logger.getLogger(TestNbiServiceFactory.class);
	
	private Map<String, String> parameterMap;
	private String username,password;
	private List<String> endUrlList; 
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	protected void setUp() throws Exception {

		parameterMap = SanityCheckUtil.loadParameters();
		endUrlList = SanityCheckUtil.mergeIpsAndPorts(parameterMap.get(SanityCheckUtil.PARAMETER_NBI_IPS), parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PORTS));
		
		username=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		password=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);
		
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	@After
	protected void tearDown() throws Exception {

		endUrlList=null;
		parameterMap=null;
	}

	@Test
	public void testInitializingNbiServiceFactory() throws IOException, ServiceException{
		
		NbiServiceFactory factory = NbiServiceFactory.getInstance();
		assertTrue("factory cannot be null",factory!=null);
		
		for (String url : endUrlList) {
			NBIServicePort nbiPort = factory.getNbiPortInstance(url,username,password);
			logger.debug("initializing nbi service for the end point url: "+url+" within username,password "+parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME)+","+parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD));
			assertTrue("nbiPort cannot be null",nbiPort!=null);
		}
		
		
		
	}


	public void testGetNBIResourceListWhenAllServersUp() throws ServiceException {

		NbiServiceFactory factory = NbiServiceFactory.getInstance();
		assertTrue("factory cannot be null",factory!=null);

		List<NBIServicePort> list = NbiServiceFactory.getNBIResourceList(endUrlList,username,password);

		assertTrue("nbi resources should be equal if all servers up",list.size()==endUrlList.size());



	}

	public void testGetNBIResourceListWhenAllServersUpButOneDown() throws ServiceException {

		String shutdownServerURL="http://172.20.44.241:7015/remotehdm/NBIService?WSDL";
		endUrlList.add(shutdownServerURL);
		NbiServiceFactory factory = NbiServiceFactory.getInstance();
		assertTrue("factory cannot be null",factory!=null);

		List<NBIServicePort> list = NbiServiceFactory.getNBIResourceList(endUrlList,username,password);

		assertTrue("nbi resources should be equal if all servers up",list.size()!=endUrlList.size());



	}
	@Test
	public void testGetNBIResourceListWithExcessiveIteratorCall() throws ServiceException {

			int a =0;

		for (int i=0;i<100;i++){

			Iterator<String> iterator = endUrlList.iterator();
				while (iterator.hasNext()){
					a++;
					logger.debug(iterator.next());
				}




		}

		logger.debug("number of iterator "+a);



	}


}
