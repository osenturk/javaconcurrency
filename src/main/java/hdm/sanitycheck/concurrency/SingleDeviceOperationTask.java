/**
 * 
 */
package hdm.sanitycheck.concurrency;

import alamotv.hdm.functions.types.request.GetParameterValuesDTO;
import com.alcatel.hdm.service.nbi.client.NBIServicePort;
import com.alcatel.hdm.service.nbi.dto.NBIFunction;
import com.alcatel.hdm.service.nbi.dto.NBISingleDeviceOperationOptions;
import hdm.sanitycheck.database.Device;
import hdm.sanitycheck.main.SanityCheckUtil;
import hdm.sanitycheck.main.SanityCheckUtil.RemoteCall;
import org.apache.log4j.Logger;

import javax.xml.rpc.soap.SOAPFaultException;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * @author ozan
 *
 */
public class SingleDeviceOperationTask extends SingleDeviceOperation implements Callable<Device> {
	
	private final static Logger  logger = Logger.getLogger(SingleDeviceOperationTask.class);
	
	private NBIServicePort nbiServicePort;
	private hdm.sanitycheck.database.Device device;
	private RemoteCall remoteCall;
	private String parameterToBeRead;

	private final int RETRY_COUNT=3;

	private Set<String> messageSet;
	
	public SingleDeviceOperationTask(NBIServicePort nbiPort, hdm.sanitycheck.database.Device device, RemoteCall remoteCall, String parameterToBeRead){
		this.nbiServicePort=nbiPort;
		this.device=device;
		this.remoteCall=remoteCall;
		this.parameterToBeRead=parameterToBeRead;
	}

	public Device call() throws RemoteException {
		

		NBIFunction nbiFunction = new NBIFunction();

		if(remoteCall.getCode()==RemoteCall.GPV.getCode()){

			GetParameterValuesDTO getParameterValuesRequestData = new GetParameterValuesDTO();
			getParameterValuesRequestData
					.setParameterNames(new String[] { parameterToBeRead });
			// specify our get parameters function
			nbiFunction.setFunctionArguments(new Object[] { getParameterValuesRequestData });

		}else{
			//TODO after SPV implemented this block should be updated.
			remoteCall=RemoteCall.CDA;
		}


		nbiFunction.setFunctionCode(remoteCall.getCode());
			
		NBISingleDeviceOperationOptions nbiOptions = new NBISingleDeviceOperationOptions();
		nbiOptions.setExecutionTimeoutSeconds(getExecutionTimeout());
		nbiOptions.setExpirationTimeoutSeconds(getExpirationTimeout());
		nbiOptions.setPriority(getPriority());
		nbiOptions.setPolicyClass(getPolicyClass());
		nbiOptions.setDisableCaptureConstraint(isDisableCaptureConstraint());
		
		/*
		 * in case the sdo crashes, it is logged.
		 */
		Long operationId=null;
		int retrySingleDeviceOperation=0;
		try {

			do{

				logger.debug(remoteCall.name()+" started for "+device.getDeviceId());
				operationId = (Long) nbiServicePort.createSingleDeviceOperationByDeviceGUID(device.getDeviceId(), nbiFunction, nbiOptions);

				if(retrySingleDeviceOperation>0) {
					/*
					one second wait for each submission
					 */
					SanityCheckUtil.getSleep(1);
					logger.error("Single device operation "+ remoteCall.name()+" retried " + retrySingleDeviceOperation + " time(s) for " + device.getDeviceId());
				}
			}while ((operationId==null)&&(retrySingleDeviceOperation++<RETRY_COUNT)) ;

				logger.info(remoteCall.name() +" completed for device: "+device.getDeviceId()+" operation id "+operationId);
		} catch (java.rmi.RemoteException remoteException) {


			/*
				filter caught exception and proceed release device
			 */
			messageSet = new HashSet<String>();
			messageSet.add("Message did not contain a valid Security Element");
			messageSet.add("UsernameToken not provided");

			Throwable throwable = remoteException.getCause();

			try {

				if (throwable instanceof SOAPFaultException) {

					if(!messageSet.contains(((SOAPFaultException) throwable).getFaultString())){

						logger.error("device might be captured- release device and try again "+device.getDeviceId());
						nbiServicePort.releaseDevice(device.getDeviceId());
						SanityCheckUtil.getSleep(1);
						logger.info("device is released");
						operationId = (Long) nbiServicePort.createSingleDeviceOperationByDeviceGUID(device.getDeviceId(), nbiFunction, nbiOptions);
						logger.info("after device released device id "+device.getDeviceId()+" and device operation id "+operationId);

					}
				}else{
					logger.error("device id: "+device.getDeviceId()+" throw exception"+remoteException);
				}
			} catch (Exception e2) {

				try {

					logger.error(" single device operation threw RemoteException but out of known matchers "+device.getDeviceId()+" "+e2.getMessage());

					int innerRetrySingleDeviceOperation=0;

					do{

						operationId = (Long) nbiServicePort.createSingleDeviceOperationByDeviceGUID(device.getDeviceId(), nbiFunction, nbiOptions);

						if(innerRetrySingleDeviceOperation>0) {
					/*
					one second wait for each submission
					 */
							SanityCheckUtil.getSleep(1);
							logger.error("Single device operation inner retry "+ remoteCall.name()+" retried " + retrySingleDeviceOperation + " time(s) for " + device.getDeviceId());
						}
					}while ((operationId==null)&&(innerRetrySingleDeviceOperation++<RETRY_COUNT)) ;


					logger.info(remoteCall.name() +" completed after threw RemoteException for device: "+device.getDeviceId()+" operation id "+operationId);

				}catch (Exception e){

					logger.error("after remote exception one more try also failed for "+device.getDeviceId()+" "+ e.getMessage());

				}



			}finally {
				messageSet=null;
			}
		}catch (Exception e3) {

			try {

				logger.error(" single device operation threw Exception different than Remote "+device.getDeviceId()+" "+e3.getMessage());

				int outerRetrySingleDeviceOperation=0;

				do{

					operationId = (Long) nbiServicePort.createSingleDeviceOperationByDeviceGUID(device.getDeviceId(), nbiFunction, nbiOptions);

					if(outerRetrySingleDeviceOperation>0) {
					/*
					one second wait for each submission
					 */
						SanityCheckUtil.getSleep(1);
						logger.error("Single device operation outer retry "+ remoteCall.name()+" retried " + retrySingleDeviceOperation + " time(s) for " + device.getDeviceId());
					}
				}while ((operationId==null)&&(outerRetrySingleDeviceOperation++<RETRY_COUNT)) ;

				logger.info(remoteCall.name() +" completed after threw Exception different than Remote for device: "+device.getDeviceId()+" operation id "+operationId);

			}catch (Exception e){

				logger.error("after remote exception one more try also failed for "+device.getDeviceId()+" "+ e.getMessage());

			}

		}finally {
			messageSet=null;
		}
		
		logger.debug("operation result id: "+operationId);
		
		device.setOperationResultId(operationId);

		return device;
	}

}
