package hdm.sanitycheck.database;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Comparator;
import java.util.Date;

/**
 * Created by elf on 12/23/15.
 */
public class Summary {


    private Date creationDate;
    private String devicetype;
    private String firmware;
    private String operation;
    private long success;
    private long failure;
    private long expirationTimeout;
    private long executionTimeout;
    private long sessionTimeout;
    private long pending;
    private long aborted;
    private long norecord;
    private long exception;
    private long successratio;
    private long totalNumberDevices;
    private long sampleNumberDevices;
    private long id;
    private long deviceTypeId;
    private long iteration;
    private int trend;
    private String protocol;

    @Override
    public String toString() {
        return "Summary{ id=" + id +
                ", creationDate=" + creationDate +
                ", iteration=" + iteration +
                ", trend=" + trend +
                ", deviceTypeId=" + deviceTypeId +
                ", devicetype=" + devicetype +
                ", firmware=" + firmware +
                ", operation=" + operation +
                ", success=" + success +
                ", failure=" + failure +
                ", expirationTimeout=" + expirationTimeout +
                ", executionTimeout=" + executionTimeout +
                ", sessionTimeout=" + sessionTimeout +
                ", pending=" + pending +
                ", aborted=" + aborted +
                ", norecord=" + norecord +
                ", exception=" + exception +
                ", norecord=" + norecord +
                ", successratio=" + successratio +
                ", totalNumberDevices=" + totalNumberDevices +
                ", sampleNumberDevices=" + sampleNumberDevices +
                ", protocol=" + protocol +
                "}";
    }




    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
                .append(this.devicetype)
                .append(this.firmware)
                .append(this.operation)
                .append(this.protocol)

                 .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Summary)) {
            return false;
        }
        if (obj == this)
            return true;

        Summary rhs = (Summary) obj;
        return new EqualsBuilder().
                // if deriving: appendSuper(super.equals(obj)).
                        append(devicetype, rhs.devicetype).
                        append(firmware, rhs.firmware).
                        append(operation, rhs.operation).
                        append(protocol, rhs.protocol).

                        isEquals();
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public long getFailure() {
        return failure;
    }

    public void setFailure(long failure) {
        this.failure = failure;
    }

    public long getAborted() {
        return aborted;
    }

    public void setAborted(long aborted) {
        this.aborted = aborted;
    }

    public String getDevicetype() {
        return devicetype;
    }

    public void setDevicetype(String devicetype) {
        this.devicetype = devicetype;
    }

    public long getException() {
        return exception;
    }

    public void setException(long exception) {
        this.exception = exception;
    }

    public String getFirmware() {
        return firmware;
    }

    public void setFirmware(String firmware) {
        this.firmware = firmware;
    }

    public long getNorecord() {
        return norecord;
    }

    public void setNorecord(long norecord) {
        this.norecord = norecord;
    }

    public long getPending() {
        return pending;
    }

    public void setPending(long pending) {
        this.pending = pending;
    }

    public long getSuccess() {
        return success;
    }

    public void setSuccess(long success) {
        this.success = success;
    }

    public long getSuccessratio() {
        return successratio;
    }

    public void setSuccessratio(long successratio) {
        this.successratio = successratio;
    }

    public long getExpirationTimeout() {
        return expirationTimeout;
    }

    public void setExpirationTimeout(long expirationTimeout) {
        this.expirationTimeout = expirationTimeout;
    }

    public long getExecutionTimeout() {
        return executionTimeout;
    }

    public void setExecutionTimeout(long executionTimeout) {
        this.executionTimeout = executionTimeout;
    }

    public long getSessionTimeout() {
        return sessionTimeout;
    }

    public void setSessionTimeout(long sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }


    public long getTotalNumberDevices() {
        return totalNumberDevices;
    }

    public void setTotalNumberDevices(long totalNumberDevices) {
        this.totalNumberDevices = totalNumberDevices;
    }

    public long getSampleNumberDevices() {
        return sampleNumberDevices;
    }

    public void setSampleNumberDevices(long sampleNumberDevices) {
        this.sampleNumberDevices = sampleNumberDevices;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDeviceTypeId() {
        return deviceTypeId;
    }

    public void setDeviceTypeId(long deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    public long getIteration() {
        return iteration;
    }

    public void setIteration(long iteration) {
        this.iteration = iteration;
    }

    public int getTrend() {
        return trend;
    }

    public void setTrend(int trend) {
        this.trend = trend;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }



    /*Comparator for sorting the list by roll no*/
    public static Comparator<Summary> SummaryComparator = new Comparator<Summary>() {

        public int compare(Summary s1, Summary s2) {

            String summaryFullName1=s1.devicetype+s1.getFirmware()+s1.getOperation();
            String summaryFullName2=s2.devicetype+s2.getFirmware()+s2.getOperation();


            if (summaryFullName1 == summaryFullName2) {
                return 0;
            }
            if (summaryFullName1 == null) {
                return -1;
            }
            if (summaryFullName2 == null) {
                return 1;
            }
               /*For ascending order*/
            return summaryFullName1.compareTo(summaryFullName2);


        }};
}
