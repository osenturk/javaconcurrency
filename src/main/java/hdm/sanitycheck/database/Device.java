/**
 * 
 */
package hdm.sanitycheck.database;

import hdm.sanitycheck.main.SanityCheckUtil;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Date;



/**
 * @author ozan
 *
 */
public class Device extends DeviceType {

	
	protected long deviceId;
	protected String subscriberId;
	protected Date lastcontacttime;
	protected String serialNumber;
	protected String externalIpaddress;

	/**
	 * devicetype description added to empower understanding
	 */
	protected String deviceTypeName;

	protected Long operationResultId;

	protected SanityCheckUtil.DeviceActionResultStatus deviceActionResultStatus;

	public long getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(long deviceId) {
		this.deviceId = deviceId;
	}

	public String getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}
	public Date getLastcontacttime() {
		return lastcontacttime;
	}
	public void setLastcontacttime(Date lastcontacttime) {
		this.lastcontacttime = lastcontacttime;
	}


	public String getDeviceTypeName() {
		return deviceTypeName;
	}

	public void setDeviceTypeName(String deviceTypeName) {
		this.deviceTypeName = deviceTypeName;
	}
	
	  @Override
	    public String toString() {
	        return super.toString() + " Device{ deviceId=" + deviceId +

	        		", subscriberid=" + subscriberId + 
	        		", lastcontacttime=" + lastcontacttime +
					", operationResultId=" + operationResultId +
	        		", deviceActionResult=" + deviceActionResultStatus +
	        		", serialnumber=" + serialNumber +
					", devicetypeName=" + deviceTypeName +
					", Product Class=" + PC +
					", OUI=" + OUI +
					", Manufacturer=" + manufacturer +
					", Model=" + model +

	        		"}";

	    }
	/**
	 * @return the operationResultId
	 */
	public Long getOperationResultId() {
		return operationResultId;
	}
	/**
	 * @param operationResultId the operationResultId to set
	 */
	public void setOperationResultId(Long operationResultId) {
		this.operationResultId = operationResultId;
	}
	
	 @Override
	   public int hashCode()
	   {
	      return new HashCodeBuilder()
	         .append(this.deviceId)
	         .append(this.deviceId)
	         .append(this.lastcontacttime)
	         .append(this.operationResultId)

	         .append(this.subscriberId)
	         .append(this.serialNumber)
		     	  .append(this.deviceTypeName)
				  .append(this.PC)
				  .append(this.OUI)
				  .append(this.manufacturer)
				  .append(this.model)

	         .toHashCode();
	   }

	@Override
	 public boolean equals(Object obj) {
	       if (!(obj instanceof Device)) {
			   return false;
		   }
	        if (obj == this)
	            return true;

	        Device rhs = (Device) obj;
	        return new EqualsBuilder().
	            // if deriving: appendSuper(super.equals(obj)).
	            append(deviceId, rhs.deviceId).

	            append(lastcontacttime, rhs.lastcontacttime).
	            append(operationResultId, rhs.operationResultId).

	            append(subscriberId, rhs.subscriberId).
	            append(serialNumber, rhs.serialNumber).
							append(deviceTypeName,rhs.deviceTypeName).
							append(PC,rhs.PC).
							append(OUI,rhs.OUI).
							append(manufacturer,rhs.manufacturer).
							append(model,rhs.model).

		isEquals();
	    }

	public SanityCheckUtil.DeviceActionResultStatus getDeviceActionResultStatus() {
		return deviceActionResultStatus;
	}

	public void setDeviceActionResultStatus(SanityCheckUtil.DeviceActionResultStatus deviceActionResultStatus) {
		this.deviceActionResultStatus = deviceActionResultStatus;
	}

	/**
	 * @return the serialNumber
	 */
	public String getSerialNumber() {
		return serialNumber;
	}
	/**
	 * @param serialNumber the serialNumber to set
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	/**
	 * @return the externalIpaddress
	 */
	public String getExternalIpaddress() {
		return externalIpaddress;
	}
	/**
	 * @param externalIpaddress the externalIpaddress to set
	 */
	public void setExternalIpaddress(String externalIpaddress) {
		this.externalIpaddress = externalIpaddress;
	}
	
}

