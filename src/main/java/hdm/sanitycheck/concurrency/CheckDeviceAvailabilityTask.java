/**
 * 
 */
package hdm.sanitycheck.concurrency;

import hdm.sanitycheck.database.Device;
import hdm.sanitycheck.main.SanityCheckUtil;

import java.rmi.RemoteException;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import com.alcatel.hdm.service.nbi.client.NBIServicePort;
import com.alcatel.hdm.service.nbi.dto.NBIFunction;
import com.alcatel.hdm.service.nbi.dto.NBISingleDeviceOperationOptions;

/**
 * @author ozan
 *
 */
public class CheckDeviceAvailabilityTask extends SingleDeviceOperation implements Callable<Device> {
	
	private final static Logger  logger = Logger.getLogger(CheckDeviceAvailabilityTask.class);
	
	private NBIServicePort nbiServicePort;
	private hdm.sanitycheck.database.Device device;
	
	public CheckDeviceAvailabilityTask(NBIServicePort nbiPort, hdm.sanitycheck.database.Device device){
		this.nbiServicePort=nbiPort;
		this.device=device;
	}

	public Device call() throws RemoteException {
		
		NBIFunction nbiFunction = new NBIFunction();
		nbiFunction.setFunctionCode(SanityCheckUtil.RemoteCall.CDA.getCode());
			
		NBISingleDeviceOperationOptions nbiOptions = new NBISingleDeviceOperationOptions();
		nbiOptions.setExecutionTimeoutSeconds(getExecutionTimeout());
		nbiOptions.setExpirationTimeoutSeconds(getExpirationTimeout());
		nbiOptions.setPriority(getPriority());
		nbiOptions.setPolicyClass(getPolicyClass());
		nbiOptions.setDisableCaptureConstraint(isDisableCaptureConstraint());
		
		/*
		 * in case the sdo crashes, it is logged.
		 */
		Long operationId=null;
		try {
			
			operationId = (Long) nbiServicePort.createSingleDeviceOperationByDeviceGUID(device.getDeviceId(), nbiFunction, nbiOptions);
			
		} catch (Exception e) {
			
			logger.error("CDA couldn't be submitted to device "+device.getDeviceId(),e);
			
		}
		
		logger.debug("operation result id: "+operationId);
		
		device.setOperationResultId(operationId);
		
		return device;
	}

}
