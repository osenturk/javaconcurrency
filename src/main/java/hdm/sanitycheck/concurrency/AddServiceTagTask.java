/**
 * 
 */
package hdm.sanitycheck.concurrency;

import com.alcatel.hdm.service.nbi.client.NBIServicePort;
import com.alcatel.hdm.service.nbi.dto.NBIServiceTag;
import hdm.sanitycheck.database.Device;
import hdm.sanitycheck.main.SanityCheckUtil;
import org.apache.log4j.Logger;

import javax.xml.rpc.soap.SOAPFaultException;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * @author ozan
 *
 */
public class AddServiceTagTask  implements Callable<Device> {
	
	private final static Logger  logger = Logger.getLogger(AddServiceTagTask.class);
	
	private NBIServicePort nbiServicePort;
	private hdm.sanitycheck.database.Device device;
	private NBIServiceTag[] nbiServiceTagArray;

	private Set<String> messageSet;
	
	
	public AddServiceTagTask(NBIServicePort nbiPort, hdm.sanitycheck.database.Device device,NBIServiceTag[] nbiServiceTagArray){
		this.nbiServicePort=nbiPort;
		this.device=device;
		this.nbiServiceTagArray=nbiServiceTagArray;
	}

	public Device call() throws RemoteException {
		
					
		/*
		 * in case the sdo crashes, it is logged.
		 */
		
//		successful result

		SanityCheckUtil.DeviceActionResultStatus deviceActionResultStatus = SanityCheckUtil.DeviceActionResultStatus.SUCCESS;
		try {



				nbiServicePort.mergeServiceTagsByDeviceGUID(device.getDeviceId(), nbiServiceTagArray);




		} catch (java.rmi.RemoteException remoteException) {


			/*
				filter caught exception and proceed release device
			 */
			messageSet = new HashSet<String>();
			messageSet.add("Message did not contain a valid Security Element");
			messageSet.add("UsernameToken not provided");

			Throwable throwable = remoteException.getCause();


			try {

				if (throwable instanceof SOAPFaultException) {

					if(!messageSet.contains(((SOAPFaultException) throwable).getFaultString())){

						logger.error(" Adding service tag retried for device id "+device.getDeviceId());

						nbiServicePort.mergeServiceTagsByDeviceGUID(device.getDeviceId(), nbiServiceTagArray);


					}
				}else{
					logger.error("add service tag failed for device id: "+device.getDeviceId()+" throw exception"+remoteException);
					deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.EXCEPTION;
				}
			} catch (Exception e2) {
				logger.error("add service tag operation unsuccessful for device, "+device.toString(),e2);

				deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.EXCEPTION;

			}finally {
				messageSet=null;
			}


		}catch (Exception e3) {
			logger.error("add service tag operation unsuccessful for device, "+device.toString(),e3);
			deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.EXCEPTION;

		}finally {
			messageSet=null;
		}
		
		logger.debug("operation result "+deviceActionResultStatus);
		
		device.setDeviceActionResultStatus(deviceActionResultStatus);
		
		return device;
	}

}
