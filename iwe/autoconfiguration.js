importPackage(Packages.com.alcatel.hdm.jcf)
importPackage(Packages.alamotv.hdm.functions.types.request)
importPackage(Packages.alamotv.hdm.functions.types.struct)

/** error codes */
var AUTOCONFIGURE_CR_CREDENTIALS_FAILED_ERROR = 1;

/**
Gets device-info from Inform message, generates CR credentials and
uses SPV to set them.
**/
function autoconfigureCRCredentials() {
	var username = generateUniqueUsername();
	var password = generateUniquePassword ();
	var setParameterValuesRequest = new SetParameterValuesDTO();
	var parameterValueStructDTO = new Array(2);
	parameterValueStructDTO[0] = new ParameterValueStructDTO();
	parameterValueStructDTO[0].setName(_inform.getRoot() + ".ManagementServer.ConnectionRequestUsername");
	parameterValueStructDTO[0].setType(SetParameterValuesDTO.TR69_TYPE_STRING);
	parameterValueStructDTO[0].setValue(username);
	parameterValueStructDTO[1] = new ParameterValueStructDTO();
	parameterValueStructDTO[1].setName(_inform.getRoot() + ".ManagementServer.ConnectionRequestPassword");
	parameterValueStructDTO[1].setType(SetParameterValuesDTO.TR69_TYPE_STRING);
	parameterValueStructDTO[1].setValue(password);
	setParameterValuesRequest.setParameterValueStructs(parameterValueStructDTO);

	var primitiveResult = execPrimitive("setParameterValues",setParameterValuesRequest);

	if( !primitiveResult.isSuccess() ) {
		return new JCFResult(AUTOCONFIGURE_CR_CREDENTIALS_FAILED_ERROR, null, null);
	}
	_log.debug("autoConfigureCRCredentials succeeded");

	return primitiveResult;
}

function generateUniqueUsername()
{
    deviceId = _inform.getDeviceID();
    var username = new java.lang.StringBuffer();

    username.append(deviceId.getOUI());
    if (deviceId.getProductClass() != null)
        {
            username.append("-");
            username.append(deviceId.getProductClass());
        }
    username.append("-");
    username.append(deviceId.getSerialNumber());

    var uname = username.toString();
    uname = uname.replaceAll("\\s+", "");

    return uname;
}

function generateUniquePassword()
{
 	var deviceId = _inform.getDeviceID();
    var rawPassword = new java.lang.StringBuffer();
    var hashText = null;

    rawPassword.append(deviceId.getOUI());
    if (deviceId.getProductClass() != null)
    {
        rawPassword.append(deviceId.getProductClass());
    }
    rawPassword.append(deviceId.getSerialNumber());
    rawPassword.append(java.lang.Long.toString(new java.util.Random().nextLong()));

    var rPassword = rawPassword.toString();
    rPassword = rPassword.replaceAll("\\s+", "");

    return rPassword;
 }