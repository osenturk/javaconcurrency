package hdm.sanitycheck.database;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by elf on 11/18/15.
 * kkk
 */
public class DeviceType {

    protected long deviceTypeId;
    protected String name;
    protected String OUI;
    protected String PC;
    protected String model;
    protected String manufacturer;
    protected String tech;
    protected String softwareVersion;
    protected String deviceProtocol;

    public String getSoftwareVersion() {
        return softwareVersion;
    }

    public void setSoftwareVersion(String softwareVersion) {
        this.softwareVersion = softwareVersion;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    protected long total;


    public String getDeviceProtocol() {
        return deviceProtocol;
    }

    public void setDeviceProtocol(String deviceProtocol) {
        this.deviceProtocol = deviceProtocol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDeviceTypeId() {
        return deviceTypeId;
    }

    public void setDeviceTypeId(long deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    public String getOUI() {
        return OUI;
    }

    public void setOUI(String OUI) {
        this.OUI = OUI;
    }

    public String getPC() {
        return PC;
    }

    public void setPC(String PC) {
        this.PC = PC;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getTech() {
        return tech;
    }

    public void setTech(String tech) {
        this.tech = tech;
    }


    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof DeviceType))
            return false;
        if (obj == this)
            return true;

        DeviceType dt = (DeviceType) obj;
        return new EqualsBuilder().
                // if deriving: appendSuper(super.equals(obj)).

                append(softwareVersion, dt.softwareVersion).
                append(manufacturer, dt.manufacturer).
                append(OUI, dt.OUI).
                append(PC, dt.PC).
                append(tech, dt.tech).
                append(model, dt.model).
                append(deviceTypeId, dt.deviceTypeId).
                append(name, dt.name).
                append(deviceProtocol,dt.deviceProtocol).

                isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()

                .append(this.softwareVersion)
                .append(this.PC)
                .append(this.OUI)
                .append(this.manufacturer)
                .append(this.model)
                .append(this.deviceTypeId)
                .append(this.tech)
                .append(this.name)
                .append(this.deviceProtocol)

                .toHashCode();
    }


    @Override
    public String toString() {
        return "DeviceType{ name=" + name +
                ", softwareversion=" + softwareVersion +
                ", total=" + total +
                ", devicetypeid=" + deviceTypeId +
                ", Product Class=" + PC +
                ", OUI=" + OUI +
                ", manufacturer=" + manufacturer +
                ", model=" + model +
                ", tech=" + tech +
                ", device protocol=" + deviceProtocol +
                "}";

    }
}
