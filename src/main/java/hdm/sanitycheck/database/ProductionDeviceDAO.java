/**
 * 
 */
package hdm.sanitycheck.database;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * @author ozan
 *
 */


//TODO DAOs should be divided into DEVICE and OPSDASHBOARD
public class ProductionDeviceDAO implements DeviceDAO {

	private final static Logger  logger = Logger.getLogger(ProductionDeviceDAO.class);

	private OracleDataSource ods;

	private String username;
	private String password;
	private String connectionURL;
	private int env;

	private final static String HOW_MANY_DAY_AGO="-1";



	public Connection createConnection() throws ClassNotFoundException, SQLException {

		Connection conn=null;

		if (ods==null) {
			synchronized (ProductionDeviceDAO.class) {
				if(ods==null)
					ods = DatasourceFactory.getDataSourceInstance(username, password, connectionURL);

				logger.info("Oracle Datasoucer created ");
			}
		}
		conn=ods.getConnection();
		logger.debug("connection created for "+ DAOFactory.DatabaseEnv.STC_PROD);

		return conn;
	}

	public void initDatabase() {

	}

	public void purgeDatabase() throws ClassNotFoundException, SQLException {

	}

	public void getSample() throws ClassNotFoundException, SQLException {

	}

	public ProductionDeviceDAO(String username, String password, String connectionURL) throws SQLException {

		this.connectionURL=connectionURL;
		this.password=password;
		this.username=username;


	}
	
	public Device getDevicesByDeviceId(long deviceId, int limit) throws SQLException, ClassNotFoundException {
		
		Device tmpDevice = null;

		Connection conn=null;
		PreparedStatement devices=null;
		ResultSet rset =null;
		
		String sql = "SELECT d.id, devicetype_id, subscriberid, d.lastcontacttime, d.softwareversion, d.serialnumber " +
					"from DEVICE d where d.activated=1 and d.deleted=0 and d.lastcontacttime is not null and d.id = ? order by lastcontacttime desc ";
		
		
		try {


			conn = createConnection();
			devices = conn.prepareStatement(sql);
			devices.setLong(1, deviceId);
			devices.setMaxRows(limit);
//			devices.setInt(2, limit);
			
			rset = devices.executeQuery();
			
	
			
			while (rset.next ()){
				
				tmpDevice = new Device();
				tmpDevice.setDeviceId(rset.getLong(1));
				tmpDevice.setDeviceTypeId(rset.getInt(2));
				tmpDevice.setSubscriberId(rset.getString(3));
				tmpDevice.setLastcontacttime(rset.getDate(4));
				tmpDevice.setSoftwareVersion(rset.getString(5));
				tmpDevice.setSerialNumber(rset.getString(6));
				
				
			}
			
		} catch (SQLException e) {

				if(conn!=null) {
					  try {

						  logger.error("Transaction rolled back", e);
						  conn.rollback();

					  } catch (SQLException e1) {
						  logger.error("Cannot access oracle database", e);
					  }

				   }
		}finally{
				if (rset!=null)
					rset.close();
				
				if (devices!=null)
					devices.close();
				
				if (conn!=null)
					conn.close();

				logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.STC_PROD);


		}
		
		
		
		return tmpDevice;
		
	}


//TODO always same devices fetched. how to make it new devices each time.

	public List<Device> getDevicesByDeviceTypeAndFirmware(DeviceType deviceType, int limit) throws SQLException, ClassNotFoundException {

		List<Device> list  = new ArrayList<Device>();

		PreparedStatement devices=null;
		ResultSet rset =null;
		Connection conn=null;

		/*
		" 1-subscriberid is not null" condition removed as this is device sanity check rather than customer device sanity check
		  2-lastcontacttime bigger than last 2 days added to ensure device is not one of silent
		 */
			String sql = "SELECT d.id, d.subscriberid, d.lastcontacttime, d.serialnumber, d.externalipaddress, " +
					   "dt.device_type_name, dt.OUI, dt.product_class,dt.model_name,dt.manufacturer " +
				" from DEVICE d, DEVICETYPE dt where d.devicetype_id=dt.id and d.activated=1 and d.deleted=0 " +
				"and d.lastcontacttime is not null and d.lastcontacttime > sysdate"+HOW_MANY_DAY_AGO +" "+
				" and dt.id = ? and d.softwareversion = ?  order by d.lastcontacttime desc";


		try {
			conn = createConnection();
			devices = conn.prepareStatement(sql);
			devices.setLong(1, deviceType.getDeviceTypeId());
			devices.setString(2, deviceType.getSoftwareVersion());
			devices.setMaxRows(limit);
//				devices.setInt(2, limit);

			rset = devices.executeQuery();

			Device tmpDevice;

			while (rset.next ()){

				tmpDevice = new Device();

				tmpDevice.setDeviceId(rset.getLong(1));
				tmpDevice.setSubscriberId(rset.getString(2));
				tmpDevice.setLastcontacttime(rset.getTimestamp(3));
				tmpDevice.setSerialNumber(rset.getString(4));
				tmpDevice.setExternalIpaddress(rset.getString(5));
				tmpDevice.setDeviceTypeName(rset.getString(6));

				tmpDevice.setOUI(rset.getString(7));
				tmpDevice.setPC(rset.getString(8));
				tmpDevice.setModel(rset.getString(9));
				tmpDevice.setManufacturer(rset.getString(10));

				tmpDevice.setDeviceTypeId(deviceType.getDeviceTypeId());
				tmpDevice.setDeviceProtocol(deviceType.getDeviceProtocol());

				list.add(tmpDevice);


			}

		} catch (SQLException e) {
			if(conn!=null) {
				  try {

					  logger.error("Transaction rolled back", e);
					  conn.rollback();

				  } catch (SQLException e1) {
					  logger.error("Cannot access oracle database", e);
				  }

			   }
		}finally{

				if (rset!=null)
					rset.close();

				if (devices!=null)
					devices.close();

				if (conn!=null)
					conn.close();

				logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.STC_PROD);


		}



		return list;

	}


	public List<DeviceType> getSoftwareVersionsByDeviceType(long deviceTypeId, int firmwareLimit, int theLeastNumberOfDevicesPerFirmware) throws SQLException, ClassNotFoundException {

		List<DeviceType> list  = new ArrayList<DeviceType>();

		PreparedStatement devices=null;
		ResultSet rset =null;
		Connection conn=null;


		String sql = "SELECT d.devicetype_id,d.softwareversion,dt.device_type_name,dp.NAMEMESSAGEKEY, count(d.softwareversion) as toplam " +
				"from DEVICE d, DEVICETYPE dt, DEVICEPROTOCOL dp " +
				"where d.devicetype_id=dt.id and dp.id=dt.protocol_id and d.activated=1 and d.deleted=0 and d.devicetype_id = ? " +
				" and d.lastcontacttime > sysdate"+HOW_MANY_DAY_AGO +" and d.lastcontacttime is not null " +
				"group by d.devicetype_id, d.softwareversion,dt.device_type_name,dp.NAMEMESSAGEKEY " +
				"having count(d.softwareversion) >= ? " +
				"order by d.devicetype_id asc, toplam desc ";


		try {
			conn = createConnection();
			devices = conn.prepareStatement(sql);
			devices.setLong(1, deviceTypeId);
			devices.setMaxRows(firmwareLimit);
			devices.setInt(2,theLeastNumberOfDevicesPerFirmware);

			rset = devices.executeQuery();


			DeviceType dt;

			while (rset.next ()){

				dt = new DeviceType();

				dt.setDeviceTypeId(rset.getLong(1));
				dt.setSoftwareVersion(rset.getString(2));
				dt.setName(rset.getString(3));
				dt.setDeviceProtocol(rset.getString(4));
				dt.setTotal(rset.getInt(5));

				list.add(dt);

			}

		} catch (SQLException e) {

		    if(conn!=null) {
				  try {

					  logger.error("Transaction rolled back", e);
					  conn.rollback();

				  } catch (SQLException e1) {
					  logger.error("Cannot access oracle database", e);
				  }

			}
		}finally{

				if (rset!=null)
					rset.close();

				if (devices!=null)
					devices.close();

				if (conn!=null)
					conn.close();

			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.STC_PROD);

		}



		return list;
	}

	public DeviceType getDeviceTypeById(long deviceTypeId) throws SQLException, ClassNotFoundException {


		DeviceType dt = null;
		Connection conn=null;

		PreparedStatement devices=null;
		ResultSet rset =null;

		String sql = "SELECT " +
				"dt.id, dt.device_type_name, dt.OUI, dt.product_class,dt.model_name,dt.manufacturer " +
				"from DEVICETYPE dt " +
				"where dt.deleted=0 and dt.id = ? " +
				"order by dt.INSERTED desc ";


		try {

			conn = createConnection();
			devices = conn.prepareStatement(sql);
			devices.setLong(1, deviceTypeId);

			rset = devices.executeQuery();


			while (rset.next ()){

				dt = new DeviceType();
				dt.setDeviceTypeId(rset.getLong(1));
				dt.setName(rset.getString(2));
				dt.setOUI(rset.getString(3));
				dt.setPC(rset.getString(4));
				dt.setModel(rset.getString(5));
				dt.setManufacturer(rset.getString(6));


			}

		} catch (SQLException e) {

				if(conn!=null) {
					  try {

						  logger.error("Transaction rolled back", e);
						  conn.rollback();

					  } catch (SQLException e1) {
						  logger.error("Cannot access oracle database", e);
					  }

				}

		}finally{


				if (rset!=null)
					rset.close();

				if (devices!=null)
					devices.close();

				if (conn!=null)
					conn.close();

			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.STC_PROD);

		}



		return dt;

	}


	public long saveSummary(Summary summary) throws SQLException, ClassNotFoundException {

		PreparedStatement save=null;
		Connection conn=null;
		ResultSet generatedKeys =null;
		String generatedColumns[] = { "ID" };

		long summaryId=0;



		String sql = "INSERT INTO OPSDASH_SANITY_SUMMARY"
				+ "(ID," +
				"CREATIONDATE," +
				"ITERATION"+
				") " +
				"VALUES (OPSDASH_SANITYSUMMARY_SEQ.NEXTVAL,?,?)";


		try {


			conn =createConnection();
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			save = conn.prepareStatement(sql, generatedColumns);
			save.setTimestamp(1,new java.sql.Timestamp( summary.getCreationDate().getTime()));
			save.setLong(2, summary.getIteration());

			save.executeUpdate();

 			generatedKeys = save.getGeneratedKeys();

			if (generatedKeys.next()) {
				summaryId=generatedKeys.getLong(1);
			}
			else {
				throw new SQLException("Creating summary failed, no ID obtained.");
			}


			/*
					prepared statement more than 7 columns in oracle cause indexautofproblem
					that's why this workdaround added.*/

			summary.setId(summaryId);
			updateSummary(summary);

		} catch (SQLException e) {


			if(conn!=null) {
				  try {

					  logger.error("Transaction rolled back", e);
					  conn.rollback();

				  } catch (SQLException e1) {
					  logger.error("Cannot access oracle database", e);
				  }

			}

		}finally{

				if (save!=null)
					save.close();

				if (generatedKeys!=null)
					generatedKeys.close();

				if (conn!=null)
					conn.close();

			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.STC_PROD);

		}

		return   summaryId;
	}

	public void updateSummary(Summary summary) throws SQLException, ClassNotFoundException {


		PreparedStatement save = null;
		Connection conn = null;


		String updateSql = " UPDATE OPSDASH_SANITY_SUMMARY SET " +
				"SUCCESS_RATIO=?," +
				"SUCCESS=?, " +
				"FAILURE=?, " +
				"EXPIRATION_TIMEOUT=?, " +
				"EXECUTION_TIMEOUT=?, " +
				"SESSION_TIMEOUT=?, " +
				"PENDING=?, " +
				"ABORTED=?, " +
				"NORECORD=?, " +
				"EXCEPTION=?, " +
				"FIRMWARE=?, " +
				"OPERATION=?, " +
				"DEVICETYPE=?," +
				"DEVICETYPEID=?," +
				"PROTOCOL=?," +
				"SAMPLENUMBEROFDEVICES=?," +
				"TREND=?," +
				"TOTALNUMBEROFDEVICES=?" +
				"WHERE  ID=? ";

		try {

			conn = createConnection();
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			save = conn.prepareStatement(updateSql);

			save.setLong(1, summary.getSuccessratio());
			save.setLong(2, summary.getSuccess());
			save.setLong(3, summary.getFailure());
			save.setLong(4, summary.getExpirationTimeout());
			save.setLong(5, summary.getExecutionTimeout());
			save.setLong(6, summary.getSessionTimeout());
			save.setLong(7, summary.getPending());
			save.setLong(8, summary.getAborted());
			save.setLong(9, summary.getNorecord());
			save.setLong(10, summary.getException());
			save.setString(11, summary.getFirmware());
			save.setString(12, summary.getOperation());
			save.setString(13, summary.getDevicetype());
			save.setLong(14, summary.getDeviceTypeId());
			save.setString(15, summary.getProtocol());
			save.setLong(16, summary.getSampleNumberDevices());
			save.setLong(17, summary.getTrend());
			save.setLong(18, summary.getTotalNumberDevices());
			save.setLong(19, summary.getId());


			save.executeUpdate();




		} catch (SQLException e) {
			if(conn!=null){
				try {

					logger.error("Transaction rolled back",e);
					conn.rollback();

				} catch (SQLException e1) {
					logger.error("Cannot access oracle database",e);
				}

			}
		}
		finally {

			if (save != null)
				save.close();
			if (conn != null)
				conn.close();
			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.STC_PROD);

		}

	}

	public void saveDetail(List<Detail> detailList, int batchSize) throws SQLException, ClassNotFoundException {


		PreparedStatement save=null;
		Connection conn=null;

		String sql = "INSERT INTO OPSDASH_SANITY_DETAILS"
				+ "(ID," +
				"CREATIONDATE," +
				"RESULT," +
				"SUMMARYID,"+
				"OPERATIONRESULTID,"+
				"SERIALNUMBER,"+
				"SUBSCRIBER,"+
				"DEVICEID,"+
				"IPADDRESS,"+
				"LASTCONTACTTIME,"+
				"MANUFACTURER,"+
				"MODEL,"+
				"PC,"+
				"OUI"+
				") " +
				"VALUES (OPSDASH_SANITYDETAIL_SEQ.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,?)";



		try {

			conn =createConnection();
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			/*
			to make bulk insert
			 */
			int count=0;

			save = conn.prepareStatement(sql);

			for (Detail detail:detailList ) {

				save.setTimestamp(1,new java.sql.Timestamp( detail.getCreationtime().getTime()));

				save.setString(2, detail.getDeviceActionResultStatus().toString());
				save.setLong(3,detail.getSummaryId());
				if(detail.getOperationResultId()!=null)
					save.setLong(4,detail.getOperationResultId());
				else
					save.setLong(4,0);


				save.setString(5, detail.getSerialNumber());
				save.setString(6, detail.getSubscriberId());
				save.setLong(7, detail.getDeviceId());
				save.setString(8, detail.getExternalIpaddress());
				/*
				manual operations will have no device details
				 */
				if(detail.getLastcontacttime()!=null)
					save.setTimestamp(9, new java.sql.Timestamp(detail.getLastcontacttime().getTime()));
				else
					save.setTimestamp(9, null);

				save.setString(10,detail.getManufacturer());
				save.setString(11,detail.getModel());


				save.setString(12,detail.getPC());
				save.setString(13,detail.getOUI());

				save.addBatch();

				if(++count % batchSize == 0) {
					save.executeBatch();
				}
			}


			save.executeBatch();


		} catch (SQLException e) {

			if(conn!=null) {
				  try {

		    		  logger.error("Transaction rolled back", e);
					  conn.rollback();

				  } catch (SQLException e1) {
					  logger.error("Cannot access oracle database", e);
				  }

			}
		}finally{

				if (save!=null)
					save.close();

				if (conn!=null)
					conn.close();
			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.STC_PROD);

		}

	}

	public long saveIteration(Iteration iteration) throws SQLException, ClassNotFoundException {

		PreparedStatement save = null;
		Connection conn = null;

		String generatedColumns[] = { "ID" };

		ResultSet generatedKeys =null;

		String insertSql = "INSERT INTO OPSDASH_SANITY_ITERATION (ID,CREATIONTIME,SUCCESSRATIO,TOTALSAMPLE,USERNAME,OPERATIONTYPE,EXECUTIONTIME) " +
							"VALUES(OPSDASH_SANITYITERATION_SEQ.NEXTVAL,? ,?,?, ?,?,?) ";

		long iterationId=0;

		try {

			conn = createConnection();
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			save = conn.prepareStatement(insertSql,generatedColumns);

			save.setTimestamp(1, new java.sql.Timestamp(iteration.getCreationTime().getTime()));
			save.setInt(2, iteration.getSuccessRatio());
			save.setLong(3, iteration.getTotalSamplesPerIteration());
			save.setString(4, iteration.getUsername());
			save.setString(5, iteration.getOperationType());
			save.setLong(6, iteration.getExecutionDuration());

			save.executeUpdate();


			 generatedKeys = save.getGeneratedKeys();

			if (generatedKeys.next()) {
				iterationId=generatedKeys.getLong(1);
			}
			else {
				throw new SQLException("Creating iteration failed, no ID obtained.");
			}

		} catch (SQLException e) {

			  if(conn!=null) {
				  try {

					  logger.error("Transaction rolled back", e);
					  conn.rollback();

				  } catch (SQLException e1) {
					  logger.error("Cannot access oracle database", e);
				  }

			  }
		}finally{

				if (save!=null)
					save.close();
				if (conn!=null)
					conn.close();
				if (generatedKeys!=null)
					generatedKeys.close();
			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.STC_PROD);

		}
		return iterationId;
	}

	public void updateIteration(Iteration iteration) throws SQLException, ClassNotFoundException {

		PreparedStatement save = null;
		Connection conn = null;

		String updateSql = " UPDATE OPSDASH_SANITY_ITERATION SET " +
				 			"SUCCESSRATIO=?," +
							"TOTALSAMPLE=?," +
							"USERNAME= ?, " +
							"OPERATIONTYPE= ?, " +
							"EXECUTIONTIME= ? " +
				 			"WHERE  ID=? ";

		try {

		conn = createConnection();
		conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		save = conn.prepareStatement(updateSql);

		save.setInt(1, iteration.getSuccessRatio());
		save.setLong(2, iteration.getTotalSamplesPerIteration());
		save.setString(3, iteration.getUsername());
		save.setString(4, iteration.getOperationType());
		save.setLong(5, iteration.getExecutionDuration());
		save.setLong(6, iteration.getId());

		save.executeUpdate();


		} catch (SQLException e) {
		             if(conn!=null){
						 try {

							 logger.error("Transaction rolled back",e);
							 conn.rollback();

						 } catch (SQLException e1) {
							 	logger.error("Cannot access oracle database",e);
						 }

					 }
			}
		finally {

				if (save != null)
					save.close();
				if (conn != null)
					conn.close();
			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.STC_PROD);

		}


	}



	public Iteration getIterationById(long id) throws SQLException, ClassNotFoundException {


		Iteration iteration = null;
		Connection conn=null;

		PreparedStatement iterationStmt=null;
		ResultSet rset =null;

		String sql = "SELECT ID,CREATIONTIME,SUCCESSRATIO,TOTALSAMPLE,USERNAME,OPERATIONTYPE,EXECUTIONTIME  " +
				     "FROM OPSDASH_SANITY_ITERATION " +
				     "WHERE ID = ? ";


		try {

			conn = createConnection();
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			iterationStmt = conn.prepareStatement(sql);
			iterationStmt.setLong(1, id);

			rset = iterationStmt.executeQuery();

			while (rset.next ()){

				iteration = new Iteration();
				iteration.setId(rset.getLong(1));
				iteration.setCreationTime(rset.getTimestamp(2));
				iteration.setSuccessRatio(rset.getInt(3));
				iteration.setTotalSamplesPerIteration(rset.getLong(4));
				iteration.setUsername(rset.getString(5));
				iteration.setOperationType(rset.getString(6));
				iteration.setExecutionDuration(rset.getLong(7));

			}

		} catch (SQLException e) {

			if(conn!=null) {
				try {

					logger.error("Transaction rolled back", e);
					conn.rollback();

				} catch (SQLException e1) {
					logger.error("Cannot access oracle database", e);
				}

			}

		}finally{


			if (rset!=null)
				rset.close();

			if (iterationStmt!=null)
				iterationStmt.close();

			if (conn!=null)
				conn.close();

			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.STC_PROD);



		}



		return iteration;

	}


	public Summary getSummaryById(long id) throws SQLException, ClassNotFoundException {


		Summary summary = null;
		Connection conn=null;

		PreparedStatement summaryStmt=null;
		ResultSet rset =null;

		String sql = "SELECT ID,CREATIONDATE,DEVICETYPE,DEVICETYPEID,FIRMWARE,OPERATION, " +
					" SUCCESS_RATIO, SAMPLENUMBEROFDEVICES,TOTALNUMBEROFDEVICES,SUCCESS, FAILURE, " +
					" EXPIRATION_TIMEOUT,EXECUTION_TIMEOUT,SESSION_TIMEOUT,PENDING,ABORTED," +
					" NORECORD,EXCEPTION,ITERATION,TREND, PROTOCOL " +
					" FROM OPSDASH_SANITY_SUMMARY " +
					" WHERE ID = ? ";

		try {

			conn = createConnection();
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			summaryStmt = conn.prepareStatement(sql);
			summaryStmt.setLong(1, id);

			rset = summaryStmt.executeQuery();

			while (rset.next ()){

				summary = new Summary();
				summary.setId(rset.getLong(1));
				summary.setCreationDate(rset.getDate(2));
				summary.setDevicetype(rset.getString(3));
				summary.setDeviceTypeId(rset.getLong(4));
				summary.setFirmware(rset.getString(5));
				summary.setOperation(rset.getString(6));
				summary.setSuccessratio(rset.getLong(7));
				summary.setSampleNumberDevices(rset.getLong(8));
				summary.setTotalNumberDevices(rset.getLong(9));
				summary.setSuccess(rset.getLong(11));
				summary.setFailure(rset.getLong(12));
				summary.setExpirationTimeout(rset.getLong(13));
				summary.setExecutionTimeout(rset.getLong(14));
				summary.setSessionTimeout(rset.getLong(15));
				summary.setPending(rset.getLong(16));
				summary.setAborted(rset.getLong(17));
				summary.setNorecord(rset.getLong(18));
				summary.setException(rset.getLong(19));
				summary.setIteration(rset.getLong(20));
				summary.setProtocol(rset.getString(21));

			}

		} catch (SQLException e) {

			if(conn!=null) {
				try {

					logger.error("Transaction rolled back", e);
					conn.rollback();

				} catch (SQLException e1) {
					logger.error("Cannot access oracle database", e);
				}

			}

		}finally{


			if (rset!=null)
				rset.close();

			if (summaryStmt!=null)
				summaryStmt.close();

			if (conn!=null)
				conn.close();

			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.STC_PROD);



		}

		return summary;

	}



}

