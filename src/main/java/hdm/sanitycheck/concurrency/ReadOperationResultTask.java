/**
 * 
 */
package hdm.sanitycheck.concurrency;

import com.alcatel.hdm.service.nbi.client.NBIServicePort;
import com.alcatel.hdm.service.nbi.dto.NBIOperationStatus;
import hdm.sanitycheck.database.Device;
import hdm.sanitycheck.main.SanityCheckUtil;
import hdm.sanitycheck.main.SanityCheckUtil.OperationResult;
import org.apache.log4j.Logger;

import javax.xml.rpc.soap.SOAPFaultException;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * @author ozan
 *
 */
public class ReadOperationResultTask implements Callable<Device> {

	private final static Logger  logger = Logger.getLogger(ReadOperationResultTask.class);
	
	private NBIServicePort nbiServicePort;
	private hdm.sanitycheck.database.Device device;
	
	private SanityCheckUtil.DeviceActionResultStatus deviceActionResultStatus;

	private Set<String> messageSet;

	private final int RETRY_COUNT=4;
	
	public ReadOperationResultTask(NBIServicePort nbiPort, hdm.sanitycheck.database.Device device){
		this.nbiServicePort=nbiPort;
		this.device=device;
	}

	public Device call() throws RemoteException {
		
		
		NBIOperationStatus nbiOperationStatus=null;
		int retrySingleDeviceOperation=0;

		try {

			if(device.getOperationResultId()!=null) {


				do{

					nbiOperationStatus = nbiServicePort.getOperationStatus(device.getOperationResultId());

					if(retrySingleDeviceOperation>0) {
					/*
					one second wait for each submission
					 */
						SanityCheckUtil.getSleep(1);
						logger.error("Retrieving device action result retried " + retrySingleDeviceOperation + " time(s) for " + device.getDeviceId()+ "device operation result id "+device.getOperationResultId());

					}

				}while ((nbiOperationStatus==null)&&(retrySingleDeviceOperation++<RETRY_COUNT)) ;



				if (nbiOperationStatus != null) {

					logger.debug("operation status code is "+nbiOperationStatus.getStatus()+" for "+device.getDeviceId()+" device operation result id "+device.getOperationResultId());
					deviceActionResultStatus = getOperationResult(nbiOperationStatus);

				} else {

					logger.debug("Device Operation Result is null for " + device.getDeviceId() + " device operation result id " + device.getOperationResultId());
					deviceActionResultStatus = SanityCheckUtil.DeviceActionResultStatus.NORECORD;

				}

			}else{
				logger.debug("Operation Result is null for "+device.toString());
				deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.NORECORD;
			}
				

		} catch ( java.rmi.RemoteException remoteException) {

			/*
			filter caught exception and proceed release device
 			*/
			messageSet = new HashSet<String>();
			messageSet.add("Message did not contain a valid Security Element");
			messageSet.add("No Operation Result Data could be found");
			messageSet.add("UsernameToken not provided");

			Throwable throwable = remoteException.getCause();

			try {

				if (throwable instanceof SOAPFaultException) {

					if(!messageSet.contains(((SOAPFaultException) throwable).getFaultString())){

						logger.error(" read operation failed, retry one more time meanwhile check ids of device and operation: "+device.getDeviceId()+", "+device.getOperationResultId());

							SanityCheckUtil.getSleep(1);
							nbiOperationStatus = nbiServicePort.getOperationStatus(device.getOperationResultId());

						deviceActionResultStatus = getOperationResult(nbiOperationStatus);
						}
				}else{
					logger.error("device id: "+device.getDeviceId()+" throw exception for operation id "+device.getOperationResultId()+".."+remoteException);
					deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.EXCEPTION;
				}
			} catch (Exception e2) {
				try {
					logger.error("Read operation throwed exception after remote exception !");
					SanityCheckUtil.getSleep(1);
					nbiOperationStatus = nbiServicePort.getOperationStatus(device.getOperationResultId());
					logger.debug("After remote exception one more try: read operation "+nbiOperationStatus.getStatus()+" for "+device.getDeviceId()+" device operation result id "+device.getOperationResultId());
					deviceActionResultStatus = getOperationResult(nbiOperationStatus);
				}catch (Exception e){
					logger.error("after remote exception one more try also failed for "+device.getDeviceId()+" operationresultid "+device.getOperationResultId()+" "+ e.getMessage());

					deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.EXCEPTION;
				}

			}finally {
				messageSet=null;
			}


		}catch (Exception e3) {

			try {
				logger.error("Read operation throwed exception and second try now!");
				SanityCheckUtil.getSleep(1);
				nbiOperationStatus = nbiServicePort.getOperationStatus(device.getOperationResultId());
				logger.debug("second try: read operation "+nbiOperationStatus.getStatus()+" for "+device.getDeviceId()+" device operation result id "+device.getOperationResultId());
				deviceActionResultStatus = getOperationResult(nbiOperationStatus);
			}catch (Exception e){
				logger.error("after second try still failure for "+device.getDeviceId()+" operationresultid "+device.getOperationResultId()+" "+ e3.getMessage());

				deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.EXCEPTION;
			}



		}finally {
			messageSet=null;
		}



		device.setDeviceActionResultStatus(deviceActionResultStatus);
		
		return device;
	}

	private SanityCheckUtil.DeviceActionResultStatus getOperationResult(NBIOperationStatus nbiOperationStatus){

		SanityCheckUtil.DeviceActionResultStatus deviceActionResultStatus;

		if(nbiOperationStatus==null){
			deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.NORECORD;

		}else if (nbiOperationStatus.getStatus()==OperationResult.ABORTED.getCode()){

			deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.ABORTED;

		}else if (nbiOperationStatus.getStatus()==OperationResult.TIMEOUT.getCode()){

			deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.FAILURE;

		}else if (nbiOperationStatus.getStatus()==OperationResult.SUCCESS.getCode()){

			deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.SUCCESS;

		}else if (nbiOperationStatus.getStatus()==OperationResult.PENDING.getCode()){

			deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.PENDING;

		}else{

			deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.NORECORD;

		}

		return deviceActionResultStatus;
	}

	
}
