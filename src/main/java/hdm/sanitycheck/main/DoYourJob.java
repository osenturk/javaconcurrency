/**
 * 
 */
package hdm.sanitycheck.main;

import hdm.sanitycheck.database.Device;
import hdm.sanitycheck.database.Iteration;
import hdm.sanitycheck.database.Summary;
import hdm.sanitycheck.main.SanityCheckUtil.RemoteCall;
import org.apache.log4j.Logger;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @author ozan
 *
 */
public class DoYourJob {


	private final static Logger  logger = Logger.getLogger(DoYourJob.class);
	
	private final static String filePath ="./files/";

	public static void main(String[] args) {

		Map<String, String> parameterMap = SanityCheckUtil.loadParameters();

		long startTime;
		long finishTime;

		Iteration mainIteration=null;

		if(!SanityCheckUtil.validateCommandLineParameter(args)) {
			logger.error("command line parameters missed!");
			System.exit(1);
		}

		RemoteCall remoteCall=SanityCheckUtil.convertStringToRemoteCall(args[0]);
		logger.info("remote call code is "+remoteCall);

		SanityCheckUtil.appendParameter(parameterMap,SanityCheckUtil.PARAMETER_IS_MANUAL, args[1] );

		
		Date date = Calendar.getInstance().getTime();
		
		startTime=date.getTime();
		logger.debug("started .. "+startTime);


		List<Device> deviceIdList = null;


		boolean isManual =false;
		isManual =Boolean.parseBoolean(parameterMap.get(SanityCheckUtil.PARAMETER_IS_MANUAL));


		if (isManual){
			
			try {
				deviceIdList = SanityCheckUtil.convertFileToDevice(filePath);
				
			} catch (IOException e) {

				e.printStackTrace();
				System.exit(1);
			}

			
			List<Future<Device>> deviceOperationList=null;
			try {
				

				int functionCode = remoteCall.getCode();
							
				switch(functionCode){
				
				
				case SanityCheckUtil.FUNCTIONCODE_MERGE_SERVICETAGS: {
					deviceOperationList = SanityCheckUtil.addServiceTags(deviceIdList, parameterMap);
					logger.info(remoteCall + " started ");
					break;
				}
				case SanityCheckUtil.FUNCTIONCODE_REMOVE:{
					
					Scanner ob=new Scanner(System.in);
					System.out.println(" Are you sure to delete devices, type 'y' or 'n':");
					
							if(ob.hasNextLine()){
									if(ob.nextLine().equalsIgnoreCase(SanityCheckUtil.OPTION_YES)){
										
										deviceOperationList = SanityCheckUtil.removeDevices(deviceIdList, parameterMap);
										logger.info(remoteCall+" started ");
										break;
										
									}else{
										
										logger.error(" devices will not be deleted, to delete device you should enter 'y', try again ");
										System.exit(1);
										
									}
									
							}else{
								logger.error(" devices will not be deleted, to delete device you should enter 'y', try again ");
								System.exit(1);
							}
					
						
						
					}

				default: {
					deviceOperationList = SanityCheckUtil.sendSingleDeviceOperation(deviceIdList, parameterMap,remoteCall);
					logger.info(remoteCall+" started ");
					break;
				}
			}
				
				
			}catch (IOException e) {
				
				logger.error("IOException occured",e);
				System.exit(1);
			} catch (ServiceException e) {
				
				logger.error("nbi related problem occurred",e);
				System.exit(1);
			}

			Map<Summary,List<Future>> resultMap= null;
			
			try {

				resultMap = SanityCheckUtil.getOperationResultPerFile(deviceOperationList, parameterMap, remoteCall);
				
			} catch (IOException e) {

				logger.error("IOException occured",e);
				System.exit(1);
			} catch (ServiceException e) {
				logger.error("nbi related problem occurred",e);
				System.exit(1);
			} catch (InterruptedException e) {

				e.printStackTrace();
				System.exit(1);
			} catch (ExecutionException e) {

				e.printStackTrace();
				System.exit(1);
			}


			List<Summary> summaryList=null;


			try {
				summaryList = SanityCheckUtil.saveDetails(parameterMap, resultMap,remoteCall);

				logger.debug("details completed.");

				resultMap.clear();

			}  catch (SQLException e) {
				e.printStackTrace();
				System.exit(1);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				System.exit(1);
			}

			try {

				mainIteration=SanityCheckUtil.saveSummary(parameterMap, summaryList, remoteCall);

				if(summaryList!=null)
					summaryList.clear();

			} catch (SQLException e) {
				e.printStackTrace();
				System.exit(1);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				System.exit(1);
			}

			logger.info("Single Device Operation completed successfully");
			

//			if results are retrieved from database else block is executed
		}else{



			Map<Summary,List<Device>> deviceNameMap=null;
			Map<Summary,List<Future<Device>>> sdoMap;



			try {

				deviceNameMap = SanityCheckUtil.getAllDevicesPerDeviceTypeAndFirmware(parameterMap);
			} catch (InterruptedException e) {
				
				logger.error("InterruptedException occurred",e);
				System.exit(1);

			} catch (ExecutionException e) {
				logger.error("ExecutionException occurred",e);
				System.exit(1);

			} catch (SQLException e) {
				logger.error("SQLException occurred",e);
				System.exit(1);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				System.exit(1);
			}


			Map<Summary, List<Future>> resultMap=new HashMap<Summary, List<Future>>();
			try {

				sdoMap= SanityCheckUtil.sendSingleDeviceOperation(deviceNameMap,parameterMap,remoteCall);

				resultMap = SanityCheckUtil.getDeviceActionResultPerSoftware( sdoMap, parameterMap,remoteCall);



				sdoMap.clear();
				if (deviceNameMap!=null)
					deviceNameMap.clear();
				
			} catch (InterruptedException e) {

				logger.error("InterruptedException occured",e);
				System.exit(1);
			} catch (ExecutionException e) {

				logger.error("execution exception occured",e);
				System.exit(1);
			} catch (IOException e) {
				
				logger.error("io exception occured",e);
				System.exit(1);
				
			} catch (ServiceException e) {

				logger.error("nbi related problem occurred",e);
				System.exit(1);
				
			}
			
			List<Summary> summaryList=null;


			try {
				summaryList = SanityCheckUtil.saveDetails(parameterMap, resultMap,remoteCall);

				logger.debug("details completed.");
			
				resultMap.clear();
			
			}  catch (SQLException e) {
				e.printStackTrace();
				System.exit(1);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				System.exit(1);
			}

			try {
				
				mainIteration=SanityCheckUtil.saveSummary(parameterMap, summaryList, remoteCall);

				if(summaryList!=null)
					summaryList.clear();
				
			}catch (SQLException e) {
				e.printStackTrace();
				System.exit(1);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				System.exit(1);
			}


		}
		
		
		date = Calendar.getInstance().getTime();
		finishTime=date.getTime();
		logger.debug("endeded .. "+finishTime);
		
		try {
			SanityCheckUtil.createPerformanceSummary(parameterMap, startTime, finishTime,mainIteration);

		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}


		parameterMap.clear();


		logger.info(remoteCall.name()+ " completed successfully");
		/*
		gracefully exit!
		 */
		System.exit(0);
	}




	
}
