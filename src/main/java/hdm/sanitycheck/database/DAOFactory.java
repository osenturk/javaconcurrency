package hdm.sanitycheck.database;

import org.apache.log4j.Logger;
import org.jasypt.util.text.BasicTextEncryptor;

import java.sql.SQLException;

/**
 * Created by elf on 1/9/16.
 * kkk
 */
public class DAOFactory {

    public enum DatabaseEnv{
        STC_PROD(1), MEMORY_DEV(2);

        private int code;

        private DatabaseEnv(int c) {
            code = c;
        }

        public int getCode() {
            return code;
        }


    }


    private static DeviceDAO dao = null;

    private final static Logger logger = Logger.getLogger(DAOFactory.class);

    private DAOFactory() {}

    public static synchronized DeviceDAO getInstance(String username, String password, String URL, int switchToDatabase) throws SQLException {



        if(dao==null){

            synchronized (DAOFactory.class) {

                if(dao==null) {

                    if (DatabaseEnv.STC_PROD.getCode()==switchToDatabase) {
                        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
                        textEncryptor.setPassword("34567");

                        String usernameTxt = textEncryptor.decrypt(username);
                        String passwordTxt = textEncryptor.decrypt(password);
                        dao = new ProductionDeviceDAO( usernameTxt,  passwordTxt,  URL);

                    }else{
                        dao= new MemoryDAO(URL,"sa","");
                    }

                }
            }
        }
        return dao;
    }
}