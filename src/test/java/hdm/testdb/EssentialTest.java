package hdm.testdb;

import hdm.sanitycheck.database.DAOFactory;
import hdm.sanitycheck.database.DeviceDAO;
import hdm.sanitycheck.main.SanityCheckUtil;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by ozansenturk on 1/29/16.
 */
public class EssentialTest {

    private Map<String, String> parameterMap;
    DeviceDAO deviceDAO;

    private final static Logger logger = Logger.getLogger(EssentialTest.class);

    @Before
    public void setup() {

        parameterMap = SanityCheckUtil.loadParameters();

        try {
            deviceDAO = DAOFactory.getInstance(parameterMap.get(SanityCheckUtil.PARAMETER_DB_USERNAME),parameterMap.get(SanityCheckUtil.PARAMETER_DB_PASSWORD),parameterMap.get(SanityCheckUtil.PARAMETER_DB_CONNECTION_URL),Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_DB_TYPE)));

            deviceDAO.initDatabase();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @After
    public void cleanup() throws SQLException, ClassNotFoundException {
        parameterMap.clear();
        deviceDAO.purgeDatabase();
        deviceDAO=null;
    }

    @Test
    public void getDevice() throws SQLException, ClassNotFoundException {

        logger.info("i guess database connection established");

        Connection connection=deviceDAO.createConnection();
        assertTrue("connection cannot be created",connection!=null);
    }


    @Test
    public void getSampleData()  {

        logger.info("i guess database connection established");

        try {
            deviceDAO.getSample();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        logger.debug("database created and one record retrieved");
    }
}
