/**
 * 
 */
package hdm.sanitycheck;

import hdm.sanitycheck.database.DAOFactory;
import hdm.sanitycheck.database.Device;
import hdm.sanitycheck.database.DeviceDAO;
import hdm.sanitycheck.database.Summary;
import hdm.sanitycheck.main.SanityCheckUtil;
import hdm.sanitycheck.main.SanityCheckUtil.OperationResult;
import hdm.sanitycheck.main.SanityCheckUtil.RemoteCall;
import org.apache.log4j.Logger;
import org.jasypt.util.password.ConfigurablePasswordEncryptor;
import org.jasypt.util.text.BasicTextEncryptor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.crypto.Cipher;
import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static junit.framework.TestCase.assertTrue;

/**
 * @author ozan
 *
 */

public class TestSanityCheckUtil {
	

	private final static Logger  logger = Logger.getLogger(TestSanityCheckUtil.class);
	private final static String filePath ="./files/"; 
		
	private Map<String, String> parameterMap;

	private Map<Summary,List<Device>> deviceNameMap;

	private Map<Summary,List<Future>> resultMap=new HashMap<Summary, List<Future>>();
	private List<Summary> summaryList;


	private int limit;
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp() throws Exception {

		parameterMap = SanityCheckUtil.loadParameters();

		limit = Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_DB_FETCH_LIMIT_PER_DT));
	
			
		
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	@After
	public void tearDown() throws Exception {
		parameterMap=null;


		resultMap=null;
		summaryList=null;
			
		limit=0;
	}

	@Test
	public void testMergeIpsAndPorts(){
	
		List<String> list = SanityCheckUtil.mergeIpsAndPorts(parameterMap.get(SanityCheckUtil.PARAMETER_NBI_IPS), parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PORTS));
		
		assertTrue(list!=null);
		assertTrue(list.size()>0);
		
		for (String string : list) {
			assertTrue("nbi url addresses cannot be null",string!=null);
			assertTrue("nbi url addresses cannot be empty string",string.length()>0);
			logger.debug("nbi url addresses: "+string);
		}
		
	}

	@Test
	public void testLoadParameters(){
				
		assertTrue("nbi ips cannot be null", parameterMap.get(SanityCheckUtil.PARAMETER_NBI_IPS)!=null);
		assertTrue("nbi ips cannot be empty string", parameterMap.get(SanityCheckUtil.PARAMETER_NBI_IPS).length()>0);
		
		assertTrue("nbi ports cannot be null", parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PORTS)!=null);
		assertTrue("nbi ports cannot be empty string", parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PORTS).length()>0);
		
		assertTrue("nbi username cannot be null", parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME)!=null);
		assertTrue("nbi username cannot be empty string", parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME).length()>0);
		
		assertTrue("nbi password cannot be null", parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD)!=null);
		assertTrue("nbi password cannot be empty string", parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD).length()>0);
				
		assertTrue("db connection url cannot be null", parameterMap.get(SanityCheckUtil.PARAMETER_DB_CONNECTION_URL)!=null);
		assertTrue("db connection url cannot be empty string", parameterMap.get(SanityCheckUtil.PARAMETER_DB_CONNECTION_URL).length()>0);
		
		assertTrue("devicetypes cannot be null", parameterMap.get(SanityCheckUtil.PARAMETER_DEVICETYPES)!=null);
		assertTrue("devicetypes cannot be empty string", parameterMap.get(SanityCheckUtil.PARAMETER_DEVICETYPES).length()>0);
		
		assertTrue("fetch limit per dt cannot be null", parameterMap.get(SanityCheckUtil.PARAMETER_DB_FETCH_LIMIT_PER_DT)!=null);
		assertTrue("fetch limit per dt cannot be empty string", parameterMap.get(SanityCheckUtil.PARAMETER_DB_FETCH_LIMIT_PER_DT).length()>0);
		
		assertTrue("sdo exeecution time cannot be null", parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTION_TIMEOUT)!=null);
		assertTrue("sdo exeecution time cannot be empty string", parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTION_TIMEOUT).length()>0);
		
		assertTrue("sdo expiration time cannot be null", parameterMap.get(SanityCheckUtil.PARAMETER_EXPIRATION_TIMEOUT)!=null);
		assertTrue("sdo expiration time cannot be empty string", parameterMap.get(SanityCheckUtil.PARAMETER_EXPIRATION_TIMEOUT).length()>0);
		
		assertTrue("sdo priority cannot be null", parameterMap.get(SanityCheckUtil.PARAMETER_SDO_PRIORITY)!=null);
		assertTrue("sdo priority cannot be empty string", parameterMap.get(SanityCheckUtil.PARAMETER_SDO_PRIORITY).length()>0);
		
		assertTrue("sdo disable capture constraint cannot be null", parameterMap.get(SanityCheckUtil.PARAMETER_SDO_DISABLE_CAPTURE_CONSTRAINT)!=null);
		assertTrue("sdo disable capture constraint cannot be empty string", parameterMap.get(SanityCheckUtil.PARAMETER_SDO_DISABLE_CAPTURE_CONSTRAINT).length()>0);
		
		assertTrue("sdo policy class cannot be null", parameterMap.get(SanityCheckUtil.PARAMETER_SDO_POLICY_CLASS)!=null);
		assertTrue("sdo policy class cannot be empty string", parameterMap.get(SanityCheckUtil.PARAMETER_SDO_POLICY_CLASS).length()>0);
		
		assertTrue("nthreads cannot be null", parameterMap.get(SanityCheckUtil.PARAMETER_NTHREADS)!=null);
		assertTrue("nthreads cannot be empty string", parameterMap.get(SanityCheckUtil.PARAMETER_NTHREADS).length()>0);
		
		assertTrue("executor type cannot be null", parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTOR_TYPE)!=null);
		assertTrue("executor type cannot be empty string", parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTOR_TYPE).length()>0);
		

	}

	@Test
	public void testExtractDeviceTypes(){
		
		String deviceTypes[] = SanityCheckUtil.extractStringArrayFromPropertiesValue(parameterMap.get(SanityCheckUtil.PARAMETER_LAN_DEVICES),SanityCheckUtil.DELIMETER_COMMA);

		for (String devicetypes:deviceTypes)
			logger.debug(devicetypes);

		assertTrue("devicetypes size should be bigger than zero", deviceTypes.length>0);

		
	}

	@Test
	public void testRemoteCallEnumType(){
		
		int parameter = Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_REMOTECALL));
		
		if (RemoteCall.CDA.getCode()==parameter){
			logger.debug(RemoteCall.CDA+" executed");
		}else if (RemoteCall.GPV.getCode()==parameter){
			logger.debug(RemoteCall.GPV+" executed");
		}else if (RemoteCall.SPV.getCode()==parameter){
			logger.debug(RemoteCall.SPV+" executed");
		}else if (RemoteCall.REMOVE_DEVICE.getCode()==parameter){
			logger.debug(RemoteCall.REMOVE_DEVICE+" executed");
		}else{
			assertTrue("unknown remote call code: "+parameter,1==2);
		}
		
		for (RemoteCall rm : RemoteCall.values()) {
			logger.debug("remotecall name and value: "+ rm +" , "+rm.getCode());
		}
	}


	@Test
	public void testGetAllDevicesPerDeviceTypeAndSoftwareVersion() throws InterruptedException, ExecutionException, SQLException, ClassNotFoundException {

		Date date = Calendar.getInstance().getTime();

		long startTime=date.getTime();
		logger.debug("started .. "+startTime);

		int sayac =0;

		deviceNameMap = SanityCheckUtil.getAllDevicesPerDeviceTypeAndFirmware(parameterMap);

		assertTrue("cannot be null", deviceNameMap!=null);

		for(Map.Entry<Summary, List<Device>> entry : deviceNameMap.entrySet()){

			logger.debug("device type name: "+entry.getKey());

			List<Device> list = entry.getValue();

			sayac=sayac+list.size();

			logger.debug("number of devices "+list.size());

			assertTrue("device type should contain "+limit+ " but "+ list.size(),list.size()==limit);
		}

		logger.debug("total number of devices is "+sayac);

		date = Calendar.getInstance().getTime();
		long finishTime=date.getTime();
		logger.debug("endeded .. "+finishTime);

		logger.debug(SanityCheckUtil.calculateDuration(startTime,finishTime,1000));
	}


	@Test
	public void testGetOperationResult() throws InterruptedException, ExecutionException, IOException, ServiceException, SQLException, ClassNotFoundException {

		Map<Summary,List<Future<Device>>> sdoMap;
		deviceNameMap = SanityCheckUtil.getAllDevicesPerDeviceTypeAndFirmware(parameterMap);

		sdoMap = SanityCheckUtil.sendSingleDeviceOperation(deviceNameMap, parameterMap,RemoteCall.CDA);

		resultMap = SanityCheckUtil.getOperationResultPerSoftware(sdoMap, parameterMap,RemoteCall.CDA);

		assertTrue("cannot be null", sdoMap!=null);
		
		for(Map.Entry<Summary,List<Future>> entry : resultMap.entrySet()){
			
			List<Future> deviceList = entry.getValue();
			
			assertTrue("devicetype list size should be equal to the limit", deviceList.size()==limit);
			
			for (Future future: deviceList) {
				
				logger.debug("checking the result per device: "+future.get());
			}
			
		}
		
	}

	@Test
	public void testGetOperationResultPerFile() throws InterruptedException, ExecutionException, IOException, ServiceException{
		
		List<Device> deviceList = SanityCheckUtil.convertFileToDevice(filePath);
		
		List<Future<Device>> futureList=SanityCheckUtil.addServiceTags(deviceList, parameterMap);
		
		assertTrue("cannot be null ", futureList!=null);

		Map<Summary,List<Future>> operationResultMap = SanityCheckUtil.getOperationResultPerFile(futureList, parameterMap, RemoteCall.ADD_SERVICE_TAG);


		for (Map.Entry<Summary, List<Future>> entry : operationResultMap.entrySet()) {

			logger.debug("Summary Key: "+entry.getKey());

			for (Future future:entry.getValue()
				 ) {
				Device device = (Device) future.get();

				logger.debug("future is "+device);
			}

		}

		for(Summary summary : summaryList){

			logger.debug("summary= "+summary.toString());

		}


		
		
	}

	@Test
	public void testGetSleep() throws InterruptedException{
		
		Calendar c = Calendar.getInstance();
		long start = c.getTimeInMillis();
		
		int expirationTimeout=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXPIRATION_TIMEOUT));
		
		SanityCheckUtil.getSleep(expirationTimeout);
		
		c= Calendar.getInstance();
		
		long finish = c.getTimeInMillis();
		
		double sleepDuration = (finish-start)/1000;
		
		assertTrue("sleep time should be equal to expiration timeout"+sleepDuration+","+expirationTimeout,sleepDuration==expirationTimeout);
		
	}



	@Test
	public void testCreateDetails() throws InterruptedException, ExecutionException, IOException, ServiceException, SQLException, ClassNotFoundException  {

		Map<Summary,List<Future<Device>>> sdoMap;

		deviceNameMap = SanityCheckUtil.getAllDevicesPerDeviceTypeAndFirmware(parameterMap);

		sdoMap = SanityCheckUtil.sendSingleDeviceOperation(deviceNameMap, parameterMap,RemoteCall.CDA);
		
		resultMap = SanityCheckUtil.getOperationResultPerSoftware(sdoMap, parameterMap, RemoteCall.CDA);
		
		summaryList = SanityCheckUtil.saveDetails(parameterMap, resultMap,RemoteCall.CDA);
		
		for(Summary summary : summaryList){

			logger.debug("summary= "+summary.toString());

		}
			
		
	}

	@Test
	public void testSaveDetails() throws InterruptedException, ExecutionException, IOException, ServiceException, SQLException, ClassNotFoundException  {

		Map<Summary,List<Future<Device>>> sdoMap;

		deviceNameMap = SanityCheckUtil.getAllDevicesPerDeviceTypeAndFirmware(parameterMap);

		sdoMap = SanityCheckUtil.sendSingleDeviceOperation(deviceNameMap, parameterMap,RemoteCall.CDA);

		resultMap = SanityCheckUtil.getOperationResultPerSoftware(sdoMap, parameterMap, RemoteCall.CDA);

		summaryList = SanityCheckUtil.saveDetails(parameterMap, resultMap,RemoteCall.CDA);

		for(Summary summary : summaryList){

			logger.debug("summary= "+summary.toString());

		}


	}

	@Test
	public void testSaveSummaryIntoDatabase() throws InterruptedException, ExecutionException, IOException, ServiceException, SQLException, ClassNotFoundException  {

		Map<Summary,List<Future<Device>>> sdoMap;

		deviceNameMap = SanityCheckUtil.getAllDevicesPerDeviceTypeAndFirmware(parameterMap);

		sdoMap = SanityCheckUtil.sendSingleDeviceOperation(deviceNameMap, parameterMap,RemoteCall.CDA);

		resultMap = SanityCheckUtil.getOperationResultPerSoftware(sdoMap, parameterMap, RemoteCall.CDA);

		summaryList = SanityCheckUtil.saveDetails(parameterMap, resultMap,RemoteCall.CDA);

		SanityCheckUtil.saveSummary(parameterMap,summaryList,RemoteCall.CDA);

		logger.debug("saved");


	}

	@Test
	public void testSendSingleDeviceOperation() throws InterruptedException, ExecutionException, IOException, ServiceException, SQLException, ClassNotFoundException  {

		List<Device> deviceList = SanityCheckUtil.convertFileToDevice(filePath);

		List<Future<Device>> futureList=SanityCheckUtil.sendSingleDeviceOperation(deviceList, parameterMap, RemoteCall.CDA);

		assertTrue("cannot be null ", futureList!=null);

		Map<Summary,List<Future>> operationResultMap = SanityCheckUtil.getOperationResultPerFile(futureList, parameterMap, RemoteCall.CDA);



		summaryList = SanityCheckUtil.saveDetails(parameterMap, operationResultMap,RemoteCall.CDA);

		for(Summary summary : summaryList){

			logger.debug("summary= "+summary.toString());

		}

		SanityCheckUtil.saveSummary(parameterMap,summaryList,RemoteCall.CDA);

		logger.debug("saved");

	}


	@Test
	public void testGetToday() throws ParseException{
		
		String currentTimeStr=SanityCheckUtil.getTodayAsTime(DateFormat.MEDIUM,Locale.US);
		logger.debug(currentTimeStr);
		
		SimpleDateFormat sf = new SimpleDateFormat("HH");
		logger.debug(sf.format(Calendar.getInstance().getTime()));
		
		
		SimpleDateFormat sf1 = new SimpleDateFormat("dd");
		logger.debug(sf1.format(Calendar.getInstance().getTime()));
	}

	@Test
	public void testFormatDate() throws ParseException{
		
		String sampleDateStr="2014-10-05 12:34:09.207";
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
		Date date= sf.parse(sampleDateStr);
				
		
		
		String formattedDate=SanityCheckUtil.formatDate(date, DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.US);
		logger.debug(formattedDate);
		
	}

	@Test
	public void testSoftwareShorten(){
		String s = "V100R001B026 STC        ";
		
		logger.debug(s.substring(0, s.length()-8));
		
		
	}

	@Test
	public void testSimulateCalculation(){
		
		Map<OperationResult, Integer> mapSagemVDSL = new HashMap<OperationResult,Integer>();
		
		mapSagemVDSL.put(OperationResult.SUCCESS, 60);
		mapSagemVDSL.put(OperationResult.TIMEOUT, 30);
		mapSagemVDSL.put(OperationResult.EXCEPTION, 10);
		mapSagemVDSL.put(OperationResult.NORECORD, 0);
		mapSagemVDSL.put(OperationResult.ABORTED, 0);
		mapSagemVDSL.put(OperationResult.PENDING, 0);
		
		Map<OperationResult, Integer> mapIPTV = new HashMap<OperationResult,Integer>();
		
		mapIPTV.put(OperationResult.SUCCESS, 40);
		mapIPTV.put(OperationResult.TIMEOUT, 10);
		mapIPTV.put(OperationResult.EXCEPTION, 10);
		mapIPTV.put(OperationResult.NORECORD, 15);
		mapIPTV.put(OperationResult.ABORTED, 20);
		mapIPTV.put(OperationResult.PENDING, 5);

		assertTrue("operation result map not null ", mapSagemVDSL.size()>0);

		int success= mapIPTV.get(OperationResult.SUCCESS);
		int timeout= mapIPTV.get(OperationResult.TIMEOUT);
		int exception=mapIPTV.get(OperationResult.EXCEPTION);
		int aborted= mapIPTV.get(OperationResult.ABORTED);
		int norecord=mapIPTV.get(OperationResult.NORECORD);
		int pending= mapIPTV.get(OperationResult.PENDING);
		
		int total = success+timeout+aborted+exception+norecord+pending;
		
		BigDecimal cumilativeTotal = new BigDecimal(total);
		
		BigDecimal successRatio = new BigDecimal(success).divide(cumilativeTotal, 2, BigDecimal.ROUND_HALF_UP);
		
		logger.debug("success ratio: "+successRatio.multiply(SanityCheckUtil.HUNDRED));


		
	}

	@Test
	public void testCopyPasteManyTimesForMe(){
		
		int howMany = 20;
		
		String newString = SanityCheckUtil.copyPasteManyTimesForMe("-", 20);
		
		logger.debug(newString);
	
		assertTrue("copy pasted string length should be equal to howmany ", newString.length()==howMany);
		
	}

	@Test
	public void testConvertFileToDevice() throws IOException{
		
		List<Device> list = SanityCheckUtil.convertFileToDevice(filePath);
		
		assertTrue("list size should be bigger than zero",list.size()>0);
		
		for (Device device : list) {
			logger.debug("device "+device.getDeviceId());
		}
		
	}
	@Test
	public void testCalculateDuration() {
		
		long start = 120930;
		long finish =2302394;
		
		logger.debug(SanityCheckUtil.calculateDuration(start, finish, 60000));
		
	}



	@Test
	public void testPercentage(){
		
		BigDecimal divisionByZero=SanityCheckUtil.percentage(0, 3);
		
		assertTrue("dividing by zero throws ArithmeticException", divisionByZero.doubleValue()==0);
		
		divisionByZero=SanityCheckUtil.percentage(1, 3);
		assertTrue("dividing by zero throws ArithmeticException", divisionByZero.doubleValue()!=0);
		
	}

	@Test
	public void testDeviceTypeComparator(){

			List<Device> list = createSampleDeviceList();


			for(Device d:list){
				logger.debug(d.toString());
			}

			Collections.sort(list,new Comparator<Device>() {
				public int compare(Device d1, Device d2) {
					return d1.getDeviceTypeName().compareTo(d2.getDeviceTypeName());
				}
			});

			logger.debug("device list sorted");

			for(Device dd:list){
				logger.debug(dd.toString());
			}



	}

	public List<Device> createSampleDeviceList(){

		List <Device> list = new ArrayList<Device>();

		Device device1 = new Device();
		device1.setDeviceTypeName("ADSL_AFAQ011");
		device1.setSoftwareVersion("version 1");
		list.add(device1);


		Device device11 = new Device();
		device11.setDeviceTypeName("ADSL_AFAQ011");
		device11.setSoftwareVersion("version 2");
		list.add(device11);

		Device device2 = new Device();
		device2.setDeviceTypeName("ADSL_AFAQ007");
		device2.setSoftwareVersion("version 1");
		list.add(device2);

		Device device22 = new Device();
		device22.setDeviceTypeName("ADSL_AFAQ007");
		device22.setSoftwareVersion("version 3");
		list.add(device22);

		Device device222 = new Device();
		device222.setDeviceTypeName("ADSL_AFAQ007");
		device222.setSoftwareVersion("version 2");
		list.add(device222);


		Device device4 = new Device();
		device4.setDeviceTypeName("VDSL_SAGEM");
		device4.setSoftwareVersion("version 1");
		list.add(device4);

		Device device5 = new Device();
		device5.setDeviceTypeName("VDSL_HG655B");
		device5.setSoftwareVersion("version 1");
		list.add(device5);

		Device device55 = new Device();
		device55.setDeviceTypeName("VDSL_HG655B");
		device55.setSoftwareVersion("version 2");
		list.add(device55);

		Device device6 = new Device();
		device6.setDeviceTypeName("ONT_HG8245T");
		device6.setSoftwareVersion("version y");
		list.add(device6);

		Device device3 = new Device();
		device3.setDeviceTypeName("ADSL_AFAQ006");
		device3.setSoftwareVersion("version x");
		list.add(device3);

		Device device7 = new Device();
		device7.setDeviceTypeName("ONT_ALU_Q");
		device7.setSoftwareVersion("version a");
		list.add(device7);

		Device device77 = new Device();
		device77.setDeviceTypeName("ONT_ALU_Q");
		device77.setSoftwareVersion("version b");
		list.add(device77);

		Device device8 = new Device();
		device8.setDeviceTypeName("ONT_ALU_P");
		device8.setSoftwareVersion("version b");
		list.add(device8);

		Device device9 = new Device();
		device9.setDeviceTypeName("STB_IPTV");
		device9.setSoftwareVersion("version 1");
		list.add(device9);

		return list;

	}

	public Map<Summary, String> createSampleDeviceMapWithSummaryAsKey(){


		Calendar c = Calendar.getInstance();

		Date sampleDate = c.getTime();
		Map<Summary,String> map = new HashMap<Summary, String>();

		Summary sum1 = new Summary();
		sum1.setDevicetype("Afaq Shamel Model 011");
		sum1.setFirmware("V100R001B027 STC");
		sum1.setOperation("CDA");
		sum1.setCreationDate(sampleDate);

		map.put(sum1,"ozan");

		Summary sum2 = new Summary();
		sum2.setDevicetype("Afaq Shamel Model 011");
		sum2.setFirmware("V100R001B027 STC");
		sum2.setOperation("GPV");
		sum2.setCreationDate(sampleDate);

		map.put(sum2,"senturk");

		Summary sum3 = new Summary();
		sum3.setDevicetype("Afaq Shamel Model 011");
		sum3.setFirmware("V100R001B029 STC");
		sum3.setOperation("GPV");
		sum3.setCreationDate(sampleDate);

		map.put(sum3,"olga");

		Summary sum4 = new Summary();
		sum4.setDevicetype("Afaq Shamel Model 011");
		sum4.setFirmware("V100R001B029 STC");
		sum4.setOperation("CDA");
		sum4.setCreationDate(sampleDate);

		map.put(sum4,"olgas");

		Summary sum5 = new Summary();
		sum5.setDevicetype("Afaq Shamel Model 011");
		sum5.setFirmware("V100R001B029 STC");
		sum5.setOperation("SPV");
		sum5.setCreationDate(sampleDate);

		map.put(sum5,"dunes");

		return  map;

	}

	@Test
	public void testMapHashKey(){

		Map<Summary,String> map = createSampleDeviceMapWithSummaryAsKey();

		Summary summary = new Summary();
		summary.setDevicetype("Afaq Shamel Model 011");
		summary.setFirmware("V100R001B027 STC");
		summary.setOperation("GPV");

		Summary summary1 = new Summary();
		summary1.setDevicetype("Afaq Shamel Model 011");
		summary1.setFirmware("V100R001B029 STC");
		summary1.setOperation("GPV");
		summary1.setCreationDate(Calendar.getInstance().getTime());

		Summary summary11 = new Summary();
		summary11.setDevicetype("Afaq Shamel Model 011");
		summary11.setFirmware("V100R001B029 STC");
		summary11.setOperation("SPV");


		logger.debug(map.get(summary));

		logger.debug(map.get(summary1));

		logger.debug(map.get(summary11));


	}

	public Map<String, List<Device>> createSampleDeviceMapWithStringKey(){

		Map<String, List<Device>> resultMap = new HashMap<String, List<Device>>();

		List <Device> list1 = new ArrayList<Device>();
		List <Device> list2 = new ArrayList<Device>();
		List <Device> list3 = new ArrayList<Device>();
		List <Device> list4 = new ArrayList<Device>();
		List <Device> list5 = new ArrayList<Device>();
		List <Device> list6 = new ArrayList<Device>();
		List <Device> list7 = new ArrayList<Device>();
		List <Device> list8 = new ArrayList<Device>();
		List <Device> list9 = new ArrayList<Device>();






		Device device1 = new Device();
		device1.setDeviceTypeName("ADSL_AFAQ011");
		device1.setSoftwareVersion("version 1");
		list1.add(device1);

		Device device11 = new Device();
		device11.setDeviceTypeName("ADSL_AFAQ011");
		device11.setSoftwareVersion("version 2");
		list1.add(device11);

		resultMap.put("ONT CIG*3FE53219AOCD15",list1);

		Device device2 = new Device();
		device2.setDeviceTypeName("ADSL_AFAQ007");
		device2.setSoftwareVersion("version 1");
		list2.add(device2);

		Device device22 = new Device();
		device22.setDeviceTypeName("ADSL_AFAQ007");
		device22.setSoftwareVersion("version 3");
		list2.add(device22);

		Device device222 = new Device();
		device222.setDeviceTypeName("ADSL_AFAQ007");
		device222.setSoftwareVersion("version 2");
		list2.add(device222);

		resultMap.put("sagem VDSL*7.09.12_STC",list2);

		Device device4 = new Device();
		device4.setDeviceTypeName("VDSL_SAGEM");
		device4.setSoftwareVersion("version 1");
		list3.add(device4);

		resultMap.put("ONT-Huawei*V1R006C00S101",list3);

		Device device5 = new Device();
		device5.setDeviceTypeName("VDSL_HG655B");
		device5.setSoftwareVersion("version 1");
		list4.add(device5);

		Device device55 = new Device();
		device55.setDeviceTypeName("VDSL_HG655B");
		device55.setSoftwareVersion("version 2");
		list4.add(device55);

		resultMap.put("ONT ALCL I-240W-A_type-1*3FE54869AFCA22",list4);

		Device device6 = new Device();
		device6.setDeviceTypeName("ONT_HG8245T");
		device6.setSoftwareVersion("version y");
		list5.add(device6);

		resultMap.put("Afaq Shamel Model 007*V100R001C01B020_T1",list5);

		Device device3 = new Device();
		device3.setDeviceTypeName("ADSL_AFAQ006");
		device3.setSoftwareVersion("version x");
		list6.add(device3);

		resultMap.put("ONT ALCL I-240W-A_type-1*3FE54869AFCA22",list6);

		Device device7 = new Device();
		device7.setDeviceTypeName("ONT_ALU_Q");
		device7.setSoftwareVersion("version a");
		list7.add(device7);

		Device device77 = new Device();
		device77.setDeviceTypeName("ONT_ALU_Q");
		device77.setSoftwareVersion("version b");
		list7.add(device77);

		resultMap.put("Thomson TG788Avn*8.C.F.0",list7);

		Device device8 = new Device();
		device8.setDeviceTypeName("ONT_ALU_P");
		device8.setSoftwareVersion("version b");
		list8.add(device8);

		resultMap.put("Afaq Shamel Model 011*V100R001B027 STC",list8);

		Device device9 = new Device();
		device9.setDeviceTypeName("STB_IPTV");
		device9.setSoftwareVersion("version 1");
		list9.add(device9);

		resultMap.put("IPTV STB Model 001*7101469",list9);





		


		return resultMap;
	}


	@Test
	public void testSortingMapWithStringKeys(){

		Map<String,List<Device>> map = createSampleDeviceMapWithStringKey();


		TreeMap<String,List<Device>> carrierMap = new TreeMap<String, List<Device>>(
				new Comparator<String>() {



					public int compare(String o1, String o2) {

						String tmp1=o1.substring(0,o1.indexOf(SanityCheckUtil.DELIMETER_DEVICETYPE_SOFTWARE));
						String tmp2=o2.substring(0,o2.indexOf(SanityCheckUtil.DELIMETER_DEVICETYPE_SOFTWARE));

						return tmp2.compareTo( tmp1);
					}

				});


		for(Map.Entry<String,List<Device>> entry : map.entrySet()){
			carrierMap.put(entry.getKey(),map.get(entry.getKey()));
		}

		assertTrue("carrierMap not null ", carrierMap.size()>0);

	}





@Test
	public void encryptPassword(){

		ConfigurablePasswordEncryptor passwordEncryptor = new ConfigurablePasswordEncryptor();
		passwordEncryptor.setAlgorithm("SHA-256");
		passwordEncryptor.setPlainDigest(true);
		String encryptedUser = passwordEncryptor.encryptPassword("hdm_read");
		String encryptedPassword = passwordEncryptor.encryptPassword("read_hdm#321");

		logger.debug("username="+encryptedUser);
		logger.debug("password="+encryptedPassword);

		if (passwordEncryptor.checkPassword("hdm_dbuser", encryptedUser)) {


			logger.debug("correct pass");
		} else {
			logger.debug("incorrect pass");
		}

		if (passwordEncryptor.checkPassword("M0tive123", encryptedPassword)) {
			logger.debug("correct pass");
		} else {
			logger.debug("incorrect pass");
		}

	}

	@Test
	public void strongTextEncryptor() throws NoSuchAlgorithmException {

		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPassword("34567");

		String encryptedUser = textEncryptor.encrypt("hdm_dbuser");
		String encryptedPassword = textEncryptor.encrypt("Lynx_2O16");

		logger.debug("username="+encryptedUser);
		logger.debug("password="+encryptedPassword);

		String plainUser = textEncryptor.decrypt(encryptedUser);
		String plainPass = textEncryptor.decrypt(encryptedPassword);

		logger.debug("username="+plainUser);
		logger.debug("password="+plainPass);


		boolean unlimited;
		unlimited = Cipher.getMaxAllowedKeyLength("RC5") >= 256;
		System.out.println("Unlimited cryptography enabled: " + unlimited);

	}

	@Test
	public void testStringArray2NumberArray(){

		String deviceTypes[]=SanityCheckUtil.extractStringArrayFromPropertiesValue(parameterMap.get(SanityCheckUtil.PARAMETER_DEVICETYPES),SanityCheckUtil.DELIMETER_COMMA);

		long array[] = SanityCheckUtil.convertStringArray2longArray(deviceTypes);

		for (long number:array			 ) {

			logger.debug(number);
		}

	}

	@Test
	public void testConvertingDeviceTypeToArray() {

		Map<String, List<Device>> resultMap = createSampleDeviceMapWithStringKey();

		for (Map.Entry<String, List<Device>> entry : resultMap.entrySet()) {
				logger.debug(entry.getKey());

				String[] array =SanityCheckUtil.extractStringArrayFromPropertiesValue(entry.getKey(),"\\"+SanityCheckUtil.DELIMETER_DEVICETYPE_SOFTWARE);
				logger.debug("devicetypename: "+array[0]+" softwareversion: "+array[1]);

		}

	}

	@Test
	public void testEnumarator(){

		RemoteCall rm = RemoteCall.CDA;

		logger.debug(rm.name());


	}

	@Test
	public void testTrimStringLeftAndRigt(){

		String kelime="V100R001B026 STC        ";

		logger.debug(kelime+kelime);
		logger.debug(kelime.trim()+kelime.trim());
	}

	@Test
	public void testAddServiceTags() throws IOException, InterruptedException, ServiceException, ExecutionException, SQLException, ClassNotFoundException  {

		List<Device> deviceIdList = SanityCheckUtil.convertFileToDevice(filePath);

		List<Future<Device>> deviceOperationList=SanityCheckUtil.addServiceTags(deviceIdList, parameterMap);

		Map<Summary,List<Future>> operationResultMap = SanityCheckUtil.getOperationResultPerFile(deviceOperationList, parameterMap, RemoteCall.ADD_SERVICE_TAG);



		summaryList = SanityCheckUtil.saveDetails(parameterMap, operationResultMap,RemoteCall.ADD_SERVICE_TAG);

		for(Summary summary : summaryList){

			logger.debug("summary= "+summary.toString());

		}

		SanityCheckUtil.saveSummary(parameterMap,summaryList,RemoteCall.ADD_SERVICE_TAG);

		logger.debug("saved");


	}


//	private void testRemoveDevices() throws FileNotFoundException, IOException, InterruptedException, ServiceException, ExecutionException, SQLException, ClassNotFoundException  {
//
//		List<Device> deviceIdList = SanityCheckUtil.convertFileToDevice(filePath);
//
//		List<Future<Device>> deviceOperationList=SanityCheckUtil.removeDevices(deviceIdList, parameterMap);
//
//		Map<Summary,List<Future>> operationResultMap = SanityCheckUtil.getOperationResultPerFile(deviceOperationList, parameterMap, RemoteCall.REMOVE_DEVICE);
//
//		summaryList = SanityCheckUtil.saveDetails(parameterMap, operationResultMap,RemoteCall.REMOVE_DEVICE);
//
//		for(Summary summary : summaryList){
//
//			logger.debug("summary= "+summary.toString());
//
//		}
//
//		SanityCheckUtil.saveSummary(parameterMap,summaryList,RemoteCall.REMOVE_DEVICE);
//
//		logger.debug("saved");
//
//
//
//	}

	@Test
	public void testDateKeyGenerator(){

		Date currentDate=null;
		currentDate = Calendar.getInstance(Locale.US).getTime();



		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHH");
		String prefix = sdf.format(currentDate);

		logger.debug(prefix);

	}

	@Test
	public void testStringContains(){

		String lanDevices =parameterMap.get(SanityCheckUtil.PARAMETER_LAN_DEVICES);


		logger.debug(lanDevices);


	}


	public void testRemoteCallPrint(){


		RemoteCall remoteCall = RemoteCall.GPV;

		logger.debug(remoteCall+ " remote call in string");

		logger.debug(remoteCall.name()+ " remote call in name");

		logger.debug(remoteCall.toString()+ " remote call toString");
	}

	@Test
	public void createConnection() throws ClassNotFoundException {

		try {
			DeviceDAO deviceDAO;
			deviceDAO=DAOFactory.getInstance(parameterMap.get(SanityCheckUtil.PARAMETER_DB_USERNAME),parameterMap.get(SanityCheckUtil.PARAMETER_DB_PASSWORD),parameterMap.get(SanityCheckUtil.PARAMETER_DB_CONNECTION_URL),Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_DB_TYPE)));

			Connection connection = deviceDAO.createConnection();
			assertTrue("connection supposed to be created ",connection!=null);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}

