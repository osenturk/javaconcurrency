/**
 * 
 */
package hdm.sanitycheck.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

/**
 * @author ozan
 *
 */
public class ExecutorServiceFactory {

	private final static Logger  logger = Logger.getLogger(ExecutorServiceFactory.class);
	
	private static ExecutorService executorService;
	
	private ExecutorServiceFactory(){};
	
	public static ExecutorService getInstance(int executorType, int nThreads){
		
		switch (executorType){
			case 1: {
				if (executorService==null)
					
					executorService= Executors.newSingleThreadExecutor();
					logger.info("single thread pool executor created");
					break;
	
			}
			case 2: {
				if (executorService==null)
					
					executorService= Executors.newFixedThreadPool(nThreads);
					logger.info("multiple thread pool executor created");
					
					break;
			}
			default: {
				if (executorService==null)
					
					executorService= Executors.newSingleThreadExecutor();
					logger.error("executor type is not found: "+executorType);
					break;
			}
		}
		
		return executorService;
	}
}
