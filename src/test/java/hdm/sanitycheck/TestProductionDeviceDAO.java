/**
 * 
 */
package hdm.sanitycheck;

import hdm.sanitycheck.database.*;
import hdm.sanitycheck.main.SanityCheckUtil;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.*;

import static junit.framework.TestCase.assertTrue;

/**
 * @author ozan
 *
 */
public class TestProductionDeviceDAO {

	private final static Logger  logger = Logger.getLogger(TestProductionDeviceDAO.class);
	
	private DeviceDAO deviceDAO;
	
	private int limit;

	private Long deviceId;
	private DeviceType softwareVersion;

	private Map<String, String> parameterMap;
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp() throws Exception {

		
		parameterMap = SanityCheckUtil.loadParameters();
		deviceDAO=DAOFactory.getInstance(parameterMap.get(SanityCheckUtil.PARAMETER_DB_USERNAME),parameterMap.get(SanityCheckUtil.PARAMETER_DB_PASSWORD),parameterMap.get(SanityCheckUtil.PARAMETER_DB_CONNECTION_URL),Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_DB_TYPE)));

		limit = 5;
		deviceId=21140379L;
		softwareVersion=new DeviceType();
//		softwareVersion.setSoftwareVersion("3FE53219AOCI77");
//		softwareVersion.setDeviceTypeId(141L);

		softwareVersion.setSoftwareVersion("V100R001B026 STC");
		softwareVersion.setDeviceTypeId(21L);

		softwareVersion.setTotal(1000001);
		// BasicConfigurator replaced with PropertyConfigurator.
	     PropertyConfigurator.configure("etc/log4j.properties");
			
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	@After
	public void tearDown() throws Exception {

		deviceDAO=null;
	}


	@Test
	public void testGetDevicesByDeviceId() throws SQLException, ClassNotFoundException {
		
		Device device = deviceDAO.getDevicesByDeviceId(deviceId,limit);
		
		assertTrue("device should be fetched "+limit, (device!=null));
		logger.debug("device fetched "+device.getSerialNumber());
		
		logger.debug(device);
		
	}
	@Test
	public void testGetDeviceByDeviceTypeAndFirmware() throws SQLException, ClassNotFoundException {

		List<Device> list = deviceDAO.getDevicesByDeviceTypeAndFirmware( softwareVersion, limit);

		assertTrue("number of device should be "+limit, (list.size()==limit));
		logger.debug("list contains "+limit+" devices");

		for (Device device : list) {
			logger.debug(device.toString());
			assertTrue("device shouldn't be null",device.getDeviceTypeName()!=null);
		}

	}
	@Test
	public void testGetSoftwareVersionByDeviceType() throws SQLException, ClassNotFoundException {

		List<DeviceType> list = deviceDAO.getSoftwareVersionsByDeviceType(21, 5,1000);

		logger.debug("list contains "+limit+" devices");

		for (DeviceType dt : list) {
			logger.debug(dt.toString());

		}

	}



	private Summary createSummary(Date date, long iterationId){

		Summary summary = new Summary();

			summary.setAborted(1);
			summary.setCreationDate(date);
		    summary.setDevicetype("Dodge Challenger");
			summary.setDeviceTypeId(1111L);
			summary.setException(2);
			summary.setFirmware("2012");
			summary.setIteration(iterationId);
			summary.setNorecord(3);
			summary.setOperation("CDA");
			summary.setPending(4);
			summary.setPending(5);
			summary.setSampleNumberDevices(100);
			summary.setSuccess(90);
			summary.setSuccessratio(90);
			summary.setExpirationTimeout(33);
			summary.setExecutionTimeout(34);
			summary.setSessionTimeout(35);
			summary.setTotalNumberDevices(5000);
			summary.setTrend(1);
			summary.setProtocol("DEVICE_PROTOCOL_DSLFTR069v1");


		return summary;
	}

	private Detail createDetail(Date date, long summaryId){

		Detail detail = new Detail();

		detail.setCreationtime(date);
		detail.setDeviceTypeName("NOCOMMENT");
		detail.setDeviceTypeId(1111L);
		detail.setExternalIpaddress("11.212.3331.3333");
		detail.setSoftwareVersion("OSX MAC");
		detail.setLastcontacttime(date);
		detail.setDeviceActionResultStatus(SanityCheckUtil.DeviceActionResultStatus.SUCCESS);
		detail.setSerialNumber("1212121212");
		detail.setSubscriberId("538924111");
		detail.setDeviceId(100L);
		detail.setSummaryId(summaryId);
		detail.setPC("PRODCLASS");
		detail.setOUI("OUI_");
		detail.setManufacturer("Alcatel");

		detail.setModel("One Touch");




		return detail;
	}
	@Test
	public void testSaveSummary() throws SQLException, ClassNotFoundException {


		Calendar cal = Calendar.getInstance();

		Iteration iteration = createIteration(cal.getTime());

		long id = deviceDAO.saveIteration(iteration);

		Summary summary = createSummary(cal.getTime(),id);

		long summaryId= deviceDAO.saveSummary(summary);


		assertTrue("something went wrong in saving summary", summaryId>0);

	}
	@Test
	public void testSaveDetail() throws Exception{


		List<Detail> detailList = new ArrayList<Detail>();

		Calendar cal = Calendar.getInstance();

		Iteration iteration = createIteration(cal.getTime());

		long id = deviceDAO.saveIteration(iteration);

		Summary summary = createSummary(cal.getTime(),id);

		long summaryId= deviceDAO.saveSummary(summary);



		Detail detail = createDetail(cal.getTime(),summaryId);
		detail.setDeviceTypeName("Bugatti");

		Detail detail1 = createDetail(cal.getTime(),summaryId);
		detail1.setDeviceTypeName("Jaguar");
		detail1.setOperationResultId(1L);

		detailList.add(detail);
		detailList.add(detail1);

		deviceDAO.saveDetail(detailList,1);

	}

	private Iteration createIteration(Date date){

		Iteration iteration = new Iteration();

		iteration.setCreationTime(date);
		iteration.setUsername("Ozan");
		iteration.setSuccessRatio(100);
		iteration.setTotalSamplesPerIteration(1000000);
		iteration.setOperationType(SanityCheckUtil.RemoteCall.ADD_SERVICE_TAG.name());
		iteration.setExecutionDuration(123);

			return iteration;
	}
	@Test
	public void testSaveIteration() throws SQLException, ClassNotFoundException  {

		Calendar cal = Calendar.getInstance();

		Iteration iteration = createIteration(cal.getTime());

		long id = deviceDAO.saveIteration(iteration);

		assertTrue("something went wrong in saving iteration", id>0);
	}


	@Test
	public void testGetIterationById() throws SQLException, ClassNotFoundException {

		Calendar cal = Calendar.getInstance();

		Iteration iteration = createIteration(cal.getTime());

		logger.debug("iteration "+iteration);

		long id = deviceDAO.saveIteration(iteration);

		Iteration it = deviceDAO.getIterationById(id);

		assertTrue("iteration cannot be found", it!=null);

		logger.debug("iteration "+it);


	}
	@Test
	public void testGetSummaryById() throws SQLException, ClassNotFoundException {

		Calendar cal = Calendar.getInstance();

		Iteration iteration = createIteration(cal.getTime());

		long id = deviceDAO.saveIteration(iteration);

		Summary summary = createSummary(cal.getTime(),id);

		logger.debug("summary "+summary);

		long summaryId= deviceDAO.saveSummary(summary);

		Summary sum = deviceDAO.getSummaryById(id);

		assertTrue("summmary cannot be found", sum!=null);

		logger.debug("iteration "+sum);


	}
	@Test
	public void testUpdateIteration() throws SQLException, ClassNotFoundException {

		Calendar cal = Calendar.getInstance();

		Iteration iterationBefore = createIteration(cal.getTime());

		long id = deviceDAO.saveIteration(iterationBefore);

		iterationBefore.setUsername("Michael Jackson");
		iterationBefore.setId(id);
		iterationBefore.setExecutionDuration(100);
		iterationBefore.setOperationType(SanityCheckUtil.RemoteCall.ADD_SERVICE_TAG.name());

		Iteration iterationAfter =SanityCheckUtil.updateIteration(iterationBefore,parameterMap);

		Iteration iterationFromDb = deviceDAO.getIterationById(id);

		assertTrue("iteration update failed due to name not matched",iterationAfter.getUsername().equalsIgnoreCase(iterationFromDb.getUsername()));

	}
	@Test
	public void testUpdateSummary() throws SQLException, ClassNotFoundException {

		Calendar cal = Calendar.getInstance();

		Iteration iterationBefore = createIteration(cal.getTime());

		long id = deviceDAO.saveIteration(iterationBefore);

		Summary summary = createSummary(cal.getTime(),id);

		long summaryId= deviceDAO.saveSummary(summary);

		logger.debug(summary);
		summary.setOperation("washing");
		summary.setId(summaryId);
		summary.setIteration(id);

		deviceDAO.updateSummary(summary);

		Summary newSummary = deviceDAO.getSummaryById(summaryId);

		assertTrue("summary update failed due to name not matched",summary.getOperation().equalsIgnoreCase(newSummary.getOperation()));

	}

}
