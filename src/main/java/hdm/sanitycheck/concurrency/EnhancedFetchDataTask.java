/**
 * 
 */
package hdm.sanitycheck.concurrency;

import hdm.sanitycheck.database.DeviceDAO;
import hdm.sanitycheck.database.DeviceType;

import java.sql.SQLException;
import java.util.concurrent.Callable;

/**
 * @author ozan
 *
 */
public class EnhancedFetchDataTask <List> implements Callable<List> {



	private DeviceType deviceType;
	protected DeviceDAO deviceDAO;
	protected int limit;



	public int getLimit() {
		return limit;
	}


	public void setLimit(int limit) {
		this.limit = limit;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public DeviceDAO getDeviceDAO() {
		return deviceDAO;
	}


	public void setDeviceDAO(DeviceDAO deviceDAO) {
		this.deviceDAO = deviceDAO;
	}

	@SuppressWarnings("unchecked")
	public List call() throws SQLException, ClassNotFoundException {

		List list = (List) deviceDAO.getDevicesByDeviceTypeAndFirmware(deviceType,limit);

		return list;
	}

}
