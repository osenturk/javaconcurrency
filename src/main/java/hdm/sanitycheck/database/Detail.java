package hdm.sanitycheck.database;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Date;

/**
 * Created by elf on 12/23/15.
 */
public class Detail extends Device{


    private Date creationtime;



    private long summaryId;

    public long getSummaryId() {
        return summaryId;
    }

    public void setSummaryId(long summaryId) {
        this.summaryId = summaryId;
    }

    @Override
    public String toString() {
        return super.toString() + " Detail{ creationtime=" + creationtime +
                 ", summaryId=" + summaryId + " , operationResultId=" + operationResultId +
                "}";
    }

    public Date getCreationtime() {
        return creationtime;
    }

    public void setCreationtime(Date creationtime) {
        this.creationtime = creationtime;
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
                .append(this.deviceId)
                .append(this.summaryId)
                .append(this.creationtime)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Detail)) {
            return false;
        }
        if (obj == this)
            return true;

        Detail rhs = (Detail) obj;
        return new EqualsBuilder().
                // if deriving: appendSuper(super.equals(obj)).
                        append(deviceId, rhs.deviceId).
                        append(summaryId,rhs.summaryId).
                        append(creationtime, rhs.creationtime).

                        isEquals();
    }







}
