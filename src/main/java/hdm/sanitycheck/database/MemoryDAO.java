/**
 * 
 */
package hdm.sanitycheck.database;

import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * @author ozan
 *
 */

public class MemoryDAO implements DeviceDAO {

	private final static Logger  logger = Logger.getLogger(MemoryDAO.class);

	private OracleDataSource ods;

	private String username;
	private String password;
	private String connectionURL;
	private int env;

	public Connection createConnection() throws ClassNotFoundException, SQLException {

		Connection conn=null;

		Class.forName("org.h2.Driver");
		conn= DriverManager.getConnection(connectionURL,username,password);


		logger.debug("connection created for "+ DAOFactory.DatabaseEnv.MEMORY_DEV);


		return conn;
	}

	public MemoryDAO(String connectionURL,String username, String password) throws SQLException {

		this.connectionURL=connectionURL;
		this.password=password;
		this.username=username;



	}


	//TODO create sql to check whether the table exists before creation
	public void initDatabase() throws ClassNotFoundException, SQLException {

		Connection conn=null;
		Statement stat=null;



		String creationSql = "create table test(id int primary key, name varchar(255)) ";

		String populationSql = "insert into test values(1, 'Hello') ";

		try {

			conn = createConnection();
			stat = conn.createStatement();

			// this line would initialize the database
			// from the SQL script file 'init.sql'
			// stat.execute("runscript from 'init.sql'");

			stat.execute(creationSql);
			stat.execute(populationSql);

			logger.info("database created and populated");



		} catch (SQLException e) {

				if(conn!=null) {
					  try {

						  logger.error("Transaction rolled back", e);
						  conn.rollback();

					  } catch (SQLException e1) {
						  logger.error("Cannot access oracle database", e);
					  }

				   }
		}finally{

				if (stat!=null)
					stat.close();

				if (conn!=null)
					conn.close();


			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.MEMORY_DEV);

		}




	}

	public void purgeDatabase() throws ClassNotFoundException, SQLException {
		Connection conn=null;
		Statement stat=null;

		String destroySql = "delete from TABLE TEST ";


		try {

			conn = createConnection();
			stat = conn.createStatement();

			stat.execute(destroySql);

			logger.info("database purged");


		} catch (SQLException e) {

			if(conn!=null) {
				try {

					logger.error("Transaction rolled back", e);
					conn.rollback();

				} catch (SQLException e1) {
					logger.error("Cannot access oracle database", e);
				}

			}
		}finally{

			if (stat!=null)
				stat.close();

			if (conn!=null)
				conn.close();

			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.MEMORY_DEV);

		}
	}

	public void getSample() throws ClassNotFoundException, SQLException {


		Connection conn=null;
		Statement stat=null;
		ResultSet rs=null;


		try {

			conn = createConnection();
			stat = conn.createStatement();

			// this line would initialize the database
			// from the SQL script file 'init.sql'
			// stat.execute("runscript from 'init.sql'");


			rs = stat.executeQuery("select * from test");
			while (rs.next()) {
				logger.debug(rs.getString("name"));
			}

		} catch (SQLException e) {

			if(conn!=null) {
				try {

					logger.error("Transaction rolled back", e);
					conn.rollback();

				} catch (SQLException e1) {
					logger.error("Cannot access oracle database", e);
				}

			}
		}finally{

			if (stat!=null)
				stat.close();

			if (conn!=null)
				conn.close();

			if(rs!=null)
				rs.close();
			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.MEMORY_DEV);

		}

	}


	public List<Device> getDevicesByDeviceTypeAndFirmware(DeviceType deviceType, int limit) throws SQLException, ClassNotFoundException {

		List<Device> list  = new ArrayList<Device>();

		PreparedStatement devices=null;
		ResultSet rset =null;
		Connection conn=null;

		/*
		" 1-subscriberid is not null" condition removed as this is device sanity check rather than customer device sanity check
		  2-lastcontacttime bigger than last 2 days added to ensure device is not one of silent
		 */
			String sql = "SELECT d.id, d.subscriberid, d.lastcontacttime, d.serialnumber, d.externalipaddress, " +
					   "dt.device_type_name, dt.OUI, dt.product_class,dt.model_name,dt.manufacturer " +
				" from DEVICE d, DEVICETYPE dt where d.devicetype_id=dt.id and d.activated=1 and d.deleted=0 " +
				"and d.lastcontacttime is not null and d.lastcontacttime > sysdate-1 " +
				" and dt.id = ? and d.softwareversion = ?  order by d.lastcontacttime desc";


		try {
			conn = createConnection();
			devices = conn.prepareStatement(sql);
			devices.setLong(1, deviceType.getDeviceTypeId());
			devices.setString(2, deviceType.getSoftwareVersion());
			devices.setMaxRows(limit);
//				devices.setInt(2, limit);

			rset = devices.executeQuery();

			Device tmpDevice;

			while (rset.next ()){

				tmpDevice = new Device();

				tmpDevice.setDeviceId(rset.getLong(1));
				tmpDevice.setSubscriberId(rset.getString(2));
				tmpDevice.setLastcontacttime(rset.getTimestamp(3));
				tmpDevice.setSerialNumber(rset.getString(4));
				tmpDevice.setExternalIpaddress(rset.getString(5));
				tmpDevice.setDeviceTypeName(rset.getString(6));

				tmpDevice.setOUI(rset.getString(7));
				tmpDevice.setPC(rset.getString(8));
				tmpDevice.setModel(rset.getString(9));
				tmpDevice.setManufacturer(rset.getString(10));

				tmpDevice.setDeviceTypeId(deviceType.getDeviceTypeId());

				list.add(tmpDevice);


			}

		} catch (SQLException e) {
			if(conn!=null) {
				  try {

					  logger.error("Transaction rolled back", e);
					  conn.rollback();

				  } catch (SQLException e1) {
					  logger.error("Cannot access oracle database", e);
				  }

			   }
		}finally{

				if (rset!=null)
					rset.close();

				if (devices!=null)
					devices.close();

				if (conn!=null)
					conn.close();
			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.MEMORY_DEV);

		}



		return list;

	}

	public Device getDevicesByDeviceId(long deviceId, int limit) throws SQLException, ClassNotFoundException {
		return null;
	}


	public List<DeviceType> getSoftwareVersionsByDeviceType(long deviceTypeId, int firmwareLimit, int theLeastNumberOfDevicesPerFirmware) throws SQLException, ClassNotFoundException {

		List<DeviceType> list  = new ArrayList<DeviceType>();

		PreparedStatement devices=null;
		ResultSet rset =null;
		Connection conn=null;


		String sql = "SELECT d.devicetype_id,d.softwareversion,dt.device_type_name, count(d.softwareversion) as toplam " +
				"from DEVICE d, DEVICETYPE dt " +
				"where d.devicetype_id=dt.id and d.activated=1 and d.deleted=0 and d.devicetype_id = ? " +
				" and d.lastcontacttime > sysdate-2 and d.lastcontacttime is not null " +
				"group by d.devicetype_id, d.softwareversion,dt.device_type_name " +
				"having count(d.softwareversion) >= ? " +
				"order by d.devicetype_id asc, toplam desc ";


		try {
			conn = createConnection();
			devices = conn.prepareStatement(sql);
			devices.setLong(1, deviceTypeId);
			devices.setMaxRows(firmwareLimit);
			devices.setInt(2,theLeastNumberOfDevicesPerFirmware);

			rset = devices.executeQuery();


			DeviceType dt;

			while (rset.next ()){

				dt = new DeviceType();

				dt.setDeviceTypeId(rset.getLong(1));
				dt.setSoftwareVersion(rset.getString(2));
				dt.setName(rset.getString(3));
				dt.setTotal(rset.getInt(4));

				list.add(dt);

			}

		} catch (SQLException e) {

		    if(conn!=null) {
				  try {

					  logger.error("Transaction rolled back", e);
					  conn.rollback();

				  } catch (SQLException e1) {
					  logger.error("Cannot access oracle database", e);
				  }

			}
		}finally{

				if (rset!=null)
					rset.close();

				if (devices!=null)
					devices.close();

				if (conn!=null)
					conn.close();
			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.MEMORY_DEV);
		}



		return list;
	}

	public DeviceType getDeviceTypeById(long deviceTypeId) throws SQLException, ClassNotFoundException {


		DeviceType dt = null;
		Connection conn=null;

		PreparedStatement devices=null;
		ResultSet rset =null;

		String sql = "SELECT " +
				"dt.id, dt.device_type_name, dt.OUI, dt.product_class,dt.model_name,dt.manufacturer " +
				"from DEVICETYPE dt " +
				"where dt.deleted=0 and dt.id = ? " +
				"order by dt.INSERTED desc ";


		try {

			conn = createConnection();
			devices = conn.prepareStatement(sql);
			devices.setLong(1, deviceTypeId);

			rset = devices.executeQuery();


			while (rset.next ()){

				dt = new DeviceType();
				dt.setDeviceTypeId(rset.getLong(1));
				dt.setName(rset.getString(2));
				dt.setOUI(rset.getString(3));
				dt.setPC(rset.getString(4));
				dt.setModel(rset.getString(5));
				dt.setManufacturer(rset.getString(6));


			}

		} catch (SQLException e) {

				if(conn!=null) {
					  try {

						  logger.error("Transaction rolled back", e);
						  conn.rollback();

					  } catch (SQLException e1) {
						  logger.error("Cannot access oracle database", e);
					  }

				}

		}finally{


				if (rset!=null)
					rset.close();

				if (devices!=null)
					devices.close();

				if (conn!=null)
					conn.close();

			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.MEMORY_DEV);
		}



		return dt;

	}


	public long saveSummary(Summary summary) throws SQLException, ClassNotFoundException {

		PreparedStatement save=null;
		Connection conn=null;
		ResultSet generatedKeys =null;
		String generatedColumns[] = { "ID" };

		long summaryId=0;

		String sql = "INSERT INTO OPSDASH_SANITY_SUMMARY"
				+ "(ID," +
				"CREATIONDATE," +
				"DEVICETYPE, " +
				"DEVICETYPEID, " +
				"FIRMWARE, " +
				"OPERATION, " +
				"SUCCESS_RATIO," +
				"SAMPLENUMBEROFDEVICES," +
				"TOTALNUMBEROFDEVICES," +
				"SUCCESS," +
				"FAILURE," +
				"EXPIRATION_TIMEOUT," +
				"EXECUTION_TIMEOUT," +
				"SESSION_TIMEOUT," +
				"PENDING," +
				"ABORTED," +
				"NORECORD," +
				"EXCEPTION," +
				"ITERATION," +
				"TREND) " +
				"VALUES (OPSDASH_SANITYSUMMARY_SEQ.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


		try {

			conn =createConnection();
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			save = conn.prepareStatement(sql, generatedColumns);

			save.setTimestamp(1,new Timestamp( summary.getCreationDate().getTime()));
			save.setString(2, summary.getDevicetype());
			save.setLong(3, summary.getDeviceTypeId());
			save.setString(4, summary.getFirmware());
			save.setString(5, summary.getOperation());
			save.setLong(6, summary.getSuccessratio());
			save.setLong(7, summary.getSampleNumberDevices());
			save.setLong(8, summary.getTotalNumberDevices());
			save.setLong(9, summary.getSuccess());
			save.setLong(10, summary.getFailure());
			save.setLong(11, summary.getExpirationTimeout());
			save.setLong(12, summary.getExpirationTimeout());
			save.setLong(13, summary.getExpirationTimeout());
			save.setLong(14, summary.getPending());
			save.setLong(15, summary.getAborted());
			save.setLong(16, summary.getNorecord());
			save.setLong(17, summary.getException());
			save.setLong(18, summary.getIteration());
			save.setInt(19, summary.getTrend());

			save.executeUpdate();

 			generatedKeys = save.getGeneratedKeys();

			if (generatedKeys.next()) {
				summaryId=generatedKeys.getLong(1);
			}
			else {
				throw new SQLException("Creating summary failed, no ID obtained.");
			}

		} catch (SQLException e) {


			if(conn!=null) {
				  try {

					  logger.error("Transaction rolled back", e);
					  conn.rollback();

				  } catch (SQLException e1) {
					  logger.error("Cannot access oracle database", e);
				  }

			}

		}finally{

				if (save!=null)
					save.close();

				if (generatedKeys!=null)
					generatedKeys.close();

				if (conn!=null)
					conn.close();
			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.MEMORY_DEV);
		}

		return   summaryId;
	}

	public void updateSummary(Summary summary) throws SQLException, ClassNotFoundException {


		PreparedStatement save = null;
		Connection conn = null;


		String updateSql = " UPDATE OPSDASH_SANITY_SUMMARY SET " +
				"SUCCESS_RATIO=?," +
				"SUCCESS=?, " +
				"FAILURE=?, " +
				"EXPIRATION_TIMEOUT=?, " +
				"EXECUTION_TIMEOUT=?, " +
				"SESSION_TIMEOUT=?, " +
				"PENDING=?, " +
				"ABORTED=?, " +
				"NORECORD=?, " +
				"EXCEPTION=?, " +
				"FIRMWARE=?, " +
				"OPERATION=?, " +
				"DEVICETYPE=?," +
				"DEVICETYPEID=?" +

				"WHERE  ID=? ";

		try {

			conn = createConnection();
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			save = conn.prepareStatement(updateSql);

			save.setLong(1, summary.getSuccessratio());
			save.setLong(2, summary.getSuccess());
			save.setLong(3, summary.getFailure());
			save.setLong(4, summary.getExpirationTimeout());
			save.setLong(5, summary.getExecutionTimeout());
			save.setLong(6, summary.getSessionTimeout());
			save.setLong(7, summary.getPending());
			save.setLong(8, summary.getAborted());
			save.setLong(9, summary.getNorecord());
			save.setLong(10, summary.getException());
			save.setString(11, summary.getFirmware());
			save.setString(12, summary.getOperation());
			save.setString(13, summary.getDevicetype());
			save.setLong(14, summary.getDeviceTypeId());
			save.setLong(15, summary.getId());

			save.executeUpdate();




		} catch (SQLException e) {
			if(conn!=null){
				try {

					logger.error("Transaction rolled back",e);
					conn.rollback();

				} catch (SQLException e1) {
					logger.error("Cannot access oracle database",e);
				}

			}
		}
		finally {

			if (save != null)
				save.close();
			if (conn != null)
				conn.close();
			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.MEMORY_DEV);
		}

	}

	public void saveDetail(List<Detail> detailList, int batchSize) throws SQLException, ClassNotFoundException {


		PreparedStatement save=null;
		Connection conn=null;

		String sql = "INSERT INTO OPSDASH_SANITY_DETAILS"
				+ "(ID," +
				"CREATIONDATE," +
				"DEVICETYPE, " +
				"DEVICETYPEID, " +
				"FIRMWARE, " +
				"OPERATION, " +
				"RESULT," +
				"SERIALNUMBER,"+
				"SUBSCRIBER,"+
				"DEVICEID,"+
				"IPADDRESS,"+
				"LASTCONTACTTIME,"+
				"SUMMARYID,"+
				"OPERATIONRESULTID,"+
				"PC,"+
				"OUI"+
				") " +
				"VALUES (OPSDASH_SANITYDETAIL_SEQ.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


		try {

			conn =createConnection();
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			/*
			to make bulk insert
			 */
			int count=0;

			save = conn.prepareStatement(sql);

			for (Detail detail:detailList ) {

				save.setTimestamp(1,new Timestamp( detail.getCreationtime().getTime()));
				save.setString(2, detail.getDeviceActionResultStatus().toString());
				save.setString(3, detail.getSerialNumber());
				save.setString(4, detail.getSubscriberId());
				save.setLong(5, detail.getDeviceId());
				save.setString(6, detail.getExternalIpaddress());
				/*
				manual operations will have no device details
				 */
				if(detail.getLastcontacttime()!=null)
					save.setTimestamp(7, new Timestamp(detail.getLastcontacttime().getTime()));
				else
					save.setTimestamp(7, null);

				save.setLong(8,detail.getSummaryId());

				if(detail.getOperationResultId()!=null)
					save.setLong(9,detail.getOperationResultId());
				else
					save.setLong(9,0);

				save.setString(10,detail.getPC());
				save.setString(11,detail.getOUI());

				save.addBatch();

				if(++count % batchSize == 0) {
					save.executeBatch();
				}
			}


			save.executeBatch();


		} catch (SQLException e) {

			if(conn!=null) {
				  try {

		    		  logger.error("Transaction rolled back", e);
					  conn.rollback();

				  } catch (SQLException e1) {
					  logger.error("Cannot access oracle database", e);
				  }

			}
		}finally{

				if (save!=null)
					save.close();

				if (conn!=null)
					conn.close();
			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.MEMORY_DEV);
		}

	}

	public long saveIteration(Iteration iteration) throws SQLException, ClassNotFoundException {

		PreparedStatement save = null;
		Connection conn = null;

		String generatedColumns[] = { "ID" };

		ResultSet generatedKeys =null;

		String insertSql = "INSERT INTO OPSDASH_SANITY_ITERATION (ID,CREATIONTIME,SUCCESSRATIO,TOTALSAMPLE,USERNAME,OPERATIONTYPE,EXECUTIONTIME) " +
							"VALUES(OPSDASH_SANITYITERATION_SEQ.NEXTVAL,? ,?,?, ?,?,?) ";

		long iterationId=0;

		try {

			conn = createConnection();
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			save = conn.prepareStatement(insertSql,generatedColumns);

			save.setTimestamp(1, new Timestamp(iteration.getCreationTime().getTime()));
			save.setInt(2, iteration.getSuccessRatio());
			save.setLong(3, iteration.getTotalSamplesPerIteration());
			save.setString(4, iteration.getUsername());
			save.setString(5, iteration.getOperationType());
			save.setLong(6, iteration.getExecutionDuration());

			save.executeUpdate();


			 generatedKeys = save.getGeneratedKeys();

			if (generatedKeys.next()) {
				iterationId=generatedKeys.getLong(1);
			}
			else {
				throw new SQLException("Creating iteration failed, no ID obtained.");
			}

		} catch (SQLException e) {

			  if(conn!=null) {
				  try {

					  logger.error("Transaction rolled back", e);
					  conn.rollback();

				  } catch (SQLException e1) {
					  logger.error("Cannot access oracle database", e);
				  }

			  }
		}finally{

				if (save!=null)
					save.close();
				if (conn!=null)
					conn.close();
				if (generatedKeys!=null)
					generatedKeys.close();
			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.MEMORY_DEV);
		}
		return iterationId;
	}

	public void updateIteration(Iteration iteration) throws SQLException, ClassNotFoundException {

		PreparedStatement save = null;
		Connection conn = null;

		String updateSql = " UPDATE OPSDASH_SANITY_ITERATION SET " +
				 			"SUCCESSRATIO=?," +
							"TOTALSAMPLE=?," +
							"USERNAME= ?, " +
							"OPERATIONTYPE= ?, " +
							"EXECUTIONTIME= ? " +
				 			"WHERE  ID=? ";

		try {

		conn = createConnection();
		conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		save = conn.prepareStatement(updateSql);

		save.setInt(1, iteration.getSuccessRatio());
		save.setLong(2, iteration.getTotalSamplesPerIteration());
		save.setString(3, iteration.getUsername());
		save.setString(4, iteration.getOperationType());
		save.setLong(5, iteration.getExecutionDuration());
		save.setLong(6, iteration.getId());

		save.executeUpdate();


		} catch (SQLException e) {
		             if(conn!=null){
						 try {

							 logger.error("Transaction rolled back",e);
							 conn.rollback();

						 } catch (SQLException e1) {
							 	logger.error("Cannot access oracle database",e);
						 }

					 }
			}
		finally {

				if (save != null)
					save.close();
				if (conn != null)
					conn.close();
			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.MEMORY_DEV);
		}


	}



	public Iteration getIterationById(long id) throws SQLException, ClassNotFoundException {


		Iteration iteration = null;
		Connection conn=null;

		PreparedStatement iterationStmt=null;
		ResultSet rset =null;

		String sql = "SELECT ID,CREATIONTIME,SUCCESSRATIO,TOTALSAMPLE,USERNAME,OPERATIONTYPE,EXECUTIONTIME  " +
				     "FROM OPSDASH_SANITY_ITERATION " +
				     "WHERE ID = ? ";


		try {

			conn = createConnection();
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			iterationStmt = conn.prepareStatement(sql);
			iterationStmt.setLong(1, id);

			rset = iterationStmt.executeQuery();

			while (rset.next ()){

				iteration = new Iteration();
				iteration.setId(rset.getLong(1));
				iteration.setCreationTime(rset.getTimestamp(2));
				iteration.setSuccessRatio(rset.getInt(3));
				iteration.setTotalSamplesPerIteration(rset.getLong(4));
				iteration.setUsername(rset.getString(5));
				iteration.setOperationType(rset.getString(6));
				iteration.setExecutionDuration(rset.getLong(7));

			}

		} catch (SQLException e) {

			if(conn!=null) {
				try {

					logger.error("Transaction rolled back", e);
					conn.rollback();

				} catch (SQLException e1) {
					logger.error("Cannot access oracle database", e);
				}

			}

		}finally{


			if (rset!=null)
				rset.close();

			if (iterationStmt!=null)
				iterationStmt.close();

			if (conn!=null)
				conn.close();

			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.MEMORY_DEV);
		}



		return iteration;

	}


	public Summary getSummaryById(long id) throws SQLException, ClassNotFoundException {


		Summary summary = null;
		Connection conn=null;

		PreparedStatement summaryStmt=null;
		ResultSet rset =null;

		String sql = "SELECT ID,CREATIONDATE,DEVICETYPE,DEVICETYPEID,FIRMWARE,OPERATION, " +
					" SUCCESS_RATIO, SAMPLENUMBEROFDEVICES,TOTALNUMBEROFDEVICES,SUCCESS, FAILURE, " +
					" EXPIRATION_TIMEOUT,EXECUTION_TIMEOUT,SESSION_TIMEOUT,PENDING,ABORTED," +
					" NORECORD,EXCEPTION,ITERATION,TREND " +
					" FROM OPSDASH_SANITY_SUMMARY " +
					" WHERE ID = ? ";

		try {

			conn = createConnection();
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			summaryStmt = conn.prepareStatement(sql);
			summaryStmt.setLong(1, id);

			rset = summaryStmt.executeQuery();

			while (rset.next ()){

				summary = new Summary();
				summary.setId(rset.getLong(1));
				summary.setCreationDate(rset.getDate(2));
				summary.setDevicetype(rset.getString(3));
				summary.setDeviceTypeId(rset.getLong(4));
				summary.setFirmware(rset.getString(5));
				summary.setOperation(rset.getString(6));
				summary.setSuccessratio(rset.getLong(7));
				summary.setSampleNumberDevices(rset.getLong(8));
				summary.setTotalNumberDevices(rset.getLong(9));
				summary.setSuccess(rset.getLong(11));
				summary.setFailure(rset.getLong(12));
				summary.setExpirationTimeout(rset.getLong(13));
				summary.setExecutionTimeout(rset.getLong(14));
				summary.setSessionTimeout(rset.getLong(15));
				summary.setPending(rset.getLong(16));
				summary.setAborted(rset.getLong(17));
				summary.setNorecord(rset.getLong(18));
				summary.setException(rset.getLong(19));
				summary.setIteration(rset.getLong(20));


			}

		} catch (SQLException e) {

			if(conn!=null) {
				try {

					logger.error("Transaction rolled back", e);
					conn.rollback();

				} catch (SQLException e1) {
					logger.error("Cannot access oracle database", e);
				}

			}

		}finally{


			if (rset!=null)
				rset.close();

			if (summaryStmt!=null)
				summaryStmt.close();

			if (conn!=null)
				conn.close();

			logger.debug("connection closed for "+ DAOFactory.DatabaseEnv.MEMORY_DEV);
		}

		return summary;

	}



}

