                           	                              	  	                        	  	                               	                              	  	        /*
    Detecting connection request failures via alarms and clear alarm after cr credentials corrected via policy.

	@author Ozan Senturk
	@version 1.0
    @date : 18 Feb 2016
*/

log.info("[CLEAR TIMEOUT ALARM] Entering script");
var result = hdmNBIEventTriggeredPolicyResult;

var st = Packages.com.alcatel.hdm.service.nbi.dto.NBIServiceTag();
st.setName("CR_FAILURE_FIX");


log.info("result of clearalarm: "+result+ " device id: "+hdmDeviceGUID );

if(result!=0){

log.info("alarm purging started for device id "+hdmDeviceGUID);
var deviceArray = [hdmDeviceGUID];
var nbiAlarmArray = nbiService.getActiveAlarmsOfDevices(deviceArray);


if(nbiAlarmArray!=null){
log.info("alarm not null");

for (i=0;i<nbiAlarmArray.length;i++) {
				var alarm = nbiAlarmArray[i];

					log.debug(alarm.getAlarmId());
					log.debug(alarm.getType());

//				ConnectionRequestFailure

					if(alarm.getType().equalsIgnoreCase("ConnectionRequestFailure")){
						nbiService.clearAlarm(alarm.getDeviceGUID(),alarm.getAlarmId());
						log.info("Alarm cleared for device id "+alarm.getDeviceGUID());


						st.setValue("succeed");
						st.setCopyOnFactoryReset(true);
						st.setFactoryResetValue("succeed");
						nbiService.setServiceTag(alarm.getDeviceGUID(), st);
					}

}
}
log.info("alarm purging finished");

}else{
	log.info("policy execution not successful for device "+hdmDeviceGUID);
	st.setValue("failed");
	st.setCopyOnFactoryReset(true);
	st.setFactoryResetValue("failed");
	nbiService.setServiceTag(new java.lang.Long(hdmDeviceGUID), st);
}










                           	     
                           	     
                           	     
                           	     
                           	     