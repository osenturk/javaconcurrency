package hdm.sanitycheck.database;

import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Logger;
import org.jasypt.util.text.BasicTextEncryptor;

import java.sql.SQLException;

/**
 * Created by elf on 1/9/16.
 * kkk
 */
public class DatasourceFactory {

    private static OracleDataSource ods  = null;

    private final static Logger logger = Logger.getLogger(DatasourceFactory.class);

    private DatasourceFactory() { }

    public static synchronized OracleDataSource getDataSourceInstance(String username, String password, String URL) throws SQLException {

        if (ods == null){

                synchronized(DatasourceFactory.class) {
                    if (ods == null) {
                        ods = new OracleDataSource();

                        ods.setUser(username);
                        ods.setPassword(password);
                        ods.setURL(URL);

                        logger.info("Oracle DataSource created..");
                    }
                }

        }



        return ods;
    }
}
