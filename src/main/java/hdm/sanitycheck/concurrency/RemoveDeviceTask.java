/**
 * 
 */
package hdm.sanitycheck.concurrency;

import com.alcatel.hdm.service.nbi.client.NBIServicePort;
import hdm.sanitycheck.database.Device;
import hdm.sanitycheck.main.SanityCheckUtil;
import hdm.sanitycheck.main.SanityCheckUtil.DeviceActionResultStatus;
import org.apache.log4j.Logger;

import javax.xml.rpc.soap.SOAPFaultException;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * @author ozan
 *
 */
public class RemoveDeviceTask  implements Callable<Device> {
	
	private final static Logger  logger = Logger.getLogger(RemoveDeviceTask.class);
	
	private NBIServicePort nbiServicePort;
	private hdm.sanitycheck.database.Device device;

	private Set<String> messageSet;
	
	
	public RemoveDeviceTask(NBIServicePort nbiPort, hdm.sanitycheck.database.Device device){
		this.nbiServicePort=nbiPort;
		this.device=device;
	}

	public Device call() throws RemoteException {
		
		/*
		 * in case the sdo crashes, it is logged.
		 */
		
//		successful result
		
		SanityCheckUtil.DeviceActionResultStatus deviceActionResultStatus = SanityCheckUtil.DeviceActionResultStatus.SUCCESS;
		try {
			
			nbiServicePort.removeDeviceByGUID(device.getDeviceId());

		} catch (java.rmi.RemoteException remoteException) {
			
			/*
				filter caught exception and proceed release device
			 */
			messageSet = new HashSet<String>();
			messageSet.add("Message did not contain a valid Security Element");
			messageSet.add("UsernameToken not provided");

			Throwable throwable = remoteException.getCause();


			try {

				if (throwable instanceof SOAPFaultException) {

					if(!messageSet.contains(((SOAPFaultException) throwable).getFaultString())){

						logger.error(" Remove device retried for device id "+device.getDeviceId());

						nbiServicePort.removeDeviceByGUID(device.getDeviceId());


					}
				}else{
					logger.error("remove device failed for device id: "+device.getDeviceId()+" throw exception"+remoteException);
					deviceActionResultStatus= DeviceActionResultStatus.EXCEPTION;
				}
			} catch (Exception e2) {
				logger.error("remove device operation unsuccessful for device, "+device,e2);

				deviceActionResultStatus= DeviceActionResultStatus.EXCEPTION;

			}finally {
				messageSet=null;
			}


		}catch (Exception e3) {
			logger.error("remove device operation unsuccessful for device, "+device.toString(),e3);
			deviceActionResultStatus= DeviceActionResultStatus.EXCEPTION;

		}finally {
			messageSet=null;
		}
		
		logger.debug("operation result "+deviceActionResultStatus);
		
		device.setDeviceActionResultStatus(deviceActionResultStatus);
		
		return device;
	}

}
