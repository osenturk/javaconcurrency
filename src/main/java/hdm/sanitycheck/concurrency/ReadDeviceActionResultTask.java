/**
 * 
 */
package hdm.sanitycheck.concurrency;

import com.alcatel.hdm.service.nbi.client.NBIServicePort;
import com.alcatel.hdm.service.nbi.dto.NBIDeviceID;
import com.alcatel.hdm.service.nbi.dto.NBIOperationStatus;
import com.alcatel.hdm.service.nbi.dto.notification.NBIDeviceActionResult;
import hdm.sanitycheck.database.Device;
import hdm.sanitycheck.main.SanityCheckUtil;
import hdm.sanitycheck.main.SanityCheckUtil.OperationResult;
import org.apache.log4j.Logger;

import javax.xml.rpc.soap.SOAPFaultException;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * @author ozan
 *
 */
public class ReadDeviceActionResultTask implements Callable<Device> {

	private final static Logger  logger = Logger.getLogger(ReadDeviceActionResultTask.class);

	private NBIServicePort nbiServicePort;
	private Device device;
	private final static String DEVICE_PROTOCOL="DEVICE_PROTOCOL_DSLFTR069v1";

	private SanityCheckUtil.DeviceActionResultStatus deviceActionResultStatus;

	private Set<String> messageSet;

	private final int RETRY_COUNT=4;

	public ReadDeviceActionResultTask(NBIServicePort nbiPort, Device device){
		this.nbiServicePort=nbiPort;
		this.device=device;
	}

	public Device call() throws RemoteException {

		NBIDeviceActionResult deviceActionResult=null;
		int retrySingleDeviceOperation=0;
		NBIDeviceID nbiDeviceID=null;

		try {

			 nbiDeviceID = extractNBIDeviceIDfromDevice(device);

			if(nbiDeviceID==null){
				logger.error("NBIDeviceId related parameter(s) null for this device "+device);
				device.setDeviceActionResultStatus(SanityCheckUtil.DeviceActionResultStatus.EXCEPTION);
				return device;
			}

			if(device.getOperationResultId()!=null) {
				do {

					deviceActionResult = nbiServicePort.getDeviceOperationStatus(nbiDeviceID, device.getOperationResultId());

					if (retrySingleDeviceOperation > 0) {

					/*
					one second wait for each submission
					 */
						SanityCheckUtil.getSleep(1);
						logger.error("Retrieving device action result retried " + retrySingleDeviceOperation + " time(s) for " + device.getDeviceId() + " device operation result id " + device.getOperationResultId());

					}

				} while ((deviceActionResult == null) && (retrySingleDeviceOperation++ < RETRY_COUNT));


				if (deviceActionResult != null) {

					logger.debug("device action result status is " + deviceActionResult.getStatus() + " and substatus is " + deviceActionResult.getSubStatus() + " for " + device.getDeviceId() + " device operation result id " + device.getOperationResultId());
					deviceActionResultStatus = getDeviceActionResult(deviceActionResult);

				} else {

					logger.debug("Device Action Result is null for " + device.getDeviceId() + " device operation result id " + device.getOperationResultId());
					deviceActionResultStatus = SanityCheckUtil.DeviceActionResultStatus.NORECORD;

				}
			}else{
				logger.debug("Operation Result id is null for "+device.toString());
				deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.NORECORD;
			}


		} catch ( RemoteException remoteException) {

			/*
			filter caught exception and proceed release device
 			*/
			messageSet = new HashSet<String>();
			messageSet.add("Message did not contain a valid Security Element");
			messageSet.add("No Operation Result Data could be found");
			messageSet.add("UsernameToken not provided");

			Throwable throwable = remoteException.getCause();

			try {

				if (throwable instanceof SOAPFaultException) {

					if(!messageSet.contains(((SOAPFaultException) throwable).getFaultString())){

						logger.error(" read operation throws SOAP exception, retry one more time meanwhile check ids of device and operation: "+device.getDeviceId()+", "+device.getOperationResultId());

						SanityCheckUtil.getSleep(1);
						deviceActionResult = nbiServicePort.getDeviceOperationStatus(nbiDeviceID,device.getOperationResultId());

						logger.debug("device action result status is "+deviceActionResult.getStatus()+" and substatus is "+deviceActionResult.getSubStatus()+ " for "+device.getDeviceId()+" device operation result id "+device.getOperationResultId());
						deviceActionResultStatus = getDeviceActionResult(deviceActionResult);
						}
				}else{
					logger.error("device id: "+device.getDeviceId()+" throw exception for operation id "+device.getOperationResultId()+".."+remoteException);
					deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.EXCEPTION;
				}
			} catch (Exception e2) {
				try {

					logger.error("Read operation throwed exception after remote exception !");
					SanityCheckUtil.getSleep(1);
					deviceActionResult = nbiServicePort.getDeviceOperationStatus(nbiDeviceID,device.getOperationResultId());

					logger.debug("device action result status is "+deviceActionResult.getStatus()+" and substatus is "+deviceActionResult.getSubStatus()+ " for "+device.getDeviceId()+" device operation result id "+device.getOperationResultId());
					deviceActionResultStatus = getDeviceActionResult(deviceActionResult);

				}catch (Exception e){

					logger.error("after remote exception one more try also failed for "+device.getDeviceId()+" operationresultid "+device.getOperationResultId()+" "+e.getMessage());
					deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.EXCEPTION;
				}

			}finally {
				messageSet=null;
			}


		}catch (Exception e3) {

			try {
				logger.error("Read operation throwed exception and second try now!");
				SanityCheckUtil.getSleep(1);
				deviceActionResult = nbiServicePort.getDeviceOperationStatus(nbiDeviceID,device.getOperationResultId());

				logger.debug("device action result status is "+deviceActionResult.getStatus()+" and substatus is "+deviceActionResult.getSubStatus()+ " for "+device.getDeviceId()+" device operation result id "+device.getOperationResultId());
				deviceActionResultStatus = getDeviceActionResult(deviceActionResult);

			}catch (Exception e){
				logger.error("after second try still failure for "+device.getDeviceId()+" operationresultid "+device.getOperationResultId()+" "+e3.getMessage());

				deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.EXCEPTION;
			}



		}finally {
			messageSet=null;
		}



		device.setDeviceActionResultStatus(deviceActionResultStatus);
		
		return device;
	}

	//TODO substatus should be used after assurement of service success.

	private SanityCheckUtil.DeviceActionResultStatus getDeviceActionResult(NBIDeviceActionResult nbiDeviceActionResult){

		SanityCheckUtil.DeviceActionResultStatus deviceActionResultStatus;

		if(nbiDeviceActionResult==null){
			deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.NORECORD;


		}else if (nbiDeviceActionResult.getStatus()== SanityCheckUtil.DeviceActionResultStatus.ABORTED.getCode()){

			deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.ABORTED;

		}else if (nbiDeviceActionResult.getStatus()== SanityCheckUtil.DeviceActionResultStatus.PENDING.getCode()){

			deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.PENDING;

		}else if (nbiDeviceActionResult.getStatus()== SanityCheckUtil.DeviceActionResultStatus.SUCCESS.getCode()){

			deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.SUCCESS;

		}else if (nbiDeviceActionResult.getStatus()== SanityCheckUtil.DeviceActionResultStatus.FAILURE.getCode()){

			if(nbiDeviceActionResult.getSubStatus()==null){

				deviceActionResultStatus=SanityCheckUtil.DeviceActionResultStatus.EXPIRATION_TIMEOUT;

			}else {

				switch (nbiDeviceActionResult.getSubStatus()) {
					case 0: {
						deviceActionResultStatus = SanityCheckUtil.DeviceActionResultStatus.EXPIRATION_TIMEOUT;

						break;
					}
					case 1: {
						deviceActionResultStatus = SanityCheckUtil.DeviceActionResultStatus.EXECUTION_TIMEOUT;

						break;
					}
					case 2: {
						deviceActionResultStatus = SanityCheckUtil.DeviceActionResultStatus.SESSION_TIMEOUT;

						break;
					}
					default: {
						deviceActionResultStatus = SanityCheckUtil.DeviceActionResultStatus.EXCEPTION;
						logger.error("device action result substatus should be 0 or 1 or 2! ");
						break;
					}


				}
			}



		}else{

			deviceActionResultStatus= SanityCheckUtil.DeviceActionResultStatus.EXCEPTION;
			logger.error("device action result status should be 0 or 1 or 2 or 3! ");

		}

		return deviceActionResultStatus;
	}



	private NBIDeviceID extractNBIDeviceIDfromDevice(Device device) {

		NBIDeviceID nbiDeviceID=null;

		if (device == null) {

			logger.error("device is null");
			nbiDeviceID = null;

		} else if (device.getOUI() == null) {

			logger.error("Device " + device.getDeviceId() + " has no OUI");
			nbiDeviceID = null;

		} else if (device.getPC() == null) {

			logger.error("Device " + device.getDeviceId() + " has no PC");
			nbiDeviceID = null;

		} else if (device.getSerialNumber() == null) {

			logger.error("Device " + device.getDeviceId() + " has no Serialnumber");
			nbiDeviceID = null;

		} else if (device.getDeviceProtocol() == null) {

			logger.error("Device " + device.getDeviceProtocol() + " has no protocol");
			nbiDeviceID = null;

		} else {

			/*
			i guess at this point NIBDeviceID will be qualified
			 */

			nbiDeviceID = new NBIDeviceID();
			nbiDeviceID.setOUI(device.getOUI());
			nbiDeviceID.setProductClass(device.getPC());
			nbiDeviceID.setSerialNumber(device.getSerialNumber());
			nbiDeviceID.setProtocol(device.getDeviceProtocol());

			logger.info("NBIDeviceID for device " + device.getDeviceId() + " OUI: " + nbiDeviceID.getOUI() + " PC: " + nbiDeviceID.getProductClass() + " SerialNumber: " + nbiDeviceID.getSerialNumber());
		}

		return nbiDeviceID;

	}
	
}
