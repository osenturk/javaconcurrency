##############################################################################################
######################################### POWER TOOL #########################################
##############################################################################################
##############################################################################################
# OZAN SENTURK - STC
# osenturk.c@stc.com.sa
#
# power tool remotely submit requests to hdm over nbi interface. It finds each device 
# by DEVICEID and performs demanded action. There are four important files. 
#
# 1- files/deviceGUID.txt
# 2- files/prerequisites.properties
# 3- config.properties
# 4- performance.csv
# 
#             deviceGUID.txt : contains target deviceIds 	
#
#   prerequisites.properties : contains prerequisites for single device operations. it should be
# 							   read before any operation
#
#          config.properties : contains configuration details. No need to touch this file unless
#                              user credentials or server ip addresses are changed.
#
#            performance.csv : contains performance statistic of each execution.
#
#
# feel free to share your comments and further requests to be added into the tool
# 
# VERSION HISTORY
#
# V1.0 : initial version - 17 DEC 2014
#
# - Adding Service Tag 	
#
# V1.1 : 31-Dec-2014
#
# - Removing Device
# - Performance stats added 
#
# ROADMAP
#
# V1.2 : Jan 2015
#
# Sending Check device availability - 15 JAN 2015
# 
# V1.3 : Jan 2015
#
# Sending Factory Reset
#
# V1.4 : Feb 2015
#
# Removing Service Tags
#
######################################### DESCRIPTION #########################################	
# 
#
# 1- Go to prerequisites.properties file and update it before execution. 
#  
# 2- Copy paste the deviceid list into the file named "deviceGUID.txt" or upload 'deviceGUID.txt' 
#    to the server.
#    
#    Whenever deviceGUID.txt uploaded from Windows to Linux or Windows to Unix, 
#    the file context should be corrected like below.
#
#    For example: dos2unix deviceGUID.txt deviceGUID.txt 
#
# 3- Run the tool under the directory '/app/BaseKit/powertool' . Wait up to see the 'successfully 
#	 completed' message seen.
#
#    For example: ./runPowerTool.sh
#
# 4- All the logs are kept under the directory '/app/BaseKit/powertool/logs' . Simply there will be
#    two files : summary and details files. All files are appendable for each day. After one month 
#    the files will be overwritten. For example: each day there will be one file. today 08 jan 215
#	 there will be one file for details and one for summary. After one month 8th Feb 2015, this file
#	 created in 8th jan will be replaced with the one created 8th feb.  
#
#    for example = ADD_S_TAG_details_08.csv
#
#    numeric values are timestamp and means that the number of the day in the month
#	 today is 08 jan 2015 so it has 08 suffix.
#  
#    for example = ADD_S_TAG_summary_08.csv
#
#    numeric values are timestamp and means that the number of the day in the month
#	 today is 08 jan 2015 so it has 08 suffix.
#
# 5- Check the sanity-check.log for troubleshooting.
#
#
##############################################################################################
performance metrics:
67000 devices removed
18 Dec 2014 09:50:43,134  INFO total duration in seconds= 853.69
18 Dec 2014 09:50:43,134  INFO total duration in minutes= 14.23
100000 devices removed
18 Dec 2014 10:31:52,800  INFO total duration in seconds= 1494.99
18 Dec 2014 10:31:52,800  INFO total duration in minutes= 24.92
35000 devices removed
18 Dec 2014 10:57:02,333  INFO total duration in seconds= 493.46
18 Dec 2014 10:57:02,333  INFO total duration in minutes= 8.22

34980 devices removed
30 Dec 2014 12:33:04,264  INFO total duration in seconds= 480.41 
30 Dec 2014 12:33:04,264  INFO total duration in minutes= 8.01 

mvn install:install-file -Dfile=files/ojdbc6.jar -DgroupId=ojdbc6 -DartifactId=ojdbc6 -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=files/ala-nbi-commons.jar -DgroupId=ala-nbi-commons -DartifactId=ala-nbi-commons -Dversion=1.0 -Dpackaging=jar

mvn install:install-file -Dfile=files/ala-nbi-service.jar -DgroupId=ala-nbi-service -DartifactId=ala-nbi-service -Dversion=1.0 -Dpackaging=jar

mvn install:install-file -Dfile=files/ala-nbi-webservice-client.jar -DgroupId=ala-nbi-webservice-client -DartifactId=ala-nbi-webservice-client -Dversion=1.0 -Dpackaging=jar

mvn install:install-file -Dfile=files/jsafeFIPS.jar -DgroupId=jsafeFIPS -DartifactId=jsafeFIPS -Dversion=1.0 -Dpackaging=jar

mvn install:install-file -Dfile=files/weblogic.jar -DgroupId=weblogic -DartifactId=weblogic -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=files/webserviceclient_ssl.jar -DgroupId=webserviceclient-ssl -DartifactId=webserviceclient-ssl -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=files/wsse.jar -DgroupId=wsse -DartifactId=wsse -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=files/hdm-commons-functions.jar -DgroupId=hdm-commons-functions -DartifactId=hdm-commons-functions -Dversion=1.0 -Dpackaging=jar


