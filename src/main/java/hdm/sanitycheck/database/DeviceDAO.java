/**
 * 
 */
package hdm.sanitycheck.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author ozan
 *
 */
public interface DeviceDAO {

	/*
        silent devices out of solution last contacttime is bigger than last 2 days
        */
	List <Device>getDevicesByDeviceTypeAndFirmware(DeviceType dt, int limit) throws SQLException, ClassNotFoundException;

	Device getDevicesByDeviceId(long deviceId, int limit) throws SQLException, ClassNotFoundException;

	/*
	silent devices out of solution last contacttime is bigger than last 2 days
	 */
	List<DeviceType> getSoftwareVersionsByDeviceType(long deviceTypeId,int upperLimit, int lowerLimit) throws SQLException, ClassNotFoundException;

	DeviceType getDeviceTypeById(long deviceTypeId) throws SQLException, ClassNotFoundException;

	long saveSummary(Summary summary) throws SQLException, ClassNotFoundException;
	void updateSummary(Summary summary) throws SQLException, ClassNotFoundException;

	void saveDetail(List<Detail> detail, int batchSize) throws SQLException, ClassNotFoundException;

	long saveIteration(Iteration iteration) throws SQLException, ClassNotFoundException;
	void updateIteration(Iteration iteration) throws SQLException, ClassNotFoundException;
	Iteration getIterationById(long id) throws SQLException, ClassNotFoundException;
    Summary getSummaryById(long id) throws SQLException, ClassNotFoundException;

	Connection createConnection() throws SQLException, ClassNotFoundException;

	void initDatabase() throws ClassNotFoundException, SQLException;
	void purgeDatabase() throws ClassNotFoundException,SQLException;
	void getSample() throws ClassNotFoundException,SQLException;
}
