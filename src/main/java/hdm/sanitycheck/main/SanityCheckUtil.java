/**
 * 
 */
package hdm.sanitycheck.main;

import com.alcatel.hdm.service.nbi.client.NBIServicePort;
import com.alcatel.hdm.service.nbi.dto.NBIServiceTag;
import hdm.sanitycheck.concurrency.*;
import hdm.sanitycheck.database.*;
import hdm.sanitycheck.nbicore.NbiServiceFactory;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.xml.rpc.ServiceException;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * @author ozan
 *
 */


public class SanityCheckUtil {




	public enum OperationResult{
		SUCCESS(1), TIMEOUT(2), PENDING(0), ABORTED(4), EXCEPTION(1000), NORECORD(1001);

		 private int code;

		 private OperationResult(int c) {
		   code = c;
		 }

		 public int getCode() {
		   return code;
		 }


	}
	public enum DeviceActionResultStatus{

		SUCCESS(0), FAILURE(1), ABORTED(2), PENDING(3),EXPIRATION_TIMEOUT(10), EXECUTION_TIMEOUT(11), SESSION_TIMEOUT(12), EXCEPTION(1000), NORECORD(1001);

		private int code;

		private DeviceActionResultStatus(int c) {
			code = c;
		}

		public int getCode() {
			return code;
		}


	}



	public enum RemoteCall{
		CDA(34), GPV(4), SPV(5), REMOVE_DEVICE(1001), ADD_SERVICE_TAG(1002), SANITY_CHECK(1010), RESET(2);

		 private int code;
		 
		 private RemoteCall(int c) {
		   code = c;
		 }

		 public int getCode() {
		   return code;
		 }
	}

		
	private static Logger  logger = Logger.getLogger(SanityCheckUtil.class);
	private static ExecutorService executorService;


	public static final String DELIMETER_COMMA=",";
	public static final String DELIMETER_SPACE=" ";
	public static final String DELIMETER_UNDERSCORE="_";
	public static final String DELIMETER_NEWLINE="\n";
	public static final String DELIMETER_PERCENTAGE="%";
	public static final String DELIMETER_DEVICETYPE_SOFTWARE="*";
	
	
	public static final String PARAMETER_NBI_IPS="NBI_IPS";
	public static final String PARAMETER_NBI_PORTS="NBI_PORTS";
	public static final String PARAMETER_NBI_USERNAME="NBI_USERNAME";
	public static final String PARAMETER_NBI_PASSWORD="NBI_PASSWORD";
	public static final String PARAMETER_DB_USERNAME="DB_USERNAME";
	public static final String PARAMETER_DB_PASSWORD="DB_PASSWORD";
	public static final String PARAMETER_DB_CONNECTION_URL="DB_CONNECTION_URL";
	public static final String PARAMETER_DB_TYPE="WHICH_DATABASE";
	public static final String PARAMETER_DEVICETYPES="DEVICETYPES";
	public static final String PARAMETER_DB_FETCH_LIMIT_PER_DT="DB_FETCH_LIMIT_PER_DT";
	public static final String PARAMETER_DB_FETCH_LIMIT_PER_FIRMWARE="DB_FETCH_LIMIT_PER_FIRMWARE";
	public static final String PARAMETER_DB_FETCH_THE_LEAST_NUMBER_OF_DEVICES_PER_FIRMWARE="DB_FETCH_THE_LEAST_NUMBER_OF_DEVICES_PER_FIRMWARE";
	public static final String PARAMETER_DB_SAVE_DETAILS_BATCHSIZE="DB_SAVE_DETAILS_BATCHSIZE";
	public static final String PARAMETER_REMOTECALL="SDO";
	public static final String PARAMETER_EXECUTION_TIMEOUT="EXECUTION_TIMEOUT";
	public static final String PARAMETER_EXPIRATION_TIMEOUT="EXPIRATION_TIMEOUT";
	public static final String PARAMETER_SDO_PRIORITY="SDO_PRIORITY";
	public static final String PARAMETER_SDO_DISABLE_CAPTURE_CONSTRAINT="SDO_DISABLE_CAPTURE_CONSTRAINT";
	public static final String PARAMETER_SDO_POLICY_CLASS="SDO_POLICY_CLASS";
	public static final String PARAMETER_HOW_MANY_REQUEST_PER_SERVER="HOW_MANY_REQUEST_PER_SERVER";


	public static final String PARAMETER_NTHREADS="NTHREADS";
	public static final String PARAMETER_EXECUTOR_TYPE="EXECUTOR_TYPE";
	public static final String PARAMETER_LAN_DEVICES="LAN_DEVICES";

	public static final String PARAMETER_ADD_SERVICETAG_NAME="ADD_SERVICE_TAG_NAME";
	public static final String PARAMETER_ADD_SERVICETAG_VALUE="ADD_SERVICE_TAG_VALUE";
	public static final String PARAMETER_ADD_SERVICETAG_FACTORY_RESET_VALUE="ADD_FACTORY_RESET_VALUE";
	public static final String PARAMETER_ADD_SERVICETAG_IS_FACTORYRESET_ENABLED="ADD_COPY_ON_FACTORY_RESET";
	public static final String PARAMETER_IS_MANUAL="IS_MANUAL";

    public static final String TOTAL_COUNT="TOTAL_COUNT";

    
    public final static int FUNCTIONCODE_RESET=2;
    public final static int FUNCTIONCODE_GPV=4;
	public final static int FUNCTIONCODE_SPV=5;
	public final static int FUNCTIONCODE_CDA=34;
	public final static int FUNCTIONCODE_RESULT=999;
	public final static int FUNCTIONCODE_REMOVE=1001;
	public final static int FUNCTIONCODE_MERGE_SERVICETAGS=1002;
	public final static int FUNCTIONCODE_REMOVE_SERVICETAGS=1003;

	public final static String LAN_DEVICE_SDO_PARAMETER="Device.ManagementServer.ConnectionRequestURL";
	public final static String GATEWAY_DEVICE_SDO_PARAMETER="InternetGatewayDevice.ManagementServer.ConnectionRequestURL";


	
    public static final long ONE_MINUTE = 60 * 1000;
    public static final BigDecimal HUNDRED= new BigDecimal(100);
    
    public static final String OPTION_YES="y";
    public static final String OPTION_NO="n";

	private static DeviceDAO deviceDAO=null;


	public static List<String> mergeIpsAndPorts(String ips, String ports){
		
		String [] trmIps = ips.split(DELIMETER_COMMA);
		String [] trmPorts = ports.split(DELIMETER_COMMA);
		
		List<String> trmUrlAddresses = new ArrayList<String>(); 
		
		for (String ip : trmIps) {
		
			
			for (String port : trmPorts) {
				
				trmUrlAddresses.add("http://"+ip+":"+port+"/remotehdm/NBIService?WSDL");
			}
			
		}
		
		return trmUrlAddresses;
		
	}
	
	public static Map<String,String> addCustomParameters(Map <String, String> map){
		
		Map <String, String> additionalMap = new HashMap<String,String>();
		
		
		return additionalMap;
	}
	

	
	public static Map<String,String> loadParameters(){
		
		Properties prop = new Properties();
		
		Map<String, String> map = new HashMap<String,String>();
		
		try {
			
			// BasicConfigurator replaced with PropertyConfigurator.
		     PropertyConfigurator.configure("etc/log4j.properties");
		     
			//load a properties file
			prop.load(new FileInputStream("etc/config.properties"));
			
			logger.info("loading parameters");
						
			if((prop.getProperty("NBI_NODE_IPS")!=null)&&(prop.getProperty("NBI_NODE_IPS").length()>0)){
				
				map.put(PARAMETER_NBI_IPS, prop.getProperty("NBI_NODE_IPS"));
				logger.debug("nbi ip addresses: "+prop.getProperty("NBI_NODE_IPS"));
				
			}else{
				throw new Exception(PARAMETER_NBI_IPS);
			}
			
			if((prop.getProperty("NBI_NODE_PORTS")!=null)&&(prop.getProperty("NBI_NODE_PORTS").length()>0)){
				
				map.put(PARAMETER_NBI_PORTS, prop.getProperty("NBI_NODE_PORTS"));
				logger.debug("nbi port numbers: "+prop.getProperty("NBI_NODE_PORTS"));
				
				
			}else{
				throw new Exception(PARAMETER_NBI_PORTS);
			}
			
			if((prop.getProperty("NBI_USERNAME")!=null)&&(prop.getProperty("NBI_USERNAME").length()>0)){
				
				map.put(PARAMETER_NBI_USERNAME, prop.getProperty("NBI_USERNAME"));
				logger.debug("nbi username: "+prop.getProperty("NBI_USERNAME"));
				
			}else{
				throw new Exception(PARAMETER_NBI_USERNAME);
			}
			
			if((prop.getProperty("NBI_PASSWORD")!=null)&&(prop.getProperty("NBI_PASSWORD").length()>0)){
				
				map.put(PARAMETER_NBI_PASSWORD, prop.getProperty("NBI_PASSWORD"));
				logger.debug("nbi password: "+prop.getProperty("NBI_PASSWORD"));
				
			}else{
				throw new Exception(PARAMETER_NBI_PASSWORD);
			}

			if((prop.getProperty("DB_USERNAME")!=null)&&(prop.getProperty("DB_USERNAME").length()>0)){
				
				map.put(PARAMETER_DB_USERNAME, prop.getProperty("DB_USERNAME"));
				logger.debug("nbi username: "+prop.getProperty("DB_USERNAME"));
				
			}else{
				throw new Exception(PARAMETER_DB_USERNAME);
			}
			
			if((prop.getProperty("DB_PASSWORD")!=null)&&(prop.getProperty("DB_PASSWORD").length()>0)){
				
				map.put(PARAMETER_DB_PASSWORD, prop.getProperty("DB_PASSWORD"));
				logger.debug("nbi password: "+prop.getProperty("DB_PASSWORD"));
				
			}else{
				throw new Exception(PARAMETER_DB_PASSWORD);
			}
			
			if((prop.getProperty("DB_CONNECTION_URL")!=null)&&(prop.getProperty("DB_CONNECTION_URL").length()>0)){
				
				map.put(PARAMETER_DB_CONNECTION_URL, prop.getProperty("DB_CONNECTION_URL"));
				logger.debug("nbi password: "+prop.getProperty("DB_CONNECTION_URL"));
				
			}else{
				throw new Exception(PARAMETER_DB_CONNECTION_URL);
			}
			
			if((prop.getProperty("DEVICETYPES")!=null)&&(prop.getProperty("DEVICETYPES").length()>0)){
				
				map.put(PARAMETER_DEVICETYPES, prop.getProperty("DEVICETYPES"));
				logger.debug("devicetypes: "+prop.getProperty("DEVICETYPES"));
				
			}else{
				throw new Exception(PARAMETER_DEVICETYPES);
			}
			if((prop.getProperty(PARAMETER_DB_FETCH_LIMIT_PER_DT)!=null)&&(prop.getProperty(PARAMETER_DB_FETCH_LIMIT_PER_DT).length()>0)){
				
				if(!isNumeric(prop.getProperty(PARAMETER_DB_FETCH_LIMIT_PER_DT)))
					throw new NumberFormatException(prop.getProperty(PARAMETER_DB_FETCH_LIMIT_PER_DT)+" "+PARAMETER_DB_FETCH_LIMIT_PER_DT);
				
				map.put(PARAMETER_DB_FETCH_LIMIT_PER_DT, prop.getProperty(PARAMETER_DB_FETCH_LIMIT_PER_DT));
				logger.debug("fetch limit per devicetype: "+prop.getProperty(PARAMETER_DB_FETCH_LIMIT_PER_DT));
				
			}else{
				throw new Exception(PARAMETER_DB_FETCH_LIMIT_PER_DT);
			}
			
			if((prop.getProperty(PARAMETER_SDO_PRIORITY)!=null)&&(prop.getProperty(PARAMETER_SDO_PRIORITY).length()>0)){
				
				if(!isNumeric(prop.getProperty(PARAMETER_SDO_PRIORITY)))
					throw new NumberFormatException(prop.getProperty(PARAMETER_SDO_PRIORITY)+" "+PARAMETER_SDO_PRIORITY);
				
				map.put(PARAMETER_SDO_PRIORITY, prop.getProperty(PARAMETER_SDO_PRIORITY));
				logger.debug("sdo priority: "+prop.getProperty(PARAMETER_SDO_PRIORITY));
				
			}else{
				throw new Exception(PARAMETER_SDO_PRIORITY);
			}
			
			if((prop.getProperty("SDO_DISABLE_CAPTURE_CONSTRAINT")!=null)&&(prop.getProperty("SDO_DISABLE_CAPTURE_CONSTRAINT").length()>0)){
				
				map.put(PARAMETER_SDO_DISABLE_CAPTURE_CONSTRAINT, prop.getProperty("SDO_DISABLE_CAPTURE_CONSTRAINT"));
				logger.debug("disable capture constraint: "+prop.getProperty("SDO_DISABLE_CAPTURE_CONSTRAINT"));
				
			}else{
				throw new Exception(PARAMETER_SDO_DISABLE_CAPTURE_CONSTRAINT);
			}
			
			if((prop.getProperty("SDO_POLICY_CLASS")!=null)&&(prop.getProperty("SDO_POLICY_CLASS").length()>0)){
				
				map.put(PARAMETER_SDO_POLICY_CLASS, prop.getProperty("SDO_POLICY_CLASS"));
				logger.debug("policy class: "+prop.getProperty("SDO_POLICY_CLASS"));
				
			}else{
				throw new Exception(PARAMETER_SDO_POLICY_CLASS);
			}
			
			
			if((prop.getProperty(PARAMETER_NTHREADS)!=null)&&(prop.getProperty(PARAMETER_NTHREADS).length()>0)){
				
				if(!isNumeric(prop.getProperty(PARAMETER_NTHREADS)))
					throw new NumberFormatException(prop.getProperty(PARAMETER_NTHREADS)+" "+PARAMETER_NTHREADS);
				
				map.put(PARAMETER_NTHREADS, prop.getProperty(PARAMETER_NTHREADS));
				logger.debug("number of threads: "+prop.getProperty(PARAMETER_NTHREADS));
				
			}else{
				throw new Exception(PARAMETER_NTHREADS);
			}
			
			if((prop.getProperty(PARAMETER_EXECUTOR_TYPE)!=null)&&(prop.getProperty(PARAMETER_EXECUTOR_TYPE).length()>0)){
				
				if(!isNumeric(prop.getProperty(PARAMETER_EXECUTOR_TYPE)))
					throw new NumberFormatException(prop.getProperty(PARAMETER_EXECUTOR_TYPE)+" "+PARAMETER_EXECUTOR_TYPE);
				
				map.put(PARAMETER_EXECUTOR_TYPE, prop.getProperty(PARAMETER_EXECUTOR_TYPE));
				logger.debug("executor type: "+prop.getProperty(PARAMETER_EXECUTOR_TYPE));
				
			}else{
				throw new Exception(PARAMETER_EXECUTOR_TYPE);
			}

			if(prop.getProperty(PARAMETER_DB_FETCH_LIMIT_PER_FIRMWARE)!=null){

				if(!isNumeric(prop.getProperty(PARAMETER_DB_FETCH_LIMIT_PER_FIRMWARE)))
					throw new NumberFormatException(prop.getProperty(PARAMETER_DB_FETCH_LIMIT_PER_FIRMWARE)+" "+PARAMETER_DB_FETCH_LIMIT_PER_FIRMWARE);

				map.put(PARAMETER_DB_FETCH_LIMIT_PER_FIRMWARE, prop.getProperty(PARAMETER_DB_FETCH_LIMIT_PER_FIRMWARE));
				logger.debug(" how many softwareversion will be fetched per devicetype : "+prop.getProperty(PARAMETER_DB_FETCH_LIMIT_PER_FIRMWARE));

			}else
				throw new Exception(PARAMETER_DB_FETCH_LIMIT_PER_FIRMWARE);


			if(prop.getProperty(PARAMETER_LAN_DEVICES)!=null){

				map.put(PARAMETER_LAN_DEVICES, prop.getProperty(PARAMETER_LAN_DEVICES));
				logger.debug(" lan devices : "+prop.getProperty(PARAMETER_LAN_DEVICES));

			}else
				throw new Exception(PARAMETER_LAN_DEVICES);



			if(prop.getProperty(PARAMETER_DB_FETCH_THE_LEAST_NUMBER_OF_DEVICES_PER_FIRMWARE)!=null){
				String fetchLeastLimitPerFirmare=prop.getProperty(PARAMETER_DB_FETCH_THE_LEAST_NUMBER_OF_DEVICES_PER_FIRMWARE);

				if(!isNumeric(fetchLeastLimitPerFirmare)) {
					throw new NumberFormatException(fetchLeastLimitPerFirmare + " " + PARAMETER_DB_FETCH_THE_LEAST_NUMBER_OF_DEVICES_PER_FIRMWARE);
				}
				/*
				if there is a need: number of device fetch limit per device type cannot be less than firmware least number of device limit
				 */
//					String fetchLimitPerDeviceType = prop.getProperty(PARAMETER_DB_FETCH_LIMIT_PER_DT);


				map.put(PARAMETER_DB_FETCH_THE_LEAST_NUMBER_OF_DEVICES_PER_FIRMWARE, prop.getProperty(PARAMETER_DB_FETCH_THE_LEAST_NUMBER_OF_DEVICES_PER_FIRMWARE));
				logger.debug(" how many number of devices should a firmware at least contain : "+prop.getProperty(PARAMETER_DB_FETCH_THE_LEAST_NUMBER_OF_DEVICES_PER_FIRMWARE));

			}else
				throw new Exception(PARAMETER_DB_FETCH_THE_LEAST_NUMBER_OF_DEVICES_PER_FIRMWARE);


			if(prop.getProperty(PARAMETER_DB_SAVE_DETAILS_BATCHSIZE)!=null){
				String saveDetailBatchSize=prop.getProperty(PARAMETER_DB_SAVE_DETAILS_BATCHSIZE);

				if(!isNumeric(saveDetailBatchSize)) {
					throw new NumberFormatException(saveDetailBatchSize + " " + PARAMETER_DB_SAVE_DETAILS_BATCHSIZE);
				}



				map.put(PARAMETER_DB_SAVE_DETAILS_BATCHSIZE, prop.getProperty(PARAMETER_DB_SAVE_DETAILS_BATCHSIZE));
				logger.debug(" current batch size for save details : "+prop.getProperty(PARAMETER_DB_SAVE_DETAILS_BATCHSIZE));

			}else
				throw new Exception(PARAMETER_DB_SAVE_DETAILS_BATCHSIZE);


			if(prop.getProperty(PARAMETER_ADD_SERVICETAG_NAME)!=null){

				map.put(PARAMETER_ADD_SERVICETAG_NAME, prop.getProperty(PARAMETER_ADD_SERVICETAG_NAME));
				logger.debug("service tag name to be added: "+prop.getProperty(PARAMETER_ADD_SERVICETAG_NAME));

			}else
				throw new Exception(PARAMETER_ADD_SERVICETAG_NAME);

			if(prop.getProperty(PARAMETER_ADD_SERVICETAG_VALUE)!=null){

				map.put(PARAMETER_ADD_SERVICETAG_VALUE, prop.getProperty(PARAMETER_ADD_SERVICETAG_VALUE));
				logger.debug("service tag value to be added: "+prop.getProperty(PARAMETER_ADD_SERVICETAG_VALUE));

			}else
				throw new Exception(PARAMETER_ADD_SERVICETAG_VALUE);

			if(prop.getProperty(PARAMETER_ADD_SERVICETAG_FACTORY_RESET_VALUE)!=null){

				map.put(PARAMETER_ADD_SERVICETAG_FACTORY_RESET_VALUE, prop.getProperty(PARAMETER_ADD_SERVICETAG_FACTORY_RESET_VALUE));
				logger.debug("service tag reset value to be added: "+prop.getProperty(PARAMETER_ADD_SERVICETAG_FACTORY_RESET_VALUE));

			}else
				throw new Exception(PARAMETER_ADD_SERVICETAG_FACTORY_RESET_VALUE);

			if(prop.getProperty(PARAMETER_ADD_SERVICETAG_IS_FACTORYRESET_ENABLED)!=null){

				map.put(PARAMETER_ADD_SERVICETAG_IS_FACTORYRESET_ENABLED, prop.getProperty(PARAMETER_ADD_SERVICETAG_IS_FACTORYRESET_ENABLED));
				logger.debug("service tag is factory reset enabled: "+prop.getProperty(PARAMETER_ADD_SERVICETAG_IS_FACTORYRESET_ENABLED));

			}else
				throw new Exception(PARAMETER_ADD_SERVICETAG_IS_FACTORYRESET_ENABLED);

			if(prop.getProperty(PARAMETER_ADD_SERVICETAG_IS_FACTORYRESET_ENABLED)!=null){

				map.put(PARAMETER_ADD_SERVICETAG_IS_FACTORYRESET_ENABLED, prop.getProperty(PARAMETER_ADD_SERVICETAG_IS_FACTORYRESET_ENABLED));
				logger.debug("service tag is factory reset enabled: "+prop.getProperty(PARAMETER_ADD_SERVICETAG_IS_FACTORYRESET_ENABLED));

			}else
				throw new Exception(PARAMETER_ADD_SERVICETAG_IS_FACTORYRESET_ENABLED);


			if((prop.getProperty(PARAMETER_EXECUTION_TIMEOUT)!=null)&&(prop.getProperty(PARAMETER_EXECUTION_TIMEOUT).length()>0)){

				if(!isNumeric(prop.getProperty(PARAMETER_EXECUTION_TIMEOUT)))
					throw new NumberFormatException(prop.getProperty(PARAMETER_EXECUTION_TIMEOUT)+" "+PARAMETER_EXECUTION_TIMEOUT);

				map.put(PARAMETER_EXECUTION_TIMEOUT, prop.getProperty(PARAMETER_EXECUTION_TIMEOUT));
				logger.debug("execution timeout: "+prop.getProperty(PARAMETER_EXECUTION_TIMEOUT));

			}else
				throw new Exception(PARAMETER_EXECUTION_TIMEOUT);

			if((prop.getProperty(PARAMETER_EXPIRATION_TIMEOUT)!=null)&&(prop.getProperty(PARAMETER_EXPIRATION_TIMEOUT).length()>0)){

				if(!isNumeric(prop.getProperty(PARAMETER_EXPIRATION_TIMEOUT)))
					throw new NumberFormatException(prop.getProperty(PARAMETER_EXPIRATION_TIMEOUT)+" "+PARAMETER_EXPIRATION_TIMEOUT);

				map.put(PARAMETER_EXPIRATION_TIMEOUT, prop.getProperty(PARAMETER_EXPIRATION_TIMEOUT));
				logger.debug("expiration timeout: "+prop.getProperty(PARAMETER_EXPIRATION_TIMEOUT));

			}else
				throw new Exception(PARAMETER_EXPIRATION_TIMEOUT);

			if((prop.getProperty(PARAMETER_DB_TYPE)!=null)&&(prop.getProperty(PARAMETER_DB_TYPE).length()>0)){

				if(!isNumeric(prop.getProperty(PARAMETER_DB_TYPE)))
					throw new NumberFormatException(prop.getProperty(PARAMETER_DB_TYPE)+" "+PARAMETER_DB_TYPE);


				map.put(PARAMETER_DB_TYPE, prop.getProperty(PARAMETER_DB_TYPE));
				logger.debug("which database is chosen: "+prop.getProperty(PARAMETER_DB_TYPE));

			}else
				throw new Exception(PARAMETER_DB_TYPE);

			if((prop.getProperty(PARAMETER_HOW_MANY_REQUEST_PER_SERVER)!=null)&&(prop.getProperty(PARAMETER_HOW_MANY_REQUEST_PER_SERVER).length()>0)){

				if(!isNumeric(prop.getProperty(PARAMETER_HOW_MANY_REQUEST_PER_SERVER)))
					throw new NumberFormatException(prop.getProperty(PARAMETER_HOW_MANY_REQUEST_PER_SERVER)+" "+PARAMETER_HOW_MANY_REQUEST_PER_SERVER);


				map.put(PARAMETER_HOW_MANY_REQUEST_PER_SERVER, prop.getProperty(PARAMETER_HOW_MANY_REQUEST_PER_SERVER));
				logger.debug("how many request per server: "+prop.getProperty(PARAMETER_HOW_MANY_REQUEST_PER_SERVER));

			}else
				throw new Exception(PARAMETER_DB_TYPE);


		} catch (IOException ex) {
	 		ex.printStackTrace();
	 		System.exit(1);
	     }catch (NumberFormatException nfe){
	    	 logger.error("parameter couldn't be parsed "+nfe.getMessage());
	    	 System.exit(1);
	     }catch (Exception e) {
			logger.error("parameter: "+e.getMessage()+" cannot be null");
			e.printStackTrace();
			System.exit(1);
		}
		
		return map;
	}
	
	public static String[] extractStringArrayFromPropertiesValue(String deviceTypesStr, String delimeter){
		
		String deviceTypes[] = deviceTypesStr.split(delimeter);
		logger.debug("deviceTypesStr: "+deviceTypesStr+ " delimeter is: ");
		return deviceTypes;
		
	}



	public static long[] convertStringArray2longArray(String array[]){

		long numberArray []=null;

		if (array!=null){


			 numberArray = new  long[array.length];

			int i=0;
			for (String str : array ) {

					numberArray[i++]=Long.parseLong(str);

			}


		}else{

			logger.error("something wrong with your devicetype list! check if all number");

		}

			return numberArray;

	}
	
	public static String[] extractColumnDetails(String columnDetailsStr){
		
		String columnDetails[] = columnDetailsStr.split(SanityCheckUtil.DELIMETER_COMMA);
		logger.debug("columnDetailStr: "+columnDetailsStr);
		return columnDetails;
		
	}


	//TODO waiting times should be added into performance table

	public static Map<Summary,List<Future<Device>>> sendSingleDeviceOperation(Map<Summary,List<Device>> deviceMap, Map<String,String>parameterMap, RemoteCall remoteCall) throws IOException, ServiceException{

		Map<Summary,List<Future<Device>>> operationResultFutureMap;

		/*
			Get Parameter Value
		 */
		String parameter = SanityCheckUtil.GATEWAY_DEVICE_SDO_PARAMETER;
		String lanDevices[] =SanityCheckUtil.extractStringArrayFromPropertiesValue(parameterMap.get(SanityCheckUtil.PARAMETER_LAN_DEVICES),DELIMETER_COMMA);

		logger.info("single device operation " +remoteCall.toString()+" in progress");

		List<String> endUrlList = SanityCheckUtil.mergeIpsAndPorts(parameterMap.get(SanityCheckUtil.PARAMETER_NBI_IPS), parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PORTS));
		String username=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);

		SingleDeviceOperation sdo = new SingleDeviceOperation();
		sdo.setDisableCaptureConstraint(Boolean.parseBoolean(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_DISABLE_CAPTURE_CONSTRAINT)));
		sdo.setExecutionTimeout(Long.parseLong(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTION_TIMEOUT)));
		sdo.setExpirationTimeout(Long.parseLong(parameterMap.get(SanityCheckUtil.PARAMETER_EXPIRATION_TIMEOUT)));
		sdo.setPolicyClass(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_POLICY_CLASS));
		sdo.setPriority(Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_PRIORITY)));

		int nThreads=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_NTHREADS));
		int executorType=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTOR_TYPE));
		executorService = ExecutorServiceFactory.getInstance(executorType, nThreads);


		int HOW_MANY_REQUEST_PER_SERVER=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_HOW_MANY_REQUEST_PER_SERVER));

		/*
			nbi resources initialized and added into iterator.
		 */
		List<NBIServicePort> nbiResourceList = NbiServiceFactory.getNBIResourceList(endUrlList, username,password);
		Iterator<NBIServicePort> iterator = nbiResourceList.iterator();

		/*
		Lazzy initialization
		 */

		operationResultFutureMap = new HashMap<Summary, List<Future<Device>>>();

		int resourceCounter=0;

		NBIServicePort nbiPort=iterator.next();

		for(Map.Entry<Summary,List<Device>> entry : deviceMap.entrySet()){


				List<Device> list = deviceMap.get(entry.getKey());



				List<Future<Device>> deviceTypeResultList = new ArrayList<Future<Device>>();





				for (Device device : list) {

					/*
						ensure that every batch new server will receive requests!
					 */

					if(++resourceCounter % HOW_MANY_REQUEST_PER_SERVER == 0) {
						if(iterator.hasNext())
							nbiPort = iterator.next();
						else {
							iterator = nbiResourceList.iterator();
							if(iterator.hasNext()){
								nbiPort=iterator.next();
							}else{
								throw new ServiceException("sonething went very incorrect!");
							}
						}
					}
					;




					for (String tmp:lanDevices){
						if(Long.parseLong(tmp)!=device.getDeviceTypeId()){
							parameter=SanityCheckUtil.GATEWAY_DEVICE_SDO_PARAMETER;
						}else{
							parameter=SanityCheckUtil.LAN_DEVICE_SDO_PARAMETER;
						}
					}

					SingleDeviceOperationTask sdoTask = new SingleDeviceOperationTask(nbiPort,device,remoteCall,parameter);

					sdoTask.setDisableCaptureConstraint(sdo.isDisableCaptureConstraint());
					sdoTask.setExecutionTimeout(sdo.getExecutionTimeout());
					sdoTask.setExpirationTimeout(sdo.getExpirationTimeout());
					sdoTask.setPolicyClass(sdo.getPolicyClass());
					sdoTask.setPriority(sdo.getPriority());

					Future<Device> sdoFuture = executorService.submit(sdoTask);
					deviceTypeResultList.add(sdoFuture);

				}



			/// summary in deviceMap is different than summary in operationResultMap. The difference is operation field.
				Summary summary = entry.getKey();

			// this part is transfer from initial summary values to future summary
			Summary operationResultSummary = new Summary();
			operationResultSummary.setDevicetype(summary.getDevicetype());
			operationResultSummary.setFirmware(summary.getFirmware());
			operationResultSummary.setOperation(remoteCall.toString());
			operationResultSummary.setTotalNumberDevices(summary.getTotalNumberDevices());
			operationResultSummary.setSampleNumberDevices(summary.getSampleNumberDevices());
			operationResultSummary.setDeviceTypeId(summary.getDeviceTypeId());
			operationResultSummary.setProtocol(summary.getProtocol());

			operationResultFutureMap.put(operationResultSummary, deviceTypeResultList);



		}


		logger.info("single device operation " +remoteCall.toString()+" completed");

		return operationResultFutureMap;

	}
	
	
	
	public static List<Future<Device>> sendSingleDeviceOperation(List<Device> deviceList, Map<String,String>parameterMap, RemoteCall remoteCall) throws IOException, ServiceException{
		
		List<String> endUrlList = SanityCheckUtil.mergeIpsAndPorts(parameterMap.get(SanityCheckUtil.PARAMETER_NBI_IPS), parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PORTS));
		String username=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);

		SingleDeviceOperation sdo = new SingleDeviceOperation();

		/*
			Get Parameter Value
		 */
		String parameter = SanityCheckUtil.GATEWAY_DEVICE_SDO_PARAMETER;
		String lanDevices[] =SanityCheckUtil.extractStringArrayFromPropertiesValue(parameterMap.get(SanityCheckUtil.PARAMETER_LAN_DEVICES),DELIMETER_COMMA);

		sdo.setDisableCaptureConstraint(Boolean.parseBoolean(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_DISABLE_CAPTURE_CONSTRAINT)));
		sdo.setExecutionTimeout(Long.parseLong(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTION_TIMEOUT)));
		sdo.setExpirationTimeout(Long.parseLong(parameterMap.get(SanityCheckUtil.PARAMETER_EXPIRATION_TIMEOUT)));
		sdo.setPolicyClass(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_POLICY_CLASS));
		sdo.setPriority(Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_PRIORITY)));
		
		logger.debug("SingleDeviceOperation Ingredients: "+ sdo);

		logger.info("single device operation " +remoteCall.toString()+" in progress");
		
		int nThreads=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_NTHREADS));
		int executorType=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTOR_TYPE));
		executorService = ExecutorServiceFactory.getInstance(executorType, nThreads);

		int HOW_MANY_REQUEST_PER_SERVER=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_HOW_MANY_REQUEST_PER_SERVER));


		/*
			nbi resources initialized and added into iterator.
		 */
		List<NBIServicePort> nbiResourceList = NbiServiceFactory.getNBIResourceList(endUrlList, username,password);
		Iterator<NBIServicePort> iterator = nbiResourceList.iterator();

		int resourceCounter=0;

		NBIServicePort nbiPort=iterator.next();

		List <Future<Device>> deviceResultList = new ArrayList<Future<Device>>();
		
		for (Device device : deviceList) {
			
			
			logger.debug("Single Device Operation, "+device.getDeviceId()+","+device.getLastcontacttime());
			
			/*
				ensure that every batch new server will receive requests!
			 */

			if(++resourceCounter % HOW_MANY_REQUEST_PER_SERVER == 0) {
				if(iterator.hasNext())
					nbiPort = iterator.next();
				else {
					iterator = nbiResourceList.iterator();
					if(iterator.hasNext()){
						nbiPort=iterator.next();
					}else{
						throw new ServiceException("sonething went very incorrect!");
					}
				}
			}


			//TODO instr might be faster than for loop
			for (String tmp:lanDevices){


				if(Long.parseLong(tmp)!=device.getDeviceTypeId()){
					parameter=SanityCheckUtil.GATEWAY_DEVICE_SDO_PARAMETER;
				}else{
					parameter=SanityCheckUtil.LAN_DEVICE_SDO_PARAMETER;
				}
			}

			SingleDeviceOperationTask sdoTask = new SingleDeviceOperationTask(nbiPort,device,remoteCall,parameter);
			sdoTask.setDisableCaptureConstraint(sdo.isDisableCaptureConstraint());
			sdoTask.setExecutionTimeout(sdo.getExecutionTimeout());
			sdoTask.setExpirationTimeout(sdo.getExpirationTimeout());
			sdoTask.setPolicyClass(sdo.getPolicyClass());
			sdoTask.setPriority(sdo.getPriority());
						
			Future<Device> sdoFuture = executorService.submit(sdoTask);
			deviceResultList.add(sdoFuture);
				
		}



		logger.info("single device operation " +remoteCall.toString()+" completed");

		return deviceResultList;
	
	}
	
	
	public static List<Future<Device>> addServiceTags(List<Device> deviceList, Map<String,String>parameterMap) throws IOException, ServiceException{
		
//		Map<Integer,List<Future<Device>>> operationResultMap = new HashMap<Integer,List<Future<Device>>>();

		
		List<String> endUrlList = SanityCheckUtil.mergeIpsAndPorts(parameterMap.get(SanityCheckUtil.PARAMETER_NBI_IPS), parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PORTS));
		String username=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);
		
//		SingleDeviceOperation sdo = new SingleDeviceOperation();
//		sdo.setDisableCaptureConstraint(Boolean.parseBoolean(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_DISABLE_CAPTURE_CONSTRAINT)));
//		sdo.setExecutionTimeout(Long.parseLong(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_EXECUTION_TIMEOUT)));
//		sdo.setExpirationTimeout(Long.parseLong(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_EXPIRATION_TIMEOUT)));
//		sdo.setPolicyClass(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_POLICY_CLASS));
//		sdo.setPriority(Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_PRIORITY)));
//		
//		logger.debug(LABEL_ADD_SERVICETAG+" , "+"SingleDeviceOperation Ingredients: "+ sdo);
		
		int nThreads=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_NTHREADS));
		int executorType=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTOR_TYPE));
		executorService = ExecutorServiceFactory.getInstance(executorType, nThreads);

		int HOW_MANY_REQUEST_PER_SERVER=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_HOW_MANY_REQUEST_PER_SERVER));

		/*
			nbi resources initialized and added into iterator.
		 */
		List<NBIServicePort> nbiResourceList = NbiServiceFactory.getNBIResourceList(endUrlList, username,password);
		Iterator<NBIServicePort> iterator = nbiResourceList.iterator();

		int resourceCounter=0;

		NBIServicePort nbiPort=iterator.next();

		List <Future<Device>> deviceResultList = new ArrayList<Future<Device>>();
		
		for (Device device : deviceList) {
			
			
			logger.debug(RemoteCall.ADD_SERVICE_TAG+" , "+device.getDeviceId()+","+device.getLastcontacttime());
			

			/*
				ensure that every batch new server will receive requests!
			 */

			if(++resourceCounter % HOW_MANY_REQUEST_PER_SERVER == 0) {
				if(iterator.hasNext())
					nbiPort = iterator.next();
				else {
					iterator = nbiResourceList.iterator();
					if(iterator.hasNext()){
						nbiPort=iterator.next();
					}else{
						throw new ServiceException("sonething went very incorrect!");
					}
				}
			}
			
			
			NBIServiceTag nbiServiceTag = new NBIServiceTag();
			nbiServiceTag.setName(parameterMap.get(PARAMETER_ADD_SERVICETAG_NAME));
			nbiServiceTag.setValue(parameterMap.get(PARAMETER_ADD_SERVICETAG_VALUE));
			nbiServiceTag.setFactoryResetValue(parameterMap.get(PARAMETER_ADD_SERVICETAG_FACTORY_RESET_VALUE));
			nbiServiceTag.setCopyOnFactoryReset(Boolean.parseBoolean(parameterMap.get(PARAMETER_ADD_SERVICETAG_IS_FACTORYRESET_ENABLED)));
			
			NBIServiceTag [] nbiServiceTagArray = {nbiServiceTag};
			
			AddServiceTagTask addServiceTagTask = new AddServiceTagTask(nbiPort, device,nbiServiceTagArray);
			
//			addServiceTagTask.setDisableCaptureConstraint(sdo.isDisableCaptureConstraint());
//			addServiceTagTask.setExecutionTimeout(sdo.getExecutionTimeout());
//			addServiceTagTask.setExpirationTimeout(sdo.getExpirationTimeout());
//			addServiceTagTask.setPolicyClass(sdo.getPolicyClass());
//			addServiceTagTask.setPriority(sdo.getPriority());
						
			Future<Device> sdoFuture = executorService.submit(addServiceTagTask);
			deviceResultList.add(sdoFuture);
				
		}
				
		
		
		
		return deviceResultList;
	
	}
	
	
	public static List<Future<Device>> removeDevices(List<Device> deviceList, Map<String,String>parameterMap) throws IOException, ServiceException{
		
//		Map<Integer,List<Future<Device>>> operationResultMap = new HashMap<Integer,List<Future<Device>>>();

				
		List<String> endUrlList = SanityCheckUtil.mergeIpsAndPorts(parameterMap.get(SanityCheckUtil.PARAMETER_NBI_IPS), parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PORTS));
		String username=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);
		
		int nThreads=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_NTHREADS));
		int executorType=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTOR_TYPE));
		executorService = ExecutorServiceFactory.getInstance(executorType, nThreads);

		int HOW_MANY_REQUEST_PER_SERVER=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_HOW_MANY_REQUEST_PER_SERVER));

		/*
			nbi resources initialized and added into iterator.
		 */
		List<NBIServicePort> nbiResourceList = NbiServiceFactory.getNBIResourceList(endUrlList, username,password);
		Iterator<NBIServicePort> iterator = nbiResourceList.iterator();

		int resourceCounter=0;

		NBIServicePort nbiPort=iterator.next();

		List <Future<Device>> deviceResultList = new ArrayList<Future<Device>>();
		
		for (Device device : deviceList) {
			
			
			logger.debug(RemoteCall.REMOVE_DEVICE+" , "+device.getDeviceId()+","+device.getLastcontacttime());
			
			/*
				ensure that every batch new server will receive requests!
			 */

			if(++resourceCounter % HOW_MANY_REQUEST_PER_SERVER == 0) {
				if(iterator.hasNext())
					nbiPort = iterator.next();
				else {
					iterator = nbiResourceList.iterator();
					if(iterator.hasNext()){
						nbiPort=iterator.next();
					}else{
						throw new ServiceException("sonething went very incorrect!");
					}
				}
			}



			RemoveDeviceTask removeDeviceTask = new RemoveDeviceTask(nbiPort, device);
			
			Future<Device> sdoFuture = executorService.submit(removeDeviceTask);
			deviceResultList.add(sdoFuture);
		}
				
		
		return deviceResultList;
		}
	
	public static void getSleep(int sleepTimeInSeconds) throws InterruptedException{
		
		/*
		 * informative flag to inform how many seconds left to finish
		 */
		
		int sleepFlag=sleepTimeInSeconds;
		
		while(sleepFlag!=0){
			
			Thread.sleep(1000);
			logger.info("please wait for "+sleepFlag-- +" seconds to see the results!! ");
			
		}
	}
	

	public static Map<Summary,List<Future>> getOperationResultPerFile(List<Future<Device>> deviceList, Map<String,String> parameterMap, RemoteCall remoteCall ) throws  InterruptedException, ExecutionException, IOException, ServiceException{

		Map<Summary,List<Future>> futureMap;
		

		logger.info("reading operation result in progress");

		/*
		 * reading result operation prepared below
		 */

		List<String> endUrlList = SanityCheckUtil.mergeIpsAndPorts(parameterMap.get(SanityCheckUtil.PARAMETER_NBI_IPS), parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PORTS));
		String username=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);
		
		int nThreads=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_NTHREADS));
		int executorType=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTOR_TYPE));
		executorService = ExecutorServiceFactory.getInstance(executorType, nThreads);
		
		
		logger.info(remoteCall+" operation result will be fetched for "+remoteCall.getCode());

		/*
			nbi resources initialized and added into iterator.
		 */
		List<NBIServicePort> nbiResourceList = NbiServiceFactory.getNBIResourceList(endUrlList, username,password);
		Iterator<NBIServicePort> iterator = nbiResourceList.iterator();

		int resourceCounter=0;

		NBIServicePort nbiPort=iterator.next();

		int execution=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTION_TIMEOUT));
		int expiration=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXPIRATION_TIMEOUT));

		long totalWaitTimeInSeconds=execution+expiration;

		long waitingOperationResultCounter=0;

		int HOW_MANY_REQUEST_PER_SERVER=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_HOW_MANY_REQUEST_PER_SERVER));


		/*
		Lazzy initializations
		 */
		futureMap=new HashMap<Summary, List<Future>>();


		/*
		All operations will be handled with one summary because only one list extracted from file
		 */
		Summary summary = new Summary();
		summary.setOperation(remoteCall.name());

		logger.info(remoteCall+" number of devices "+deviceList.size());
		
		/*
		single device operation calculated after operation result fetched from HDM
		 */


		List<Future> resultList = new ArrayList<Future>();

		if (remoteCall.getCode()<RemoteCall.REMOVE_DEVICE.getCode()){


			SanityCheckUtil.getSleep(execution+expiration);


			
			/*
			 * waiting for expiration timeout to be completed
			 */
			

			for (Future<Device> future : deviceList) {

				while((!future.isDone())&&(waitingOperationResultCounter<totalWaitTimeInSeconds)) {

					/*
					waiting for the results to be retrieved
					 */
					getSleep(1);
					logger.debug(remoteCall+" waiting for operation results to be fetched in "+ ++waitingOperationResultCounter +" seconds");

				}
				
				Device device = (Device) future.get();

					/*
					 * fair distribution between the hdm nodes
					 * 
					 * first it goes to endUrlIndex=0 and then increases 1
					 */
					int endUrlIndex=0;
					
					logger.debug(remoteCall+" device and operation result "+ device);
							
					/*
					ensure that every batch new server will receive requests!
				 */

				if(++resourceCounter % HOW_MANY_REQUEST_PER_SERVER == 0) {
					if(iterator.hasNext())
						nbiPort = iterator.next();
					else {
						iterator = nbiResourceList.iterator();
						if(iterator.hasNext()){
							nbiPort=iterator.next();
						}else{
							throw new ServiceException("sonething went very incorrect!");
						}
					}
				}
							
					ReadOperationResultTask readResultTask = new ReadOperationResultTask(nbiPort, device);
					Future<Device> result = executorService.submit(readResultTask);
//					Device resultDevice= result.get();
						/*
						add as future object to be able to wait for result if needed
						 */
					resultList.add(result);
			}	
		
		}else{
			
			logger.debug(remoteCall+" Non Single Device Operation result will be fecthed directly");
			
			SanityCheckUtil.getSleep(10);
			
			for (Future<Device> future : deviceList) {

				resultList.add(future);

			}
			
			
		}

		summary.setSampleNumberDevices(deviceList.size());
		futureMap.put(summary,resultList);

		logger.info("operation label is "+remoteCall+" reading operation result completed");

		return  futureMap;
		
	}



	public static Map<Summary,List<Future>> getOperationResultPerSoftware( Map<Summary,List<Future<Device>>> sdoMap, Map<String,String> parameterMap, RemoteCall remoteCall ) throws InterruptedException, ExecutionException, IOException, ServiceException{

		logger.info("reading operation result in progress");


		Map<Summary, List<Future>> resultMap;

		/*
		 * reading result operation prepared below
		 */

		List<String> endUrlList = SanityCheckUtil.mergeIpsAndPorts(parameterMap.get(SanityCheckUtil.PARAMETER_NBI_IPS), parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PORTS));
		String username=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);

		int nThreads=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_NTHREADS));
		int executorType=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTOR_TYPE));
		executorService = ExecutorServiceFactory.getInstance(executorType, nThreads);

		int expiration = Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXPIRATION_TIMEOUT));
		int execution = Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTION_TIMEOUT));

		int HOW_MANY_REQUEST_PER_SERVER=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_HOW_MANY_REQUEST_PER_SERVER));

		/*
		 * waiting for expiration timeout to be completed - added 10 seconds more
		 */
		SanityCheckUtil.getSleep(execution+expiration);

		long totalWaitTimeInSeconds=execution+expiration;

		long waitingOperationResultCounter=0;

		/*
			nbi resources initialized and added into iterator.
		 */
		List<NBIServicePort> nbiResourceList = NbiServiceFactory.getNBIResourceList(endUrlList, username,password);
		Iterator<NBIServicePort> iterator = nbiResourceList.iterator();

		int resourceCounter=0;

		NBIServicePort nbiPort=iterator.next();

		/*
		lazzy initializations after other resources initialized
		 */
		resultMap= new HashMap<Summary, List<Future>>();

		for(Map.Entry<Summary,List<Future<Device>>> entry : sdoMap.entrySet()){

			logger.debug("device type and software and operation name: "+entry.getKey().toString());

			List<Future<Device>> list = entry.getValue();

			logger.debug("number of devices "+list.size());



			List<Future> deviceResultList = new ArrayList<Future>();

			for (Future<Device> future : list) {

			/*
				ensure that every batch new server will receive requests!
			 */
				if(++resourceCounter % HOW_MANY_REQUEST_PER_SERVER == 0) {
					if(iterator.hasNext())
						nbiPort = iterator.next();
					else {
						iterator = nbiResourceList.iterator();
						if(iterator.hasNext()){
							nbiPort=iterator.next();
						}else{
							throw new ServiceException("sonething went very incorrect!");
						}
					}
				}
//					if(!future.isDone()){
//						logger.error("device result is not completed yet "+((Device)future.get()).getDeviceId());
//					}
				while((!future.isDone())&&(waitingOperationResultCounter<totalWaitTimeInSeconds)) {

					/*
					waiting for the results to be retrieved
					 */
					getSleep(1);
					logger.debug("fetching of "+remoteCall.name()+" results are underway "+ ++waitingOperationResultCounter +" seconds");

				}

				 Device device = future.get();



				ReadOperationResultTask readResultTask = new ReadOperationResultTask(nbiPort, device);

				Future<Device> result = executorService.submit(readResultTask);
				deviceResultList.add(result);



			}


			resultMap.put(entry.getKey(), deviceResultList);


		}

		logger.info("reading operation result completed");

		return resultMap;

	}


	public static Map<Summary,List<Future>> getDeviceActionResultPerSoftware( Map<Summary,List<Future<Device>>> sdoMap, Map<String,String> parameterMap, RemoteCall remoteCall ) throws InterruptedException, ExecutionException, IOException, ServiceException{

		logger.info("reading operation result in progress");


		Map<Summary, List<Future>> resultMap;

		/*
		 * reading result operation prepared below
		 */

		List<String> endUrlList = SanityCheckUtil.mergeIpsAndPorts(parameterMap.get(SanityCheckUtil.PARAMETER_NBI_IPS), parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PORTS));
		String username=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);

		int nThreads=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_NTHREADS));
		int executorType=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTOR_TYPE));
		executorService = ExecutorServiceFactory.getInstance(executorType, nThreads);

		int expiration = Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXPIRATION_TIMEOUT));
		int execution = Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTION_TIMEOUT));

		int HOW_MANY_REQUEST_PER_SERVER=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_HOW_MANY_REQUEST_PER_SERVER));

		/*
		 * waiting for expiration timeout to be completed
		 */
		long totalWaitTimeInSeconds=execution+expiration;
		SanityCheckUtil.getSleep(execution+expiration);



		long waitingOperationResultCounter=0;

		/*
			nbi resources initialized and added into iterator.
		 */
		List<NBIServicePort> nbiResourceList = NbiServiceFactory.getNBIResourceList(endUrlList, username,password);
		Iterator<NBIServicePort> iterator = nbiResourceList.iterator();

		int resourceCounter=0;

		NBIServicePort nbiPort=iterator.next();

		/*
		lazzy initializations after other resources initialized
		 */
		resultMap= new HashMap<Summary, List<Future>>();

		for(Map.Entry<Summary,List<Future<Device>>> entry : sdoMap.entrySet()){

			logger.debug("device type and software and operation name: "+entry.getKey().toString());

			List<Future<Device>> list = entry.getValue();

			logger.debug("number of devices "+list.size());



			List<Future> deviceResultList = new ArrayList<Future>();

			for (Future<Device> future : list) {

			/*
				ensure that every batch new server will receive requests!
			 */
				if(++resourceCounter % HOW_MANY_REQUEST_PER_SERVER == 0) {
					if(iterator.hasNext())
						nbiPort = iterator.next();
					else {
						iterator = nbiResourceList.iterator();
						if(iterator.hasNext()){
							nbiPort=iterator.next();
						}else{
							throw new ServiceException("sonething went very incorrect!");
						}
					}
				}
//					if(!future.isDone()){
//						logger.error("device result is not completed yet "+((Device)future.get()).getDeviceId());
//					}
				while((!future.isDone())&&(waitingOperationResultCounter<totalWaitTimeInSeconds)) {

					/*
					waiting for the results to be retrieved
					 */
					getSleep(1);
					logger.debug("fetching of "+remoteCall.name()+" results are underway "+ ++waitingOperationResultCounter +" seconds");

				}

				Device device = future.get();



				ReadDeviceActionResultTask readResultTask = new ReadDeviceActionResultTask(nbiPort, device);

				Future<Device> result = executorService.submit(readResultTask);
				deviceResultList.add(result);



			}


			resultMap.put(entry.getKey(), deviceResultList);


		}

		logger.info("reading operation result completed");

		return resultMap;

	}


	public static String convertDate2DateTimeString(Date date){
		
		DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.US);
		
		return formatter.format(date);
	}
	

	


	public static List<Summary> saveDetails(Map<String, String> parameterMap, Map<Summary,List<Future>> resultMap, RemoteCall remoteCall) throws SQLException, ClassNotFoundException {

				int expiration = Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXPIRATION_TIMEOUT));
				int execution = Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTION_TIMEOUT));

				deviceDAO=DAOFactory.getInstance(parameterMap.get(SanityCheckUtil.PARAMETER_DB_USERNAME),parameterMap.get(SanityCheckUtil.PARAMETER_DB_PASSWORD),parameterMap.get(SanityCheckUtil.PARAMETER_DB_CONNECTION_URL),Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_DB_TYPE)));


				long totalWaitTimeInSeconds=execution+expiration;

				long waitingForReadResultsToBeFetchedCounter=0;

				List<Summary> summaryList = new ArrayList<Summary>();

				/*
					to save details in more flexible way
				 */
				List <Detail> detailList = new ArrayList<Detail>();


				/*
				 *
				 * files are classified by date, each date there will be particular file
				 *
				 */

				Date currentDate=null;
				currentDate = Calendar.getInstance(Locale.US).getTime();

						/*
						 * to log the current time in the detailfile
						 */
				String currentTimeStr=getTodayAsTime(DateFormat.MEDIUM, Locale.US);

				SimpleDateFormat sdf = new SimpleDateFormat("dd");
				String prefix = sdf.format(currentDate);

				Iteration iteration = new Iteration();
				iteration.setCreationTime(currentDate);
				iteration.setUsername("ozan senturk");
				iteration.setOperationType(remoteCall.name());

				//Each execution means one iteration

				long iterationId = deviceDAO.saveIteration(iteration);


				logger.info("saving details");
				PrintWriter detailWriter=null;



				/*
				 *
				 * resultmap contains list of devices which are indexed by device type ids
				 *
				 */
				for(Map.Entry<Summary,List<Future>> entry : resultMap.entrySet()){

					logger.debug("calculating number of result is started..");




					Summary keySummary=entry.getKey();
					if (keySummary==null) {
						logger.error("entry key: "+entry.getKey());
						throw new IllegalArgumentException();
					}
					/*
					Each summary execution contains his own details
					with particular iteration id
					 */
					keySummary.setIteration(iterationId);
					keySummary.setCreationDate(currentDate);

					long summaryId=deviceDAO.saveSummary(keySummary);
					keySummary.setId(summaryId);


					List<Future> deviceList = entry.getValue();


					if(deviceList.size()==0) {
						logger.error("there is no device for this devicetype firmware combination: "+keySummary.toString());
						continue;
					}



						long successCounter=0;
						long failureCounter=0;
						long abortCounter=0;
						long expirationTimeoutCounter=0;
						long executionTimeoutCounter=0;
						long sessionTimeoutCounter=0;
						long pendingCounter=0;
						long exceptionCounter=0;
						long norecordCounter=0;

					Device device;
						for (Future future : deviceList) {

							/*
								Adding results into the details list

							 */

							if (remoteCall.getCode()<RemoteCall.REMOVE_DEVICE.getCode()){
								/*
								if it is synchronous operation skip it!
								 */

								while((!future.isDone())&&(waitingForReadResultsToBeFetchedCounter<totalWaitTimeInSeconds)) {

									/*
									waiting for the results to be retrieved
									 */
									try {
										getSleep(1);
									} catch (InterruptedException e) {
										e.printStackTrace();
										continue;
									}
									logger.debug("waiting for Read Results for "+keySummary.getOperation()+ ++waitingForReadResultsToBeFetchedCounter +" seconds");

								}

							}

							try {
								device = (Device) future.get();
								logger.debug("Device details saved: Device result successfully fetched for "+device);

							} catch (ExecutionException e) {
								e.printStackTrace();
								logger.error(" Read result cannot be fetched for one device due to executionException");
								continue;
							} catch (InterruptedException e) {
								e.printStackTrace();
								logger.error(" Read result cannot be fetched for one device due to interruptedException");
								continue;
							}

							Detail detail = new Detail();
							detail.setDeviceId(device.getDeviceId());
							detail.setCreationtime(currentDate);
							detail.setExternalIpaddress(device.getExternalIpaddress());
							detail.setLastcontacttime(device.getLastcontacttime());
							detail.setManufacturer(device.getManufacturer());
							detail.setModel(device.getModel());
							detail.setDeviceActionResultStatus(device.getDeviceActionResultStatus());
							detail.setOperationResultId(device.getOperationResultId());
							detail.setOUI(device.getOUI());
							detail.setPC(device.getPC());
							detail.setSerialNumber(device.getSerialNumber());
							detail.setSubscriberId(device.getSubscriberId());
							detail.setTotal(keySummary.getTotalNumberDevices());
							detail.setSummaryId(summaryId);
							detailList.add(detail);


							switch (device.getDeviceActionResultStatus().getCode()){

								case 0:{
									successCounter++;

									break;
								}
								case 1:{
									failureCounter++;

									break;
								}
								case 10:{
									/*
									failure counter is intersection of all timeouts as it is super status
									 */
									expirationTimeoutCounter++;
									failureCounter++;
									break;
								}
								case 11:{
									executionTimeoutCounter++;
									failureCounter++;
									break;
								}
								case 12:{
									failureCounter++;
									sessionTimeoutCounter++;
									break;
								}
								case 2:{
									abortCounter++;
									break;
								}
								case 3:{
									pendingCounter++;
									break;
								}
							}




						}


							keySummary.setSuccess(successCounter);
							keySummary.setFailure(failureCounter);
							keySummary.setAborted(abortCounter);

							keySummary.setException(exceptionCounter);

							keySummary.setNorecord(norecordCounter);

							keySummary.setPending(pendingCounter);

							keySummary.setExpirationTimeout(expirationTimeoutCounter);
							keySummary.setExecutionTimeout(executionTimeoutCounter);
							keySummary.setSessionTimeout(sessionTimeoutCounter);

						logger.info("Summary after details calculated "+keySummary);





					// end of counter map

						/*
							Each device operation result should be added to the list only once as
							there is only one result available for one device.
						*/
					summaryList.add(keySummary);
				}


			deviceDAO=DAOFactory.getInstance(parameterMap.get(SanityCheckUtil.PARAMETER_DB_USERNAME),parameterMap.get(SanityCheckUtil.PARAMETER_DB_PASSWORD),parameterMap.get(SanityCheckUtil.PARAMETER_DB_CONNECTION_URL),Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_DB_TYPE)));
			final int batchSize=Integer.parseInt(parameterMap.get(PARAMETER_DB_SAVE_DETAILS_BATCHSIZE));
			logger.debug("batch size is: "+ batchSize);
			deviceDAO.saveDetail(detailList,batchSize);


			logger.info("details saved");

			Collections.sort(summaryList,Summary.SummaryComparator);

			return summaryList;
	}



	public static Iteration saveSummary(Map<String, String> parameterMap, List<Summary> summaryList, RemoteCall remoteCall) throws SQLException, ClassNotFoundException {

		logger.info("saving summary");


		Date currentDate=Calendar.getInstance(Locale.US).getTime();


		long iterationId=0;

		long successCount=0;
		long failureCount=0;
		long pendingCount=0;
		long norecordCount=0;
		long exceptionCount=0;
		long abortCount=0;
		long expirationTimeout=0;
		long executionTimeout=0;
		long sessionTimeout=0;
		long cumilativeTotal=0;



		for(Summary summary : summaryList){

			abortCount=abortCount+summary.getAborted();
			pendingCount=pendingCount+summary.getPending();
			norecordCount=norecordCount+summary.getNorecord();
			exceptionCount=exceptionCount+summary.getException();
			expirationTimeout=expirationTimeout+summary.getExpirationTimeout();
			executionTimeout=executionTimeout+summary.getExecutionTimeout();
			sessionTimeout=sessionTimeout+summary.getSessionTimeout();
			successCount=successCount+summary.getSuccess();
			failureCount=failureCount+summary.getFailure();
			cumilativeTotal=cumilativeTotal+summary.getSampleNumberDevices();

			logger.debug("devicetypename and total values: "+ summary.toString());

//			summary.setCreationDate(currentDate); no need to update summary date
			summary.setSuccessratio(SanityCheckUtil.percentage(summary.getSuccess(),summary.getSampleNumberDevices()).multiply(HUNDRED).longValue());

			deviceDAO=DAOFactory.getInstance(parameterMap.get(SanityCheckUtil.PARAMETER_DB_USERNAME),parameterMap.get(SanityCheckUtil.PARAMETER_DB_PASSWORD),parameterMap.get(SanityCheckUtil.PARAMETER_DB_CONNECTION_URL),Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_DB_TYPE)));
			deviceDAO.updateSummary(summary);

			iterationId=summary.getIteration();


		}// end of all device type map


		/*
		 * to calculate performance total count added into the map.
		 */

		parameterMap.put(TOTAL_COUNT, cumilativeTotal+"");

		BigDecimal iterationSuccessRatio=SanityCheckUtil.percentage(successCount, cumilativeTotal).multiply(HUNDRED);

		Iteration updateMe =  deviceDAO.getIterationById(iterationId);

		updateMe.setId(iterationId);
		updateMe.setSuccessRatio(iterationSuccessRatio.intValue());
		updateMe.setTotalSamplesPerIteration(cumilativeTotal);
		updateMe.setOperationType(remoteCall.name());
		updateMe.setUsername(updateMe.getUsername());
		updateMe=SanityCheckUtil.updateIteration(updateMe,parameterMap);



		logger.info("summary saved");

		return updateMe;
	}
	

	
	   public static void createPerformanceSummary(Map<String, String> parameterMap, long startTime, long finishTime, Iteration iteration ) throws SQLException, ClassNotFoundException {

			/*
			 * calculate number of devices
			 */
			int totalCount=0;
			
			totalCount=Integer.parseInt(parameterMap.get(TOTAL_COUNT));
			
			logger.info(iteration.getOperationType()+ " number of records: "+totalCount);
			
			/*
		     * calculate the duration
			*/

			BigDecimal duration_per_minute = SanityCheckUtil.calculateDuration(startTime, finishTime, SanityCheckUtil.ONE_MINUTE);
			BigDecimal duration_per_second = SanityCheckUtil.calculateDuration(startTime, finishTime, 1000);

		   	iteration.setExecutionDuration(duration_per_second.longValue());

			logger.info("total duration in seconds= "+ duration_per_second);
			logger.info("total duration in minutes= "+ duration_per_minute);
		    logger.info(SanityCheckUtil.calculateDevicePerSecond(duration_per_second, totalCount)+" devices per second");


		    SanityCheckUtil.updateIteration(iteration,parameterMap);


	   }


	
	 	public static String shortenDateString(String str){
			
			return str.substring(0, str.length()-3);
			
		}
	
	 	public static String getTodayAsTime(int dateFormat, Locale loc){
	 		
	 		DateFormat format = DateFormat.getTimeInstance(DateFormat.MEDIUM, loc);
	 		Date currentTime = Calendar.getInstance(loc).getTime();
		
			String currentTimeStr=format.format(currentTime);
			
			return currentTimeStr;
	 	}
	 	
	 	public static String formatDate(Date toBeFormattedDate,int dateFormat, int timeFormat, Locale loc){
	 		
	 		DateFormat df = DateFormat.getDateTimeInstance(dateFormat, timeFormat, loc);
	 		
			return df.format(toBeFormattedDate);
			
		}
	 	

	 	
	 	public static String copyPasteManyTimesForMe(String c, int times){
	 		
	 		StringBuilder builder = new StringBuilder();	 		
	 		
	 		for (int i = 0; i < times; i++) {
				builder.append(c);
			}
	 		
	 		logger.debug("copy pasted string length is: "+builder.length());
	 		
	 		return builder.toString();
	 	}
	 	
	 	public static List<Device> convertFileToDevice(String filePath) throws IOException{
	 		
			List<Device> deviceList = new ArrayList<Device>();
			BufferedReader reader =null;
			Device device = null;
			try {
				
				reader = new BufferedReader(new FileReader(filePath+"deviceGUID.txt"));
				String line = null;
				while ((line = reader.readLine()) != null) {
				    
					try {
						
						Long deviceId = Long.parseLong(line);
						device = new Device();
						device.setDeviceId(deviceId);
						deviceList.add(device);
						
					} catch (NumberFormatException e) {
						
						logger.error("invalid line > " +line);
						
					}finally{
						
					}
					
				}
				
				
			} catch (FileNotFoundException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}finally{
				reader.close();
			}
			
			return deviceList;
		
		}
	 	
	 	public static BigDecimal percentage(long yourValue, long totalValue){
	 		
	 		BigDecimal yourVALUE = new BigDecimal(yourValue);
	 		BigDecimal totalVALUE = new BigDecimal(totalValue);
	 		
	 		BigDecimal resultVALUE = BigDecimal.ZERO;
	 		
	 		if (yourValue!=0){
	 			
	 			resultVALUE=yourVALUE.divide(totalVALUE, 2, BigDecimal.ROUND_HALF_UP);
	 			logger.debug("percentage is "+ resultVALUE);
	 			
	 		}
	 		
	 		return resultVALUE;
	 	}
	 	
	 	public static BigDecimal calculateDuration(long startTime, long finishTime, long interval){
	 		
	 		BigDecimal startVALUE = new BigDecimal(startTime);
	 		BigDecimal finishVALUE = new BigDecimal(finishTime);
	 		BigDecimal intervalVALUE = new BigDecimal(interval);
	 		
	 		BigDecimal durationVALUE = finishVALUE.subtract(startVALUE);
	 		
	 		return durationVALUE.divide(intervalVALUE,2,RoundingMode.HALF_UP);
	 		
	 	}
	 	
	 	public static BigDecimal calculateDevicePerSecond(BigDecimal duration, long numberOfDevices){
	 		
	 		BigDecimal total = new BigDecimal(numberOfDevices);
	 		return total.divide(duration,3,RoundingMode.HALF_UP);
	 		
	 	}
	 	
	 	public static boolean isNumeric(String number){
	 		
	 		try  
	 		  {  
	 		    @SuppressWarnings("unused")
				long d = Long.parseLong(number);  
	 		  }  
	 		  catch(NumberFormatException nfe)  
	 		  {
	 			logger.error("this is not numeric "+number);
	 		    return false;  
	 		  }  
	 			
	 		  return true;
	 		  
	 	}







	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map<Summary,List<Device>> getAllDevicesPerDeviceTypeAndFirmware(Map<String,String> parameterMap) throws InterruptedException, ExecutionException, SQLException, ClassNotFoundException {

		List<Device> list = new ArrayList<Device>();
		Map<Summary, List<Device>> deviceMap = new HashMap<Summary,List<Device>>();

		int limit=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_DB_FETCH_LIMIT_PER_DT));

		Map<Summary,Future<List>> deviceFutureMap=new HashMap<Summary, Future<List>>();

		deviceDAO=DAOFactory.getInstance(parameterMap.get(SanityCheckUtil.PARAMETER_DB_USERNAME),parameterMap.get(SanityCheckUtil.PARAMETER_DB_PASSWORD),parameterMap.get(SanityCheckUtil.PARAMETER_DB_CONNECTION_URL),Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_DB_TYPE)));

		int nThreads = Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_NTHREADS));
		int executorType = Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTOR_TYPE));

		if ((nThreads<1)||(executorType<1)){
			logger.error("parameters is given below zero or equal to zero-- "+"nThreads: "+nThreads+" executorType: "+executorType);
			executorType=1;
			nThreads=1;
		}

		executorService=ExecutorServiceFactory.getInstance(executorType, nThreads);


		String deviceTypes[]=SanityCheckUtil.extractStringArrayFromPropertiesValue(parameterMap.get(SanityCheckUtil.PARAMETER_DEVICETYPES),SanityCheckUtil.DELIMETER_COMMA);
		long deviceTypeIdArray[] = SanityCheckUtil.convertStringArray2longArray(deviceTypes);


		for (long deviceTypeId : deviceTypeIdArray) {

			List<DeviceType> deviceTypeArray = deviceDAO.getSoftwareVersionsByDeviceType(deviceTypeId, Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_DB_FETCH_LIMIT_PER_FIRMWARE)),Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_DB_FETCH_THE_LEAST_NUMBER_OF_DEVICES_PER_FIRMWARE)));


			for(DeviceType deviceType:deviceTypeArray) {


				EnhancedFetchDataTask<List> fetcher = new EnhancedFetchDataTask<List>();
				fetcher.setDeviceDAO(deviceDAO);
				fetcher.setLimit(limit);
				fetcher.setDeviceType(deviceType);


				Future<List> future = executorService.submit(fetcher);
				Summary summary = new Summary();
				summary.setDevicetype(deviceType.getName());
				summary.setFirmware(deviceType.getSoftwareVersion());
				summary.setTotalNumberDevices(deviceType.getTotal());
				summary.setSampleNumberDevices(limit);
				summary.setDeviceTypeId(deviceTypeId);
				summary.setProtocol(deviceType.getDeviceProtocol());
				deviceFutureMap.put(summary, future);
			}

		}

		int howManySecondsWaited=0;

		for(Map.Entry<Summary, Future<List>> entry : deviceFutureMap.entrySet()){


			while (!entry.getValue().isDone() ){
				logger.info("fetching devicetypes "+entry.getKey()+ " is being waited for "+ howManySecondsWaited++ + " second");
				Thread.sleep(1000);
			};

		}
		logger.debug("device fetching operation completed succesfully..");


		for(Map.Entry<Summary, Future<List>> entry : deviceFutureMap.entrySet()){

			list = (List<Device>) entry.getValue().get();


			deviceMap.put(entry.getKey(), list);

			logger.debug("device type name "+ entry.getKey()+" added with "+list.size()+ " devices");

		}

		return deviceMap;
	}

	public static boolean validateCommandLineParameter(String args[]){

		boolean isValid=false;

		if((args!=null)&&(args.length>0)){

			isValid=true;

		}else{

			logger.error("check the parameter you entered" + args + " it is invalid.");

		}

		return isValid;

	}

	public static void appendParameter(Map<String, String> parameterMap, String keyValue, String entryValue){

			parameterMap.put(keyValue, entryValue);
			logger.info("key and its value given from command line "+keyValue+", "+entryValue);


	}


	public static RemoteCall convertStringToRemoteCall(String remoteCallStr){

		RemoteCall remoteCall=RemoteCall.GPV;

		int firstArg;

		if ((remoteCallStr != null)&&(remoteCallStr.length()>0)) {
			try {
				firstArg = Integer.parseInt(remoteCallStr);

				switch(firstArg) {


					case SanityCheckUtil.FUNCTIONCODE_CDA: {
						remoteCall = RemoteCall.CDA;
						logger.info("Remote Call " + remoteCall);
						break;
					}

					case SanityCheckUtil.FUNCTIONCODE_GPV: {
						remoteCall = RemoteCall.GPV;
						logger.info("Remote Call " + remoteCall);
						break;
					}
					case SanityCheckUtil.FUNCTIONCODE_MERGE_SERVICETAGS: {
						remoteCall = RemoteCall.ADD_SERVICE_TAG;
						logger.info("Remote Call " + remoteCall);
						break;
					}
					case SanityCheckUtil.FUNCTIONCODE_REMOVE: {
						remoteCall = RemoteCall.REMOVE_DEVICE;
						logger.info("Remote Call " + remoteCall);
						break;
					}
					case SanityCheckUtil.FUNCTIONCODE_SPV: {
						//TODO SPV will be active
						remoteCall = RemoteCall.GPV;
						logger.info("Remote Call " + remoteCall);
						break;
					}

					default: {
						logger.error("invalid command line parameter, please correct it: " + firstArg);
						System.exit(1);
					}
				}

			} catch (NumberFormatException e) {
				logger.error("Argument" + remoteCallStr + " must be an integer.");
				System.exit(1);
			}
		}else{

			logger.error("check the parameter you entered" + remoteCallStr + " it is invalid.");
			System.exit(1);
		}


		return remoteCall;
	}


	public static Iteration updateIteration(Iteration saveIteration, Map<String, String> parameterMap) throws SQLException, ClassNotFoundException {




		logger.info("iteration before update "+saveIteration);

		deviceDAO.updateIteration(saveIteration);

		Iteration iteration = deviceDAO.getIterationById(saveIteration.getId());

		logger.info("iteration updated successfully "+iteration);

		return iteration;

	}


}


