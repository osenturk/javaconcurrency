package hdm.sanitycheck.database;

import java.util.Date;

/**
 * Created by ozansenturk on 1/13/16.
 */
public class Iteration {

    private long id;
    private String username;
    private Date creationTime;
    private int successRatio;
    private long totalSamplesPerIteration;


    private String operationType;
    private long executionDuration;



    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public long getExecutionDuration() {
        return executionDuration;
    }

    public void setExecutionDuration(long executionDuration) {
        this.executionDuration = executionDuration;
    }

    //TODO hashcode and equals should be overridden


    @Override
    public String toString() {
        return "Iteration{ id=" + id +

                ", creationtime=" + creationTime +
                ", successRatio=" + successRatio +
                ", totalSamplesPerIteration=" + totalSamplesPerIteration +
                ", username=" + username +
                ", operationType=" + operationType +
                ", executionDuration=" + executionDuration +
                "}";
    }


    public long getTotalSamplesPerIteration() {
        return totalSamplesPerIteration;
    }

    public void setTotalSamplesPerIteration(long totalSamplesPerIteration) {
        this.totalSamplesPerIteration = totalSamplesPerIteration;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public int getSuccessRatio() {
        return successRatio;
    }

    public void setSuccessRatio(int successRatio) {
        this.successRatio = successRatio;
    }
}
