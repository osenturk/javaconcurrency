/**
 * 
 */
package hdm.sanitycheck.concurrency;

/**
 * @author ozan
 *
 */
public class SingleDeviceOperation {
	
	private long expirationTimeout;
	private long executionTimeout;
	private boolean disableCaptureConstraint;
	private String policyClass;
	private int priority;
	
	/**
	 * @return the expirationTimeout
	 */
	public long getExpirationTimeout() {
		return expirationTimeout;
	}
	/**
	 * @param expirationTimeout the expirationTimeout to set
	 */
	public void setExpirationTimeout(long expirationTimeout) {
		this.expirationTimeout = expirationTimeout;
	}
	/**
	 * @return the executionTimeout
	 */
	public long getExecutionTimeout() {
		return executionTimeout;
	}
	/**
	 * @param executionTimeout the executionTimeout to set
	 */
	public void setExecutionTimeout(long executionTimeout) {
		this.executionTimeout = executionTimeout;
	}
	/**
	 * @return the disableCaptureConstraint
	 */
	public boolean isDisableCaptureConstraint() {
		return disableCaptureConstraint;
	}
	/**
	 * @param disableCaptureConstraint the disableCaptureConstraint to set
	 */
	public void setDisableCaptureConstraint(boolean disableCaptureConstraint) {
		this.disableCaptureConstraint = disableCaptureConstraint;
	}
	/**
	 * @return the policyClass
	 */
	public String getPolicyClass() {
		return policyClass;
	}
	/**
	 * @param policyClass the policyClass to set
	 */
	public void setPolicyClass(String policyClass) {
		this.policyClass = policyClass;
	}
	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	@Override
    public String toString() {
        return "SingleDeviceOperation{ expirationTimeout=" + expirationTimeout + 
        		", executionTimeout=" + executionTimeout + 
        		", disableCaptureConstraint=" + disableCaptureConstraint + 
        		", policyClass=" + policyClass +
        		", priority=" + priority +
        		"}";
	}
	
}
