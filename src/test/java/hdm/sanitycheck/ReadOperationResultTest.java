/**
 * 
 */
package hdm.sanitycheck;

import hdm.sanitycheck.concurrency.ExecutorServiceFactory;
import hdm.sanitycheck.concurrency.ReadDeviceActionResultTask;
import hdm.sanitycheck.concurrency.ReadOperationResultTask;
import hdm.sanitycheck.database.Device;
import hdm.sanitycheck.main.SanityCheckUtil;
import hdm.sanitycheck.nbicore.NbiServiceFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.alcatel.hdm.service.nbi.client.NBIServicePort;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author ozan
 *
 */
public class ReadOperationResultTest {
	
	private final static Logger  logger = Logger.getLogger(ReadOperationResultTest.class);
	private Map<String, String> parameterMap;
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp() throws Exception {

		
		parameterMap = SanityCheckUtil.loadParameters();
		
		// BasicConfigurator replaced with PropertyConfigurator.
	     PropertyConfigurator.configure("etc/log4j.properties");
		
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	@After
	public void tearDown() throws Exception {

		
		parameterMap=null;
		
	}

	@Test
	public void testReadOperationResultTask() throws InterruptedException, ExecutionException, IOException, ServiceException{
		
		/*
		 * reading result operation prepared below
		 */
		
		Device device = new Device();
		
		device.setDeviceId(11409030);
		device.setOperationResultId(397714549L);
		
		List<String> endUrlList = SanityCheckUtil.mergeIpsAndPorts(parameterMap.get(SanityCheckUtil.PARAMETER_NBI_IPS), parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PORTS));
		String username=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);
		
		int nThreads=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_NTHREADS));
		int executorType=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTOR_TYPE));
		ExecutorService executorService = ExecutorServiceFactory.getInstance(executorType, nThreads);
		
		NbiServiceFactory factory = NbiServiceFactory.getInstance();
		NBIServicePort nbiPort = factory.getNbiPortInstance(endUrlList.get(0), username, password);
		
		ReadOperationResultTask readResultTask = new ReadOperationResultTask(nbiPort, device);
		
		Future<Device> result = executorService.submit(readResultTask);
		Device resultDevice= result.get();
		logger.debug("result is: "+ resultDevice.getDeviceActionResultStatus());
	}


	@Test
	public void ReadDeviceActionResultTask() throws InterruptedException, ExecutionException, IOException, ServiceException{

		/*
		 * reading result operation prepared below
		 */
//		20071915-397705527- Technicolor TG788Avn,	589835,	CP1222SA94U, EXECUTION TIMEOUT

//		11508825-397706650-RETRIED 1 HG655b	00E0FC	21530315887S1A000689, EXPIRATION TIMEOUT

//		1067619-397706463-RETRIED 2 HG520b	00E0FC	21530303288K93269791
//		HG655b	00E0FC	21530315887S1A005131 13311355 device operation result id 397706574
//		HG655b	00E0FC	21530315887S1A017875 11409030 device operation result id 397714549
//		28749067	397743250	I-240W-A	184A6F	ALCLFAF885CA	NORECORD

		Device device = new Device();

		device.setDeviceId(20071915);
		device.setOperationResultId(397705527L);
		device.setSerialNumber("CP1222SA94U");
		device.setPC("Technicolor TG788Avn");
		device.setOUI("589835");
		device.setDeviceProtocol("DEVICE_PROTOCOL_DSLFTR069v1");

		List<String> endUrlList = SanityCheckUtil.mergeIpsAndPorts(parameterMap.get(SanityCheckUtil.PARAMETER_NBI_IPS), parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PORTS));
		String username=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);

		int nThreads=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_NTHREADS));
		int executorType=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTOR_TYPE));
		ExecutorService executorService = ExecutorServiceFactory.getInstance(executorType, nThreads);

		NbiServiceFactory factory = NbiServiceFactory.getInstance();
		NBIServicePort nbiPort = factory.getNbiPortInstance(endUrlList.get(0), username, password);

		ReadDeviceActionResultTask readResultTask = new ReadDeviceActionResultTask(nbiPort, device);

		Future<Device> result = executorService.submit(readResultTask);
		Device resultDevice= result.get();
		logger.debug("result is: "+ resultDevice.getDeviceActionResultStatus());
	}


	

	
}



