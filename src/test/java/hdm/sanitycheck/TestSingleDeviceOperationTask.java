/**
 * 
 */
package hdm.sanitycheck;

import com.alcatel.hdm.service.nbi.client.NBIServicePort;
import com.alcatel.hdm.service.nbi.dto.*;
import com.alcatel.hdm.service.nbi.dto.notification.NBIDeviceActionResult;
import hdm.sanitycheck.concurrency.ExecutorServiceFactory;
import hdm.sanitycheck.concurrency.SingleDeviceOperation;
import hdm.sanitycheck.database.Device;
import hdm.sanitycheck.database.Summary;
import hdm.sanitycheck.main.SanityCheckUtil;
import hdm.sanitycheck.main.SanityCheckUtil.OperationResult;
import hdm.sanitycheck.main.SanityCheckUtil.RemoteCall;
import hdm.sanitycheck.nbicore.NbiServiceFactory;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.soap.SOAPFaultException;
import javax.xml.soap.Detail;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static junit.framework.TestCase.assertTrue;

/**
 * @author ozan
 *
 */
public class TestSingleDeviceOperationTask {
	
	private final static Logger  logger = Logger.getLogger(TestSingleDeviceOperationTask.class);
	private final static String filePath ="./files/"; 
	
	private int nThreads, executorType;
	private ExecutorService executorService;
	private RemoteCall remoteCall=RemoteCall.CDA;

	private List<Summary> deviceOperationResultList;
	private Map<OperationResult, Integer> resultPerFileMap;
	private Map<Summary,List<Device>> deviceNameMap;
	private Map<Summary,List<Future<Device>>> sdoMap;
	private Map<Summary,List<Future>> resultMap = new HashMap<Summary, List<Future>>();
	private List<Summary> summaryList;

	private Map<String, String> parameterMap;

	private final static String URL="http://172.20.44.242:7015/remotehdm/NBIService?WSDL";
	
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp() throws Exception {

		parameterMap = SanityCheckUtil.loadParameters();

		nThreads=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_NTHREADS));
		executorType=Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTOR_TYPE));
		executorService = ExecutorServiceFactory.getInstance(executorType, nThreads);

		
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	@After
	public void tearDown() throws Exception {

		executorService.shutdown();
		parameterMap=null;
		
	}

	@Test
	public void testSingleDeviceOperationBulk() throws InterruptedException, ExecutionException, IOException, ServiceException, SQLException, ClassNotFoundException {


		Date date = Calendar.getInstance().getTime();

		long startTime=date.getTime();
		logger.debug("started .. "+startTime);

		Map<Summary,List<Future<Device>>> sdoMap = null;




		deviceNameMap = SanityCheckUtil.getAllDevicesPerDeviceTypeAndFirmware(parameterMap);


		sdoMap= SanityCheckUtil.sendSingleDeviceOperation(deviceNameMap,parameterMap,RemoteCall.GPV);


		resultMap = SanityCheckUtil.getOperationResultPerSoftware( sdoMap, parameterMap,RemoteCall.GPV);


		summaryList = SanityCheckUtil.saveDetails(parameterMap, resultMap,RemoteCall.GPV);



		SanityCheckUtil.saveSummary(parameterMap, summaryList, RemoteCall.GPV);

		date = Calendar.getInstance().getTime();
		long finishTime=date.getTime();
		logger.debug("endeded .. "+finishTime);

		logger.debug(SanityCheckUtil.calculateDuration(startTime,finishTime,1000));



	}
	@Test
	public void testExtractingDeviceTypeNameFromMapKey(){

		String  deviceTypeKey1="Huawei VDSL 102*V100R001C01B135";
		String  deviceTypeKey2="ONT CIG*3FE53219AOCD15,";
		String  deviceTypeKey3="ONT CIG*3FE53219AOCD15,";

		String tmp = deviceTypeKey1.substring(0,deviceTypeKey1.indexOf(SanityCheckUtil.DELIMETER_DEVICETYPE_SOFTWARE));

		logger.debug(tmp);
	}

	@Test
	public void testCatchSoapFaultException(){

		QName qname=null;
		Detail detail=null;

			try {

					throw new javax.xml.rpc.soap.SOAPFaultException(qname, "", "", detail);

				}catch (SOAPFaultException soapFaultException){

				logger.error(soapFaultException.getDetail().toString());
				logger.error(soapFaultException.getMessage());
				logger.error(soapFaultException.getFaultString().toString());

			}

	}


	@Test
	public void testFindDeviceByTemplate() throws ServiceException {



		List<String> endUrlList = SanityCheckUtil.mergeIpsAndPorts(parameterMap.get(SanityCheckUtil.PARAMETER_NBI_IPS), parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PORTS));
		String username=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);

		SingleDeviceOperation sdo = new SingleDeviceOperation();
		sdo.setDisableCaptureConstraint(Boolean.parseBoolean(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_DISABLE_CAPTURE_CONSTRAINT)));
		sdo.setExecutionTimeout(Long.parseLong(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTION_TIMEOUT)));
		sdo.setExpirationTimeout(Long.parseLong(parameterMap.get(SanityCheckUtil.PARAMETER_EXPIRATION_TIMEOUT)));
		sdo.setPolicyClass(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_POLICY_CLASS));
		sdo.setPriority(Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_PRIORITY)));

		NbiServiceFactory factory = NbiServiceFactory.getInstance();

		NBIServicePort nbiServicePort=	NbiServiceFactory.getNbiPortInstance(URL,username,password);


//		SingleDeviceOperationTask sdoTask = new SingleDeviceOperationTask(nbiPort,device,remoteCall,parameter);
//
//		sdoTask.setDisableCaptureConstraint(sdo.isDisableCaptureConstraint());
//		sdoTask.setExecutionTimeout(sdo.getExecutionTimeout());
//		sdoTask.setExpirationTimeout(sdo.getExpirationTimeout());
//		sdoTask.setPolicyClass(sdo.getPolicyClass());
//		sdoTask.setPriority(sdo.getPriority());
		/*
			nbi resources initialized and added into iterator.
		 */
//		try{

//			nbiServicePort.captureDevice(23227799L);

			NBITemplate nbiTemplate = new NBITemplate();
			nbiTemplate.setName("ct.find.devices.serialNumber"); //Find Devices By Id
			nbiTemplate.setParameters(new NBIParameter[]{
					new NBIParameter("serialNumber","LK922677266")});


		NBIDeviceData[] devices=null;
		try {
			 devices = nbiServicePort.findDevicesByTemplate( nbiTemplate, -1, -1);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		if(devices!=null){
			logger.debug(devices);
		}

//		}catch (RemoteException e) {
//			e.printStackTrace();
//		}


	}

	@Test
	public void testSingleDeviceOperation() throws ServiceException {



		String username=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);

		NBIServicePort nbiServicePort=	NbiServiceFactory.getNbiPortInstance(URL,username,password);

		Device device = new Device();
		device.setDeviceId(11287170L);

		NBISingleDeviceOperationOptions nbiOptions = new NBISingleDeviceOperationOptions();
		nbiOptions.setExecutionTimeoutSeconds(Long.parseLong(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTION_TIMEOUT)));
		nbiOptions.setExpirationTimeoutSeconds(Long.parseLong(parameterMap.get(SanityCheckUtil.PARAMETER_EXPIRATION_TIMEOUT)));
		nbiOptions.setPriority(Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_PRIORITY)));
		nbiOptions.setPolicyClass(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_POLICY_CLASS));
		nbiOptions.setDisableCaptureConstraint(true);

		NBIFunction nbiFunction= new NBIFunction();
		nbiFunction.setFunctionCode(34);


		/*
			nbi resources initialized and added into iterator.
		 */


		Long operationResultId=null;



		try {
			operationResultId=nbiServicePort.createSingleDeviceOperationByDeviceGUID(device.getDeviceId(),nbiFunction,nbiOptions);
		logger.debug(operationResultId);
		} catch (java.rmi.RemoteException remoteException) {
			Throwable throwable = remoteException.getCause();

			try {

				if (throwable instanceof SOAPFaultException) {

					logger.error(((SOAPFaultException) throwable).getFaultCode().toString());

					nbiServicePort.releaseDevice(device.getDeviceId());
					operationResultId = (Long) nbiServicePort.createSingleDeviceOperationByDeviceGUID(device.getDeviceId(), nbiFunction, nbiOptions);

					assertTrue("locked device accept single device operation", operationResultId!=null);

				}
			} catch (RemoteException e) {
				e.printStackTrace();
			} finally {

			}

		}






	}

	@Test
	public void testCaptureDisableConstraintForSingleDeviceOperation() throws ServiceException {



		String username=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password=parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);

		NBIServicePort nbiServicePort=	NbiServiceFactory.getNbiPortInstance(URL,username,password);

		Device device = new Device();
		device.setDeviceId(2935621);

		NBISingleDeviceOperationOptions nbiOptions = new NBISingleDeviceOperationOptions();
		nbiOptions.setExecutionTimeoutSeconds(Long.parseLong(parameterMap.get(SanityCheckUtil.PARAMETER_EXECUTION_TIMEOUT)));
		nbiOptions.setExpirationTimeoutSeconds(Long.parseLong(parameterMap.get(SanityCheckUtil.PARAMETER_EXPIRATION_TIMEOUT)));
		nbiOptions.setPriority(Integer.parseInt(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_PRIORITY)));
		nbiOptions.setPolicyClass(parameterMap.get(SanityCheckUtil.PARAMETER_SDO_POLICY_CLASS));
		nbiOptions.setDisableCaptureConstraint(false);

		NBIFunction nbiFunction= new NBIFunction();
		nbiFunction.setFunctionCode(34);


		/*
			nbi resources initialized and added into iterator.
		 */


		Long operationDisableCaptureConstraintFalse=null;

		try {
			operationDisableCaptureConstraintFalse=nbiServicePort.createSingleDeviceOperationByDeviceGUID(device.getDeviceId(),nbiFunction,nbiOptions);
		} catch (java.rmi.RemoteException remoteException) {

			assertTrue("if disableCaptureConstraint is false, you supposed to submit sdo",1==2);
		}
		assertTrue("locked device cannot accept single device operation", operationDisableCaptureConstraintFalse != null);

		Long operationDisableCaptureConstraintTrue=null;
		NBISingleDeviceOperationOptions nbiOptionsDisableCaptureConstraintTrue= nbiOptions;
		nbiOptionsDisableCaptureConstraintTrue.setDisableCaptureConstraint(true);

		try {
			operationDisableCaptureConstraintTrue=nbiServicePort.createSingleDeviceOperationByDeviceGUID(device.getDeviceId(),nbiFunction,nbiOptionsDisableCaptureConstraintTrue);
		} catch (java.rmi.RemoteException remoteException) {
			Throwable throwable = remoteException.getCause();

			try {

				if (throwable instanceof SOAPFaultException) {

					logger.error(((SOAPFaultException) throwable).getFaultCode().toString());

					assertTrue("locked device accept single device operation", operationDisableCaptureConstraintTrue==null);

				}
			}finally {

			}

		}






	}
	@Test
	public void testGetOperationResultWithLockedDevice() throws ServiceException {

		String username = parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password = parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);


		NBIServicePort nbiServicePort = NbiServiceFactory.getNbiPortInstance(URL, username, password);
//24136475
		Device device = new Device();
		device.setDeviceId(16362649L);

		device.setOperationResultId(397118576L);

		NBIOperationStatus nbiOperationStatus = null;
		SanityCheckUtil.OperationResult operationResult=null;

		try {

			if (device.getOperationResultId() != null) {
				nbiOperationStatus = nbiServicePort.getOperationStatus(device.getOperationResultId());
				operationResult = getOperationResult(nbiOperationStatus);
			} else {
				logger.debug("Operation Result is null for " + device.toString());
				operationResult = OperationResult.NORECORD;
			}


		} catch (java.rmi.RemoteException remoteException) {

			Throwable throwable = remoteException.getCause();

			try {

				if (throwable instanceof SOAPFaultException) {
					assertTrue("not possible to have another operation result rather than exception",operationResult==OperationResult.EXCEPTION);
				}
			}finally {

			}

		}

	}

	//TODO redesign exception classification: SESSION TIMEOUT, EXPIRATION TIMEOUT
	@Test
	public void testGetOperationResult() throws ServiceException {

		/*
		PENDING
		28756945	397389105	I-240W-A	54A619	ALCLFADB16BE
		TIMEOUT
		14827177	397387637	Technicolor TG788Avn	589835	CP1206RAQV9
		SUCCESS
		20239615	397387706	Technicolor TG788Avn	589835	CP1219RA56L
		EXCEPTION
		21626551	397396159	__________I-240W-P__	0019C7	ALCLF8B9CECE	EXCEPTION
		 */

		String username = parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password = parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);


		NBIServicePort nbiServicePort = NbiServiceFactory.getNbiPortInstance(URL, username, password);
//24136475
		Device device = new Device();
		device.setDeviceId(21626551);

		device.setOperationResultId(397396159L);

		NBIOperationStatus nbiOperationStatus = null;
		SanityCheckUtil.OperationResult operationResult=null;

		try {

			if (device.getOperationResultId() != null) {
				nbiOperationStatus = nbiServicePort.getOperationStatus(device.getOperationResultId());
				NBIDeviceID nbiDeviceID=new NBIDeviceID();
				nbiDeviceID.setSerialNumber("ALCLF8B9CECE");
				nbiDeviceID.setOUI("__________I-240W-P__");
				nbiDeviceID.setProtocol("DEVICE_PROTOCOL_DSLFTR069v1");
				nbiDeviceID.setProductClass("0019C7");
				NBIDeviceActionResult deviceActionResult =nbiServicePort.getDeviceOperationStatus(nbiDeviceID,device.getOperationResultId());
				operationResult = getOperationResult(nbiOperationStatus);
			} else {
				logger.debug("Operation Result is null for " + device.toString());
				operationResult = OperationResult.NORECORD;
			}


		} catch (java.rmi.RemoteException remoteException) {

			Throwable throwable = remoteException.getCause();

			try {

				if (throwable instanceof SOAPFaultException) {
					assertTrue("not possible to have another operation result rather than exception",operationResult==OperationResult.EXCEPTION);
				}
			}finally {

			}

		}

	}




	@Test
	public void testGetDeviceOperationStatus () throws ServiceException {

		String username = parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password = parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);


		NBIServicePort nbiServicePort = NbiServiceFactory.getNbiPortInstance(URL, username, password);
//24136475
		Device device = new Device();
		device.setDeviceId(1067619);

		device.setOperationResultId(397729734L);

		NBIOperationStatus nbiOperationStatus = null;
		SanityCheckUtil.OperationResult operationResult=null;

		try {

			if (device.getOperationResultId() != null) {
				nbiOperationStatus = nbiServicePort.getOperationStatus(device.getOperationResultId());
				NBIDeviceID nbiDeviceID=new NBIDeviceID();

//				I-240W-A	54A619	ALCLFADB04D1 28734111 device operation result id 397715175
//				HG520b	00E0FC	21530303288K94215364 1067089

				nbiDeviceID.setSerialNumber("21530303288K94215364");
//				nbiDeviceID.setSerialNumber("SZ20100530578");
				nbiDeviceID.setOUI("00E0FC");
//				nbiDeviceID.setOUI("002569");
				nbiDeviceID.setProtocol("DEVICE_PROTOCOL_DSLFTR069v1");
				nbiDeviceID.setProductClass("HG520b");
//				nbiDeviceID.setProductClass("F@ST2804STC");
				NBIDeviceActionResult deviceActionResult =nbiServicePort.getDeviceOperationStatus(nbiDeviceID,device.getOperationResultId());
				operationResult = getOperationResult(nbiOperationStatus);

				logger.debug("operatio status "+operationResult);

				logger.debug("device action result status "+deviceActionResult.getStatus()+" substatus "+deviceActionResult.getSubStatus());

			} else {
				logger.debug("Operation Result is null for " + device.toString());
				operationResult = OperationResult.NORECORD;
			}


		} catch (java.rmi.RemoteException remoteException) {

			Throwable throwable = remoteException.getCause();

			try {

				if (throwable instanceof SOAPFaultException) {
					assertTrue("not possible to have another operation result rather than exception",operationResult==OperationResult.EXCEPTION);
				}
			}finally {

			}

		}

	}


	@Test
	public void GetDeviceActionResultHistory () throws ServiceException {

		String username = parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password = parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);


		NBIServicePort nbiServicePort = NbiServiceFactory.getNbiPortInstance(URL, username, password);
//24136475

		NBIOperationStatus nbiOperationStatus = null;
		SanityCheckUtil.OperationResult operationResult=null;
		NBIDeviceActionResult[] nbiDeviceActionResultArray =null;

		try {

//			HG655b	00E0FC	21530315887S1A017875 11409030 device operation result id 397714549
//			I-240W-A	54A619	ALCLFADB04D1 28734111 device operation result id 397715175
			NBIDeviceID nbiDeviceID=new NBIDeviceID();

				nbiDeviceID.setSerialNumber("21530315887S1A017875");
//				nbiDeviceID.setSerialNumber("SZ20100530578");
				nbiDeviceID.setOUI("00E0FC");
//				nbiDeviceID.setOUI("002569");
				nbiDeviceID.setProtocol("DEVICE_PROTOCOL_DSLFTR069v1");
				nbiDeviceID.setProductClass("HG655b");
//				nbiDeviceID.setProductClass("F@ST2804STC");

				nbiDeviceActionResultArray = nbiServicePort.getDeviceOperationsHistory(nbiDeviceID,-1,2);

			if(nbiDeviceActionResultArray[0]!=null){
				logger.debug("substatus "+nbiDeviceActionResultArray[0].getSubStatus());
				logger.debug("status "+nbiDeviceActionResultArray[0].getStatus());
			}




		} catch (java.rmi.RemoteException remoteException) {

			Throwable throwable = remoteException.getCause();

			try {

				if (throwable instanceof SOAPFaultException) {
					assertTrue("not possible to have another operation result rather than exception",operationResult==OperationResult.EXCEPTION);
				}
			}finally {

			}

		}

	}


	private OperationResult getOperationResult(NBIOperationStatus nbiOperationStatus){

		OperationResult operationResult;

		if(nbiOperationStatus==null){
			operationResult= OperationResult.NORECORD;

		}else if (nbiOperationStatus.getStatus()==OperationResult.ABORTED.getCode()){

			operationResult= OperationResult.ABORTED;
			logger.debug("nbi operation result: "+nbiOperationStatus.getFaultKey());

		}else if (nbiOperationStatus.getStatus()==OperationResult.TIMEOUT.getCode()){

			operationResult= OperationResult.TIMEOUT;

		}else if (nbiOperationStatus.getStatus()==OperationResult.SUCCESS.getCode()){

			operationResult= OperationResult.SUCCESS;

		}else if (nbiOperationStatus.getStatus()==OperationResult.PENDING.getCode()){

			operationResult= OperationResult.PENDING;

		}else{

			operationResult= OperationResult.NORECORD;
			}

		return operationResult;
	}

	@Test
	public void testWhileDoIncrement(){

		int retry=2;

		int retryCount=0;


		do{

			logger.debug("inside do while loop counter "+retryCount);

		}while(retryCount++<retry);

		logger.debug("outside do while loop counter "+retryCount);
	}


	@Test
	public void GetActiveDeviceAlarmsByDeviceArray () throws ServiceException {

		String username = parameterMap.get(SanityCheckUtil.PARAMETER_NBI_USERNAME);
		String password = parameterMap.get(SanityCheckUtil.PARAMETER_NBI_PASSWORD);


		NBIServicePort nbiServicePort = NbiServiceFactory.getNbiPortInstance(URL, username, password);
//24136475

		NBIOperationStatus nbiOperationStatus = null;
		SanityCheckUtil.OperationResult operationResult=null;
		NBIDeviceActionResult[] nbiDeviceActionResultArray =null;

		Long deviceGUID=23025502L;

		try {

//			HG655b	00E0FC	21530315887S1A017875 11409030 device operation result id 397714549
//			I-240W-A	54A619	ALCLFADB04D1 28734111 device operation result id 397715175
			NBIDeviceID nbiDeviceID=new NBIDeviceID();

			nbiDeviceID.setSerialNumber("21530315887S1A017875");
//				nbiDeviceID.setSerialNumber("SZ20100530578");
			nbiDeviceID.setOUI("00E0FC");
//				nbiDeviceID.setOUI("002569");
			nbiDeviceID.setProtocol("DEVICE_PROTOCOL_DSLFTR069v1");
			nbiDeviceID.setProductClass("HG655b");
//				nbiDeviceID.setProductClass("F@ST2804STC");

			Long [] deviceArray = {deviceGUID};
			NBIAlarm[] nbiAlarmArray = nbiServicePort.getActiveAlarmsOfDevices(deviceArray);

			if(nbiAlarmArray!=null){

				for (int i=0;i<nbiAlarmArray.length;i++) {

					NBIAlarm alarm = nbiAlarmArray[i];


					logger.debug(alarm.getAlarmId());
					logger.debug(alarm.getType());

//				ConnectionRequestFailure

					if(alarm.getType().equalsIgnoreCase("ConnectionRequestFailure")){
						nbiServicePort.clearAlarm(alarm.getDeviceGUID(),alarm.getAlarmId());
						logger.debug("Alarm cleaned for device id "+alarm.getDeviceGUID());
					}


				}

			}



		} catch (java.rmi.RemoteException remoteException) {

			Throwable throwable = remoteException.getCause();

			try {

				if (throwable instanceof SOAPFaultException) {
					assertTrue("not possible to have another operation result rather than exception",operationResult==OperationResult.EXCEPTION);
				}
			}finally {

			}

		}

	}



}




